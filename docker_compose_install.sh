clear
docker-compose down
docker rmi chm_django:latest
docker-compose up --build -d
# docker-compose run django  python manage.py makemigrations --merge
docker-compose run django  python manage.py migrate
docker-compose run django  python manage.py createsuperuser
docker ps