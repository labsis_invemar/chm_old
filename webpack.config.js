var BundleTracker = require('webpack-bundle-tracker');
const webpack = require('webpack');
const dotenv = require('dotenv');
const fs = require('fs');
const path = require('path');


module.exports = (env) => {

    // Get the root path (assuming your webpack config is in the root of your project!)
    const currentPath = path.join(__dirname);

    // Create the fallback path (the production .env)
    const basePath = currentPath + '/.env';

    // We're concatenating the environment name to our filename to specify the correct env file!
    const envPath = basePath + '.' + env.ENVIRONMENT;

    // Check if the file exists, otherwise fall back to the production .env
    const finalPath = fs.existsSync(envPath) ? envPath : basePath;

    // Set the path parameter in the dotenv config
    const fileEnv = dotenv.config({
        path: finalPath
    }).parsed;
    

    // reduce it to a nice object, the same as before (but with the variables from the file)
    const envKeys = Object.keys(fileEnv).reduce((prev, next) => {
        prev[`process.env.${next}`] = JSON.stringify(fileEnv[next]);
        return prev;
    }, {});

    return {
        entry: './apps/frontend/src/index.jsx',
        output: {
            filename: 'main.js',
            chunkFilename: '[name].bundle.js',
            path: __dirname + '/apps/frontend/static/frontend'
        },
        performance: {
            hints: 'warning',
            maxEntrypointSize: 700000,
            maxAssetSize: 700000,
        },
        module: {
            rules: [{
                    test: /\.jsx?$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader"
                    }
                },
                {
                    test: /\.css$/,
                    loader: "style-loader!css-loader"
                },
            ]
        },
        plugins: [
            new BundleTracker({
                path: __dirname + '/chm/',
                filename: 'webpack-stats.json'
            }),
            new webpack.DefinePlugin(envKeys)
        ],
    }
};