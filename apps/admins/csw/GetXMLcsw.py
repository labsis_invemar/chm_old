from owslib.csw import CatalogueServiceWeb
import xml.etree.ElementTree as ET
from requests.exceptions import ConnectionError


class GetXMLcsw:
    def __init__(self, url,pagination=False):
        self.url = url
        self.pagination = pagination

    def get_csw(self):
        #'https://www.caribbeanmarineatlas.net/catalogue/csw'
        csw = None
        try:
            csw = CatalogueServiceWeb(self.url)
            csw.getrecords2(outputschema='http://www.isotc211.org/2005/gmd',typenames='gmd:MD_Metadata',esn='full',maxrecords=1000)
        except ConnectionError:
            csw = CatalogueServiceWeb(self.url,skip_caps=True)
            csw.getrecords2(outputschema='http://www.isotc211.org/2005/gmd',typenames='gmd:MD_Metadata',esn='full',maxrecords=1000)
        #csw.getrecords2()
        
        #csw.getrecords2(outputschema='http://www.opengis.net/cat/csw/2.0.2',typenames='gmd:MD_Metadata',esn='full',maxrecords=50)
        
        # print(csw.results)
        # print(csw.request)
        # print(csw.response)
        tree = ET.fromstring(csw.response)
        cswarr = []
        c = {}
        next_record = csw.results["nextrecord"]
        for child in tree.iter():
            title = self.get_element("title",child)
            country = self.get_element("country",child)
            abstract = self.get_element("abstract",child)
            topicCategory = self.get_element("topicCategory",child)
            date = self.get_date(child)
            url = self.get_url(child)
            if title is not None:
                c['title'] = title
            elif country is not None:
                c['country'] = country
            elif abstract is not None:
                c['abstract'] = abstract
            elif topicCategory is not None:
                c['topicCategory'] = topicCategory
            elif url is not None:
                c['url'] = url
            elif date is not None:
                c['date'] = date
            if  'title' in c and 'abstract' in c and 'topicCategory' in c and 'url' in c and 'date' in c:
                cswarr.append(c)
                c = {}
        if self.pagination:
            while next_record>0:
                try:
                    csw.getrecords2(outputschema='http://www.isotc211.org/2005/gmd',typenames='gmd:MD_Metadata',esn='full',maxrecords=1000, startposition=next_record)
                except ConnectionError:
                    csw = CatalogueServiceWeb(self.url,skip_caps=True)
                    csw.getrecords2(outputschema='http://www.isotc211.org/2005/gmd',typenames='gmd:MD_Metadata',esn='full',maxrecords=1000, startposition=next_record)

                # print(csw.results)
                # print(csw.request)
                # print(csw.response)
                tree = ET.fromstring(csw.response)
                next_record=csw.results["nextrecord"]
                c = {}
                for child in tree.iter():
                    title = self.get_element("title",child)
                    country = self.get_element("country",child)
                    abstract = self.get_element("abstract",child)
                    topicCategory = self.get_element("topicCategory",child)
                    date = self.get_date(child)
                    url = self.get_url(child)
                    if title is not None:
                        c['title'] = title
                    elif country is not None:
                        c['country'] = country
                    elif abstract is not None:
                        c['abstract'] = abstract
                    elif topicCategory is not None:
                        c['topicCategory'] = topicCategory
                    elif url is not None:
                        c['url'] = url
                    elif date is not None:
                        c['date'] = date
                    if  'title' in c and 'abstract' in c and 'topicCategory' in c and 'url' in c and 'date' in c:
                        cswarr.append(c)
                        c = {}
        return cswarr



    def get_element(self,tag,child):
        if(tag in child.tag):
            for item in child.iter():
                if item.text is not None and item.text.strip() != "":
                    #print(tag,": ",item.text)
                    return str(item.text)
    def get_date(self,child):
        if("date" in child.tag):
            for item in child.iter():
                if( "CI_Date" in item.tag):
                    for item2 in item.iter():
                        if("date" in item2.tag):
                            for item3 in item2.iter(): 
                                if("DateTime" in item3.tag):
                                    for item4 in item3.iter():
                                        if item4.text is not None and item4.text.strip() != "":
                                            #print("Fecha : ",item4.text)
                                            return str(item4.text)
    def get_url(self,child):
        if("distributionInfo" in child.tag):
            for item in child.iter():
                url = False
                if("MD_Distribution" in item.tag):
                    for item2 in item.iter():
                        if("transferOptions" in item2.tag):
                            for item3 in item2.iter():
                                if("MD_DigitalTransferOptions" in item3.tag):
                                    for item4 in item3.iter():    
                                        if( "onLine" in item4.tag):
                                            for item5 in item4.iter():
                                                if("CI_OnlineResource" in item5.tag):
                                                    for item6 in item5.iter(): 
                                                        if("linkage" in item6.tag):
                                                            for item7 in item6.iter():
                                                                if item7.text is not None and item7.text.strip() != "" and not url:
                                                                    #print("URL : ",item7.text)
                                                                    url = True
                                                                    return str(item7.text)
                                                                


    





#'__class__', '__copy__', '__deepcopy__', '__delattr__', '__delitem__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', 
#'__getattribute__', '__getitem__', '__getstate__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__len__', 
#'__lt__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__setitem__', '__setstate__', '__sizeof__',
# '__str__', '__subclasshook__', 'append', 'attrib', 'clear', 'extend', 'find', 'findall', 'findtext', 'get', 'getchildren', 'getiterator', 
# 'insert', 'items', 'iter', 'iterfind', 'itertext', 'keys', 'makeelement', 'remove', 'set', 'tag', 'tail', 'text']




