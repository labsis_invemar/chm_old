import os
import sys
import datetime
from termcolor import colored

sys.path.append(
    os.path.dirname(
        os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    )
)

from chm.settings.prod  import BASE_DIR, DATABASES

"""
    class for dump and restore backup of mongodb
"""

class Backup(object):

    def __init__(self):
        self.dumps_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'dumps')
        self.params = {
            #'DANGO':'MONGO'
            'HOST':'--host',
            'PORT':'--port',
            'USER':'--username',
            'PASSWORD':'--password',
            'NAME':'--db',
        }

    def dump(self):
        for conn_name, conn_params in DATABASES.items():
            command = self.dump_get_command(conn_params)
            self.exe_command(command)

    def restore(self):
        for conn_name, conn_params in DATABASES.items():
            command = self.restore_get_command(conn_params)
            if command:
               self.exe_command(command)
    
    def dump_get_command(self, conn_params):
        command_params = ['mongodump',]
        command_params += self.get_params_command(conn_params)
        command_params.append('--out {}/{}'.format(self.dumps_dir, self.dump_get_dir_name()))
        return ' '.join(command_params)

    def restore_get_command(self, conn_params):
        dump = self.restore_input_dump()
        if dump:
            command_params = ['mongorestore','--drop']
            command_params += self.get_params_command(conn_params)
            if 'NAME' in conn_params and conn_params['NAME']:
                command_params.append(
                    '{}/{}/{}'.format(self.dumps_dir, dump, conn_params['NAME'])
                    )
            else:
                command_params.append(self.dumps_dir)
            return ' '.join(command_params)
        return None

    def get_params_command(self, conn_params):
        command_params = []
        for django_param, mongo_param in self.params.items():
            if django_param in conn_params and conn_params[django_param]:
                command_params.append('{} {}'.format(mongo_param, conn_params[django_param]))
        return command_params

    def exe_command(self, command):
        os.system(command)
        self.print_msg('Command executed: ')
        self.print_msg(command)

    def dump_get_dir_name(self):
        return datetime.datetime.now().strftime("%Y%m%d%H%M%S")
    
    def restore_input_dump(self):
        dump = index_dump = None
        dumps = os.listdir(self.dumps_dir)
        if dumps:
            for i, dump_name in enumerate(dumps):
                self.print_msg('{} {}'.format(i, dump_name))
            self.print_msg('Enter the dump to restore (only the left number):')
            try:
                index_dump = int(input())
            except Exception as err:
                self.print_input_error('green')
                return None
            if index_dump > -1:
                try:
                    dump = dumps[index_dump]
                except Exception as err:
                    self.print_input_error()
                    return None
            else:
                self.print_input_error()
        else:
            self.print_msg('There is not dumps avalibles. Create it with dump.py','red')
        return dump

    def print_input_error(self, color='red'):
        self.print_msg('Entered dump is not valid', color)
        
    def print_msg(self, text, color='yellow'):
        print(colored(text, color))