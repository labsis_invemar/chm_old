from django.contrib import admin
from .models import *


admin.site.register(Webservicechm)
admin.site.register(Headers)
admin.site.register(DetailField)
admin.site.register(Scraping)
admin.site.register(ScrapingField)
admin.site.register(ApiReader)
