# Generated by Django 2.0 on 2019-06-04 13:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('admins', '0011_auto_20190531_1507'),
    ]

    operations = [
        migrations.AddField(
            model_name='scraping',
            name='rootItem',
            field=models.CharField(default='', max_length=200, verbose_name='Root url item'),
            preserve_default=False,
        ),
    ]
