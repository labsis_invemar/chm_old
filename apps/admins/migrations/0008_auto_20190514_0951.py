# Generated by Django 2.0 on 2019-05-14 14:51

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('admins', '0007_auto_20190510_1039'),
    ]

    operations = [
        migrations.RenameField(
            model_name='scraping',
            old_name='startUlr',
            new_name='startulr',
        ),
    ]
