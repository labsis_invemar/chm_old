# Generated by Django 2.0 on 2019-05-10 15:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('admins', '0006_scraping_scrapingfield'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='scrapingfield',
            name='webservice',
        ),
        migrations.AddField(
            model_name='scrapingfield',
            name='scraping',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.DO_NOTHING, to='admins.Scraping'),
            preserve_default=False,
        ),
    ]
