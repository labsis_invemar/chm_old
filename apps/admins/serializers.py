from .models import *
from rest_framework import serializers
from django.contrib.auth.models import User


class WebServiceSerializer(serializers.ModelSerializer):
    update = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S")
    class Meta:
        model = Webservicechm
        fields = '__all__'


class HeadersSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Headers
        fields = '__all__'


class DetailfiledSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = DetailField
        fields = '__all__'


class ScrapingSerializer(serializers.ModelSerializer):
    update = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S")
    class Meta:
        model = Scraping
        fields = '__all__'


class ScrapingFieldSerializer(serializers.ModelSerializer):

    class Meta:
        model = ScrapingField
        fields = '__all__'


class UpdloadFiledSerializer(serializers.ModelSerializer):
    class Meta:
        model = UpdloadFiled
        fields = '__all__'


class OaiRepoSerializer(serializers.ModelSerializer):
    class Meta:
        model = OaiRepo
        fields = '__all__'


class ApiReaderSerializer(serializers.ModelSerializer):
    # update = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S")
    class Meta:
        model = ApiReader
        fields = '__all__'

class ApiReaderFieldSerializer(serializers.ModelSerializer):
    class Meta:
        model = ApiReaderField
        fields = '__all__'