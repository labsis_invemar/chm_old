from django.db import models


class Webservicechm(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200, null=False, verbose_name='Name' )
    endpoint = models.CharField(max_length=200, null=False, verbose_name='End Point')
    method =  models.CharField(max_length=200, null=False, verbose_name='Method')
    pagination = models.CharField(max_length=200, null=False, verbose_name='Pagination') 
    page_key = models.CharField(max_length=200, null=True, blank=True, verbose_name='Key Page') 
    page_init = models.CharField(max_length=200, null=True, blank=True, verbose_name='Init Page')
    page_end = models.CharField(max_length=200, null=True, blank=True, verbose_name='End Page')
    category = models.CharField(max_length=200, null=False, verbose_name='Category')
    update = models.DateTimeField(null=False, auto_now=True, verbose_name='Update Harvesting')
    acumulado = models.IntegerField(null=False, verbose_name='Total Data')
    def __str__(self):
        return self.name


class Headers(models.Model):
    id = models.AutoField(primary_key=True)
    key = models.CharField(max_length=200, null=False, verbose_name='key' )
    value = models.CharField(max_length=200, null=False, verbose_name='value' )
    webservice = models.ForeignKey(Webservicechm,models.DO_NOTHING,)
   
    def __str__(self):
        return "key:"+self.key +"-value:"+self.value+"-webservice:"+self.webservice.name


class DetailField(models.Model):
    id = models.AutoField(primary_key=True)
    field_chm = models.CharField(max_length=200, null=False, verbose_name='Field CHM' )
    field_web = models.CharField(max_length=200, null=False, verbose_name='Field Web Service' )
    webservice = models.ForeignKey(Webservicechm,models.DO_NOTHING)

    def __str__(self):
        return "key:"+self.field_chm +"-value:"+self.field_web+"-webservice:"+self.webservice.name

    
class Scraping(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200, null=False, verbose_name='Name' )
    dominio = models.CharField(max_length=200, null=False, verbose_name='Dominio')
    starturl =  models.CharField(max_length=200, null=False, verbose_name='Start url')
    rootItem = models.CharField(max_length=200, null=False, verbose_name='Root url item')
    classItem = models.CharField(max_length=200, null=False, verbose_name='Class Item') 
    category = models.CharField(max_length=200, null=False, verbose_name='Category')
    pagination = models.CharField(max_length=200, null=False, verbose_name='Pagination') 
    page_key = models.CharField(max_length=200, null=True, blank=True, verbose_name='Key Page') 
    page_init = models.CharField(max_length=200, null=True, blank=True, verbose_name='Init Page')
    page_end = models.CharField(max_length=200, null=True, blank=True, verbose_name='End Page')
    increment = models.CharField(max_length=200, null=True, blank=True, verbose_name='Increment')
    connector = models.CharField(max_length=200, null=True, blank=True, verbose_name='Connector')
    update = models.DateTimeField(null=False, auto_now=True, verbose_name='Update Harvesting')
    acumulado = models.IntegerField(null=False, default=0, verbose_name='Total Data')
    def __str__(self):
        return self.name


class ScrapingField(models.Model):
    id = models.AutoField(primary_key=True)
    field_chm = models.CharField(max_length=200, null=False, verbose_name='Field CHM' )
    root_item = models.CharField(max_length=200, null=False, verbose_name='Root URL Field' )
    field_web = models.CharField(max_length=200, null=False, verbose_name='Field Web Service' )
    type_field = models.CharField(max_length=200, null=False, verbose_name='Type Field' )
    size_text = models.CharField(max_length=200, null=False, verbose_name='Size Text' )
    scraping = models.ForeignKey(Scraping,models.DO_NOTHING)

    def __str__(self):
        return "key:"+self.field_chm +"-value:"+self.field_web+"-webservice:"+self.scraping.name


class UpdloadFiled(models.Model):
    id = models.AutoField(primary_key=True)    
    name = models.CharField(max_length=200, null=False, verbose_name='Name' )
    category = models.CharField(max_length=200, null=False, verbose_name='Category')
    records_read = models.CharField(max_length=200, null=False, verbose_name='Read records' )
    records_s = models.CharField(max_length=200, null=False, verbose_name='Stored records' )
    records_r = models.CharField(max_length=200, null=False, verbose_name='Repeated records' )
    records_i = models.CharField(max_length=200, null=False, verbose_name='Invalid records' )
    date_regis = models.CharField(max_length=200, null=False, verbose_name='Date' ) 
    datadwl = models.CharField(max_length=600, null=False, verbose_name='datadwl' )
    headdwl = models.CharField(max_length=600, null=False, verbose_name='headdwl' )
    
    def __str__(self):
        return self.name

class OaiRepo(models.Model):

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200, null=False, verbose_name='Name' )
    category = models.CharField(max_length=200, null=False,default="documents", verbose_name='Category')
    metadata_prefix = models.CharField(max_length=200,default='oai_dc', null=False, verbose_name='Metadata Prefix' )
    domain = models.CharField(max_length=200, null=False, verbose_name='Domain' )
    url_complement = models.CharField(max_length=200, null=False, verbose_name='Url Complement' )
    update = models.CharField(max_length=4, verbose_name='Force Update')
    data_set = models.CharField(max_length=200, verbose_name='Data Set')
    limit = models.IntegerField(null=False, verbose_name='Limit')
    last_harvest = models.DateTimeField(verbose_name='Last Harvest', default=None)
    shared = models.CharField(verbose_name='Shared',max_length=6,default='No')
    acumulado = models.IntegerField(null=False, default=0, verbose_name='Total Data')
    def __str__(self):
        return self.name + self.shared



class ApiReader(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200, null=False, verbose_name='Name' )
    dominio = models.CharField(max_length=200, null=False, verbose_name='Dominio')
    url =  models.CharField(max_length=200, null=False, verbose_name='Url')
    item = models.CharField(max_length=200, null=False, verbose_name='Item')
    classItem = models.CharField(max_length=200, null=False, verbose_name='Class Item') 
    category = models.CharField(max_length=200, null=False, verbose_name='Category')
    def __str__(self):
        return self.name

        
class ApiReaderField(models.Model):
    id = models.AutoField(primary_key=True)
    field_chm = models.CharField(max_length=200, null=False, verbose_name='Field CHM' )
    item = models.CharField(max_length=200, null=False, verbose_name='Item api' )
    type_field = models.CharField(max_length=200, null=False, verbose_name='Type Field' )
    size_text = models.CharField(max_length=200, null=False, verbose_name='Size Text' )
    apireader = models.ForeignKey(ApiReader,models.DO_NOTHING)
    def __str__(self):
        return "key:"+self.field_chm +"-value:"+self.item+"-webservice:"+self.apireader.name