from django.urls import path, include
from .views import (OaiRepoCreate,
    WebServiceCreate, deleterdr, get_oai,
    ScrapingCreate, list_admin,
    get_webservice, get_scraping,
    user_save, get_user,save_data_update_file,refreshWebService,deleteSource, 
    ApiReaderCreate, get_apireader, deleterdr, updaterdr
    
    )

app_name = 'admins' 

urlpatterns = [
    path('list_admin/', list_admin, name='list_admin'),
    path('user/', user_save, name='user_save'),
    path('uploadfile/',save_data_update_file,name="index" ),
    path('webservice/', WebServiceCreate, name='webservice'),
    path('scraping/', ScrapingCreate, name='scraping'),
    path('oai/', OaiRepoCreate, name='webservice'),
    path('get_webservice/', get_webservice, name='get_webservice'),
    path('get_oai/', get_oai, name='get_oai_repository'),
    path('get_scraping/', get_scraping, name='get_scraping'),
    path('get_user/', get_user, name='get_user'),
    path('refreshWebService/', refreshWebService, name="refreshWebService"),
    path('deleteSource/', deleteSource, name="deleteSource"),
    path('apireader/', ApiReaderCreate, name='apireader'),
    path('get_apireader/', get_apireader, name='get_apireader'),
    path('deleterdr/', deleterdr, name="deleterdr"),
     path('updaterdr/', updaterdr, name="updaterdr"),
     
]