from .models import *
from ..main.models import *
import xlwt
import unidecode
import openpyxl
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
import base64
import datetime
from django.db.models.signals import post_save
from django.dispatch import receiver
from pprint import pprint
import bson

def get_filters(worksheet):
        filters={}     
        for i, row in enumerate(worksheet.iter_rows()):             
            if i == 0:
                for cell in row:
                    if cell.value is not None:                        
                        filters[cell.value.lower().replace(" ", "")]=cell.column            
        # print('filtrosss>>>',filters)
        return filters

def get_data_excel(filters, worksheet, type_worksheet):
    start, stop = 0, 10
    obj_data = {}
    data = []   
    data_fail = []             
    for index, row in enumerate(worksheet.iter_rows()):
        if (start < index):
            obj_data = {}
            for cell in row:
                if(cell.value is not None):                            
                    for attr, value in filters.items():
                        if(value == cell.column):
                            obj_data[attr] = cell.value                
            
            if create_id_for_obj(obj_data, type_worksheet)  is not None:
                data.append(create_id_for_obj(obj_data, type_worksheet))       
            else:
                if(len(obj_data.keys())>0):
                    data_fail.append(obj_data)        
    return {
        'data':data,
        'data_fail':data_fail
    }

def instance_models(filters, type_worksheet):
    # aqui simpre se manda con sw == 2 para que la funcion retorne simpre los modelos        
    return keys_for_model(filters, type_worksheet, 2)


def save_data(model,data,obj_saved_):
    if(model.__name__ == 'Training'):
        traning = model()
        traning.id_nombre = data['id_nombre'] if 'id_nombre' in data else None
        traning.ubicacion = data['state'] # if 'address' in data else None
        traning.facultad = data['department'] if 'department' in data else None
        traning.institucion = data['institution'] if 'institution' in data else None
        traning.order = 2
        traning.telefono = data['phone'] if 'phone' in data else None
        traning.nombre = data['institution']+" -> "+"  "+data['department']+" -> "+"  "+data['program'] 
        traning.pais = data['state'] if 'state' in data else None
        traning.tipo = ' Formal '+data['type'] if 'type' in data else None
        traning.url = data['url'] if 'url' in data else None
        traning.email = data['email'] if 'email' in data else None
        traning.imagen = "/static/main/index/images/training-default.png"
        traning.source = obj_saved_.name
        traning.source_id = obj_saved_.id
        traning.method_harvesting = 'upload'
        try:
            duplicate =  model.objects.filter(id_nombre=data['id_nombre'])
        except Exception as ex:
            print('error al buscar trainign',ex)
                
        if(len(duplicate)>0):            
            return 1
        else: 
            try:
                traning.save()
            except Exception as ex:
                print('error al guardar triangin',ex)
            
            return 0 
        
    elif (model.__name__ == 'Vessel'):    
        vessel = model()
        vessel.id_nombre = data['id_nombre'] if 'id_nombre' in data else None
        vessel.pais = data['state'] if 'state' in data else None
        vessel.nombre = data['name'] if 'name' in data else None
        vessel.descripcion = data['resume'] if 'resume' in data else None
        vessel.equipi_nav = data['naviequipment'] if 'naviequipment' in data else None
        vessel.cantidad_trip = data['crew'] if 'crew' in data else None
        vessel.institucion = data['institution'] if 'institution' in data else None
        vessel.categoria = data['category'] if 'category' in data else None
        vessel.estado = data['status'] if 'status' in data else None
        vessel.actividad = data['activity'] if 'activity' in data else None
        vessel.envergadura = data['size'] if 'size' in data else None
        vessel.source = obj_saved_.name
        vessel.source_id = obj_saved_.id
        vessel.method_harvesting = 'upload'
        try:
            duplicate =  model.objects.filter(id_nombre=data['id_nombre'])        
        except Exception as ex:
            print('error al buscar vessel',ex)
        
        if(len(duplicate)>0):            
            return 1
        else: 
            try:
                vessel.save()
            except Exception as ex:
                print('error al guardar vessel',ex)
            return 0 
        
def type_to_model(type_worksheet):
    print('type_model------',type_worksheet)
    if(type_worksheet == 'vess'):
        return Vessel
    elif (type_worksheet == 'geo'):
        return Platformgeo
    elif(type_worksheet == 'experts'):
        return People
    elif(type_worksheet == 'documents'):
        return Document
    elif(type_worksheet == 'training'):
        return Training
    elif (type_worksheet == 'laboratories'):
        return Laboratory
    elif (type_worksheet == 'institutions'):
        return Institution

def keys_for_model(filters, type_worksheet, sw):
    if type_worksheet.lower() == 'training':
        if('url' in filters and 'program' in filters and 'type' in filters and 'department' in filters and 'institution' in filters):
            if sw == 1:
                return {'valid':True, 'type_worksheet':type_worksheet}
            else:
                return  Training
    elif type_worksheet.lower() == 'vess':             
        if('state' in filters and 'name' in filters and 'resume' in filters):                
            if sw == 1:
                return {'valid':True, 'type_worksheet':type_worksheet}
            else:                    
                return  Vessel


def create_id_for_obj(obj_data, type_worksheet):
    objw = keys_for_model(obj_data, type_worksheet, 1)       
    if (objw is not None  and  objw['valid'] == True and objw['type_worksheet'] == 'training'):       
        obj_data['id_nombre'] = unidecode.unidecode(
            obj_data['program'][0:10].upper().replace(" ", "")+
            obj_data['type'].upper().replace(" ", "")+
            obj_data['department'][-10:].upper().replace(" ", "")+
            obj_data['institution'][-10:].upper().replace(" ", ""))
        return obj_data
    if (objw is not None  and  objw['valid'] == True and objw['type_worksheet'] == 'vess'):
        obj_data['id_nombre'] = unidecode.unidecode(
            obj_data['name'].replace(" ", "").upper()+
            obj_data['state'].replace(" ", "").upper())
        return obj_data


def validate_sheets(sheets,wb,type_worksheet):
    array_sheet_invalid = []
    for sheet in sheets:
        filters = {}        
        model = None
        try:                
            worksheet = wb[sheet]
        except Exception as e:
            return {'error':True, 'msg':'Error when trying to open the worksheet','class':'alert-danger','data':None}

        filters = get_filters(worksheet)                                    
        model = instance_models(filters, type_worksheet)

        if (model is None):
            array_sheet_invalid.append(sheet)
        
    if (len(array_sheet_invalid)>0):
        return {'error':False, 'msg':'','class':'alert-danger','data':array_sheet_invalid}
        # return {array_sheet_invalid}
        


def store_data_admin_uploadFiled(model,category,name_file,array_data_s,array_data_r,array_data_fail,filters,obj_saved_=None):    
        if(obj_saved_ is None):
            objfiled = UpdloadFiled()
            objfiled.category = category.__name__ 
            objfiled.name = name_file
            objfiled.records_s = array_data_s
            objfiled.records_r = array_data_r
            objfiled.records_i = len(array_data_fail)
            objfiled.records_read = array_data_s + array_data_r +len(array_data_fail)
            objfiled.date_regis = datetime.datetime.now().strftime("%Y-%m-%d")
            objfiled.datadwl = array_data_fail
            objfiled.headdwl = filters
            try:
                objfiled.save()
            except expression as identifier:
                print('error al guarda uploadFIle')
            
            return objfiled
        else:             
            dict_= {
                'records_s': array_data_s,
                'records_r': array_data_r,
                'records_i': len(array_data_fail),
                'records_read': array_data_s + array_data_r +len(array_data_fail),
                'datadwl':array_data_fail,
                'headdwl':filters
                }
            try:
                model.objects.filter(pk=obj_saved_.id).update(**dict_)
            except Exception as ex:
                print('error al editar uploadfile')           
            return obj_saved_

def export_data_xls(type_worksheet, filters, data):
    # print('filters>>>',filters)
    # print('hoja de trabajo>>>',type_worksheet)
    # print('datos>>>',data[0])
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename='+type_worksheet+'_failed.xls'
    
    
    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet(type_worksheet)

    # Sheet header, first row
    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    for i, attr in enumerate(filters):
        # print('index>>>',i)
        # print('item>>>',attr)
        ws.write(row_num, i, attr, font_style)
    

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()

    rows = data
    

    for row in rows:
        row_num += 1
        for i, attr in enumerate(filters):
            if attr in row:
                ws.write(row_num, i, row[attr], font_style)
            else:
                ws.write(row_num, i, '-----', font_style) 
        
                

    wb.save(response)        
    return response