from urllib import response
import openpyxl
import http.client
import requests
from django.http import (
    HttpResponse, HttpResponseRedirect, JsonResponse
    )
from django.shortcuts import redirect, render
from django.utils.decorators import method_decorator
from django.views import View
import unidecode
from pprint import pprint
from .forms import UploadFileForm
from apps.main.models import *
from apps.main.serializers import UserSerializer, DocumentsSerializer
from .serializers import *
from .models import *
from apps.admins.models import *
import xlwt
from rest_framework import generics
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from chm.settings.base import BASE_DIR,PATH_ENV
from apps.main.helper import (
    get_paginate_num, 
    get_paginate_data, 
    get_model_fields_select,
    get_data_list,
    get_serializer_class,
    get_fields_list,
    obj_set_data
    )
from django.contrib.auth.models import User
from apps.main.helper import get_paginate_num,get_paginate_data,get_model_fields_select
# from apps.admins.pycsw.GetXMLcsw import GetXMLcsw
from apps.admins.csw.GetXMLcsw import GetXMLcsw
from django.contrib.auth.models import User
import json
import os
import sys
from pprint import pprint
from .helper import *
from datetime import datetime
from django.utils import timezone
import subprocess
from django.db.models import Q
from oaipmh.client import Client
from oaipmh.metadata import MetadataRegistry, MetadataReader


oai_dc_reader = MetadataReader(
    fields={
    'title':       ('textList', 'oai_dc:dc/dc:title/text()'),
    'creator':     ('textList', 'oai_dc:dc/dc:creator/text()'),
    'subject':     ('textList', 'oai_dc:dc/dc:subject/text()'),
    'description': ('textList', 'oai_dc:dc/dc:description/text()'),
    'publisher':   ('textList', 'oai_dc:dc/dc:publisher/text()'),
    'contributor': ('textList', 'oai_dc:dc/dc:contributor/text()'),
    'date':        ('textList', 'oai_dc:dc/dc:date/text()'),
    'type':        ('textList', 'oai_dc:dc/dc:type/text()'),
    'format':      ('textList', 'oai_dc:dc/dc:format/text()'),
    'identifier':  ('textList', 'oai_dc:dc/dc:identifier/text()'),
    'source':      ('textList', 'oai_dc:dc/dc:source/text()'),
    'language':    ('textList', 'oai_dc:dc/dc:language/text()'),
    'relation':    ('textList', 'oai_dc:dc/dc:relation/text()'),
    'coverage':    ('textList', 'oai_dc:dc/dc:coverage/text()'),
    'rights':      ('textList', 'oai_dc:dc/dc:rights/text()'),
    },
    namespaces={
    'oai_dc': 'http://www.openarchives.org/OAI/2.0/oai_dc/',
    'dc' : 'http://purl.org/dc/elements/1.1/'}
    )

def parse_JSONLD(record, url):
    response=""
    try:
        record_metadata=record[1].getMap()
        JSON = {
                '@context' : {
                    '@vocab' : 'https://schema.org/'
                },
                '@type' : 'CreativeWork',
                '@id' : record[0].identifier() ,
                'name' : record_metadata["title"][0].strip(),
                'description' : ', '.join([str(elem) for elem in record_metadata["description"]]),
                'url' :  url.split(r'/^https?:\/\/.*\w+\.\w+/')[0] +'/handle/'+ record[0].identifier().split(':')[-1],
                'identifier' : {
                    '@id' : "https://hdl.handle.net/" + record[0].identifier().split(':')[-1],
                    '@type' : 'PropertyValue',
                    'propertyID' : 'https://hdl.handle.net/',
                    'value' : record[0].identifier().split(':')[-1],   
                    'url' : "https://hdl.handle.net/" + record[0].identifier().split(':')[-1]
                }
            }
        rec_keys=record_metadata.keys()
        if "creator" in rec_keys and len(record_metadata["creator"]):
            authors=[]
            for x in record_metadata["creator"]:
                author = {
                    '@type' : 'Person',
                        'name' : x
                        }
                authors.append(author)
            JSON['author'] = authors
        contributors=[]
        if "contributors" in rec_keys and len(record_metadata["contributors"]) :

            for x in record_metadata["contributors"]:
                contributor = {
                    '@type' : 'Organization',
                    'name' : x
                    }
                contributors.append(contributor)

        if "publisher" in rec_keys and len(record_metadata["publisher"]) :

            for x in record_metadata["publisher"]:
                publisher = {
                    '@type' : 'Organization',
                    'name' : x
                    }
                contributors.append(publisher)
            
        if (len(contributors)):
            JSON['contributor'] = contributors
                
        if "date" in rec_keys and len(record_metadata["date"]):
            JSON['datePublished'] = record_metadata["date"][-1]
        if "language" in rec_keys and len(record_metadata["language"]):
            JSON['inLanguage'] = record_metadata["language"]
        if "subject" in rec_keys and len(record_metadata["subject"]):
            JSON['keywords'] = record_metadata["subject"]
        if "spatial" in rec_keys and len(record_metadata["spatial"]):
            JSON['spatialCoverage'] = record_metadata["spatial"]
        if "temporal" in rec_keys and len(record_metadata["temporal"]):
            JSON['temporalCoverage'] = record_metadata["temporal"]
        if "source" in rec_keys and len(record_metadata["source"]):
            JSON['isPartOf'] = record_metadata["source"]
       
        if "relation"in rec_keys and len(record_metadata["relation"]):
            JSON['isPartOf'] = record_metadata["relation"]
        if "coverage" in rec_keys and len(record_metadata["coverage"]):
            JSON['coverage'] = record_metadata["coverage"]
        if "rights" in rec_keys and len(record_metadata["rights"]):
            JSON['license'] = record_metadata["rights"]

        if "format" in rec_keys and len(record_metadata["format"]) and len(list(filter((lambda x: x.find("/")),record_metadata["format"]))):
            JSON['encodingFormat'] = list(filter((lambda x: x.find("/")),record_metadata["format"]))[0]

        
        response = json.dumps(JSON).replace('\"', '"')
    except:
        print("Error parsing JSONLD")
        response = None
    return response

def record_exist(name, record,force_update,url,domain, shared):
    try:
        nombre=str(record[1].getField('title')[0]).strip().replace("'","").replace('"',"").replace("\\","").replace("[","").replace("]","")
        if nombre=='':
            return True
        queryset=Document.objects.get((Q(nombre__icontains = nombre.split("(")[0].split(")")[0]) | Q(url= "https://hdl.handle.net/" + record[0].identifier().split(':')[-1]) | Q(url= list(filter(lambda x: x.startswith('http'), record[1].getField('identifier')))[0])))
        print("ESTE ES EL NUEVO PRINT:::1"+queryset)
        if queryset.exists():
            if force_update:
                queryset.delete()
                doc ={} 
                doc["nombre"] = nombre
                doc["id_nombre"] = nombre.encode('ascii', 'ignore').decode().upper().replace(" ","").replace(".","").replace(",","").replace("/","").replace("(","").replace(")","")
                doc["imagen"]= '/static/main/index/images/doc-default.png'
                doc["url"]= list(filter(lambda x: x.startswith('http'), record[1].getField('identifier')))[0] if list(filter(lambda x: x.startswith('http'), record[1].getField('identifier'))) !=[] else "https://hdl.handle.net/" + record[0].identifier().split(':')[-1]
                doc["fecha"]= record[1].getField('date')[0] if record[1].getField('date')!=[] else ""
                doc["resumen"]= ', '.join([str(elem) for elem in record[1].getField('description')])[:3000] if record[1].getField('description')!= [] else "N/D"
                doc["pais"]= ', '.join([str(elem) for elem in record[1].getField('coverage')])[:300] if record[1].getField('coverage')!= [] else "."
                doc["source"]=  name
                # doc["shared"]= shared
                doc["method_harvesting"] = 'OAI_PMH'
                doc["json_ld"]=parse_JSONLD(record,url)
                serializer=DocumentsSerializer(data=doc)
                if serializer.is_valid():
                    serializer.save()
                    return True
                
            elif queryset.filter(json_ld__icontains="context").exists() :
                return True
            else:
                json_ld_new=parse_JSONLD(record, url)
                doc_ex= queryset.exclude(json_ld__icontains="context").update(json_ld=json_ld_new)
                return True
        queryset= Document.objects.filter(id_nombre__icontains = nombre.encode('ascii', 'ignore').decode().upper().replace(" ","").replace(".","").replace(",","").replace("/","").replace("(","").replace(")",""))
        if queryset.exists() :
            if force_update:
                queryset.delete()
                doc ={} 
                doc["nombre"] = nombre 
                doc["id_nombre"] = nombre.encode('ascii', 'ignore').decode().upper().replace(" ","").replace(".","").replace(",","").replace("/","").replace("(","").replace(")","")
                doc["imagen"]= '/static/main/index/images/doc-default.png'
                doc["url"]= list(filter(lambda x: x.startswith('http'), record[1].getField('identifier')))[0] if list(filter(lambda x: x.startswith('http'), record[1].getField('identifier'))) !=[] else "https://hdl.handle.net/" + record[0].identifier().split(':')[-1]
                doc["fecha"]= record[1].getField('date')[-1] if record[1].getField('date')!=[] else ""
                doc["resumen"]= ', '.join([str(elem) for elem in record[1].getField('description')])[:3000] if record[1].getField('description')!= [] else "N/D"
                doc["pais"]= ', '.join([str(elem) for elem in record[1].getField('coverage')])[:300] if record[1].getField('coverage')!= [] else "."
                doc["source"]= name
                doc["shared"]= shared
                doc["method_harvesting"] = 'OAI_PMH'
                doc["json_ld"]=parse_JSONLD(record,url)
                serializer=DocumentsSerializer(data=doc)
                if serializer.is_valid():
                    serializer.save()
            return True
        return False
    except:
        return False

def WebServiceOAI(name,domain,complement,limit,prefix,dataset,update, acumulado):
    URL = domain + complement
    force_update= True if update=="Yes" else False
    registry = MetadataRegistry()
    registry.registerReader(prefix, oai_dc_reader)
    client = Client(URL, registry)
    doc_length = 0
    num_records = 0
    documents=[]
    records=[]
    errors=[]
    repeated_records=[]
    if dataset!="":
        records=client.listRecords(metadataPrefix=prefix, set=dataset)
    else:
        records= client.listRecords(metadataPrefix=prefix)
        
    for record in records:
        try:
            if num_records%200==0:
                print("num records:::::",num_records)
            if record[0].isDeleted() or record[1].getMap().keys()==[] :
                num_records+=1
                continue
            if record_exist(name, record, force_update,URL,domain) :
                if force_update:
                    doc_length+=1
                num_records+=1 
                repeated_records.append(str(record[1].getField('title')[0]).strip().replace("'","").replace("\"","").replace("\\","").replace("[","").replace("]",""))
                continue
            else:
                doc ={}
                doc["nombre"] = str(record[1].getField('title')[0]).strip().replace("'","").replace("\"","").replace("\\","").replace("[","").replace("]","")
                doc["id_nombre"] = doc["nombre"].encode('ascii', 'ignore').decode().upper().replace(" ","").replace(".","").replace(",","").replace("/","").replace("(","").replace(")","")
                doc["imagen"]= '/static/main/index/images/doc-default.png'
                doc["url"]= list(filter(lambda x: x.startswith('http'), record[1].getField('identifier')))[0] if list(filter(lambda x: x.startswith('http'), record[1].getField('identifier'))) !=[] else "https://hdl.handle.net/" + record[0].identifier().split(':')[-1]
                doc["fecha"]= record[1].getField('date')[-1] if record[1].getField('date')!=[] else ""
                doc["resumen"]= ', '.join([str(elem) for elem in record[1].getField('description')])[:3000] if record[1].getField('description')!= [] else "N/D"
                doc["pais"]= ', '.join([str(elem) for elem in record[1].getField('coverage')])[:300] if record[1].getField('coverage')!= [] else "."
                doc["source"]= name
                doc["method_harvesting"] = 'OAI_PMH'
                doc["json_ld"]=parse_JSONLD(record,URL)
                serializer=DocumentsSerializer(data=doc)
                print("num records::15:::")
                if serializer.is_valid():
                    serializer.save()
                if serializer.errors:
                    errors.append(serializer.errors)
                doc_length+=1
                num_records+=1 
            if limit>0 and doc_length>=limit:
                
                break
        except Exception as e:
            print(e)
            print(num_records)
            errors.append({"metadata":record[1].getMap(), "header":record[0].identifier()})
            continue
    response={
        "data":". <br/><b>Documents Inserted: </b>" + str(doc_length) + "<br/> <b>Documents harvested: </b>" + str(num_records),
        "Error":errors,
        "acumulado": str(num_records)
    }
    
    if repeated_records!=[]:
        response["data"]=response["data"]+"<br/><b>Repeated records: </b> <br/> <ul><li> " + "</li><li>".join(repeated_records) + "</li></ul>"
    return (response)

@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def WebServiceCreate(request):
    data = json.loads(request.body.decode('utf-8'))
    mensaje = "Error"
    web = None
    if (data.get("id")==''):
        web = Webservicechm()
        web.name = data.get("name")   
        web.endpoint = data.get("endpoint")  
        web.category = data.get("category")  
        web.pagination = data.get("pagination")
        web.page_key = data.get("key_page")
        web.page_init = data.get("init_page")
        web.page_end = data.get("end_page")
        web.method = data.get("method")
        web.save()
        save_header(data.get("headers"),web)
        save_category_webservice(data.get("categoryField"),web)
        mensaje = "was created correctly"
    else:
        Webservicechm.objects.filter(id= int(data.get('id'))).update(
            name = data.get("name"),   
            endpoint = data.get("endpoint"), 
            category = data.get("category"),  
            pagination = data.get("pagination"),
            page_key = data.get("key_page"),
            page_init = data.get("init_page"),
            page_end = data.get("end_page"),
            method = data.get("method"),  
        )
        web = Webservicechm.objects.get(id = int(data.get('id')))
        Headers.objects.filter(webservice =web ).delete()
        save_header(data.get("headers"),web)
        DetailField.objects.filter(webservice =web ).delete()
        save_category_webservice(data.get("categoryField"),web)
        mensaje = "was edited correctly"
    
    
    serializer_class = WebServiceSerializer(web)   
    return Response({'data_web':serializer_class.data,'result':mensaje}, status=200)

def save_category_webservice(categoryField,web):
    for cfield in categoryField:
        field = DetailField()
        field.field_chm = cfield['fieldlocal']
        field.field_web = cfield['fieldweb']
        field.webservice = web
        field.save()

def save_header(headers,web):
    for header in headers:
        head = Headers()
        head.key = header['headerkey']
        head.value = header['headervalue']
        head.webservice = web
        head.save()

@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def ScrapingCreate(request):
    data = json.loads(request.body.decode('utf-8'))
    mensaje = "Error"
    scr = None
    if (data.get("id")==''):
        scr = Scraping()
        scr.name = data.get("name")   
        scr.dominio = data.get("dominio")  
        scr.category = data.get("category")  
        scr.starturl = data.get("starturl")
        scr.rootItem = data.get("rootItem")
        scr.pagination = data.get("pagination")
        scr.page_key = data.get("key_page")
        scr.page_init = data.get("init_page")
        scr.page_end = data.get("end_page")
        scr.increment = data.get("increment")
        scr.connector = data.get("connector")
        scr.classItem = data.get("classItem")
        scr.save()
        
        save_categoryField_scraping(data.get("categoryField"),scr)
        mensaje = "Successfully saved"
    else:
        Scraping.objects.filter(id=int(data.get("id") )).update(
            name = data.get("name"),   
            dominio = data.get("dominio"),  
            category = data.get("category"),  
            pagination = data.get("pagination"),
            page_key = data.get("key_page"),
            page_init = data.get("init_page"),
            page_end = data.get("end_page"),
            increment = data.get("increment"),
            connector = data.get("connector"),
            starturl = data.get("starturl"),
            rootItem = data.get("rootItem"),
            classItem = data.get("classItem")
        )
        scr = Scraping.objects.get(id = int(data.get("id")))

        ScrapingField.objects.filter(scraping=scr).delete()
        save_categoryField_scraping(data.get("categoryField"),scr)
        mensaje = "Successfully edited"
    
    
    serializer_class = ScrapingSerializer(scr)   
    return Response({'data_scr':serializer_class.data,'result':mensaje}, status=200)


def save_categoryField_scraping(categoryField,scr):
    for cfield in categoryField:
        field = ScrapingField()
        field.field_chm = cfield['fieldlocal']
        field.field_web = cfield['fieldweb']
        field.type_field = cfield['typefield']
        field.size_text = cfield['sizetext']
        field.root_item = cfield['rootitem']
        field.scraping = scr
        field.save()

@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def OaiRepoCreate(request):
    data = json.loads(request.body.decode('utf-8'))
    mensaje = "Error"
    oai = None
    cant = 0
    if (data.get("id")==''):
        oai = OaiRepo()
        oai.name=data.get("name")
        oai.category=data.get("category")
        oai.metadata_prefix=data.get("metadata_prefix")
        oai.domain=data.get("domain")
        oai.url_complement=data.get("url_complement")
        oai.data_set=data.get("data_set")
        oai.limit=data.get("limit")
        oai.acumulado=0
        if oai.domain=="http://aquadocs.org/":
            oai.shared=False
        else:
            oai.shared=True
        oai.save()
        mensaje = "was created correctly"
        
    else:
        OaiRepo.objects.filter(id= int(data.get('id'))).update(
            name=data.get("name"),
            category=data.get("category"),
            metadata_prefix=data.get("metadata_prefix"),
            domain=data.get("domain"),
            url_complement=data.get("url_complement"),
            data_set=data.get("data_set"),
            update=data.get("update"),
            limit=data.get("limit"),
            # acumulado = data.get("acumulado")
            
        )
        oai=OaiRepo.objects.get(id = int(data.get('id')))
        mensaje = "was edited correctly"
    serializer_class = OaiRepoSerializer(oai)   
    return Response({'data_oai':serializer_class.data,'result':mensaje}, status=200)

@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def list_admin(request):
    data = {}
    menu = request.query_params.get('menu')
    print(request.query_params.get('menu'))
    # print(menu)
    serializer_class = get_serializer_class(menu)
    # print(get_serializer_class(menu))
    if serializer_class:
        # print("seria")
        fields = get_fields_list(menu) 
        data = get_data_list(request, menu, serializer_class, fields)
    return Response(data, status=200)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def refreshWebService(request):
    data = json.loads(request.body.decode('utf-8'))
    print("Esta es la data::::::::::", data)
    if(data.get('menu')=='webservice'):from urllib import response
import openpyxl
import http.client
import requests
from django.http import (
    HttpResponse, HttpResponseRedirect, JsonResponse
    )
from django.shortcuts import redirect, render
from django.utils.decorators import method_decorator
from django.views import View
import unidecode
from pprint import pprint
from .forms import UploadFileForm
from apps.main.models import *
from apps.main.serializers import UserSerializer, DocumentsSerializer
from .serializers import *
from .models import *
from apps.admins.models import *
import xlwt
from rest_framework import generics
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from chm.settings.base import BASE_DIR,PATH_ENV
from apps.main.helper import (
    get_paginate_num, 
    get_paginate_data, 
    get_model_fields_select,
    get_data_list,
    get_serializer_class,
    get_fields_list,
    obj_set_data
    )
from django.contrib.auth.models import User
from apps.main.helper import get_paginate_num,get_paginate_data,get_model_fields_select
# from apps.admins.pycsw.GetXMLcsw import GetXMLcsw
from apps.admins.csw.GetXMLcsw import GetXMLcsw
from django.contrib.auth.models import User
import json
import os
import sys
from pprint import pprint
from .helper import *
from datetime import datetime
from django.utils import timezone
import subprocess
from django.db.models import Q
from oaipmh.client import Client
from oaipmh.metadata import MetadataRegistry, MetadataReader


oai_dc_reader = MetadataReader(
    fields={
    'title':       ('textList', 'oai_dc:dc/dc:title/text()'),
    'creator':     ('textList', 'oai_dc:dc/dc:creator/text()'),
    'subject':     ('textList', 'oai_dc:dc/dc:subject/text()'),
    'description': ('textList', 'oai_dc:dc/dc:description/text()'),
    'publisher':   ('textList', 'oai_dc:dc/dc:publisher/text()'),
    'contributor': ('textList', 'oai_dc:dc/dc:contributor/text()'),
    'date':        ('textList', 'oai_dc:dc/dc:date/text()'),
    'type':        ('textList', 'oai_dc:dc/dc:type/text()'),
    'format':      ('textList', 'oai_dc:dc/dc:format/text()'),
    'identifier':  ('textList', 'oai_dc:dc/dc:identifier/text()'),
    'source':      ('textList', 'oai_dc:dc/dc:source/text()'),
    'language':    ('textList', 'oai_dc:dc/dc:language/text()'),
    'relation':    ('textList', 'oai_dc:dc/dc:relation/text()'),
    'coverage':    ('textList', 'oai_dc:dc/dc:coverage/text()'),
    'rights':      ('textList', 'oai_dc:dc/dc:rights/text()'),
    },
    namespaces={
    'oai_dc': 'http://www.openarchives.org/OAI/2.0/oai_dc/',
    'dc' : 'http://purl.org/dc/elements/1.1/'}
    )

def parse_JSONLD(record, url):
    response=""
    try:
        record_metadata=record[1].getMap()
        JSON = {
                '@context' : {
                    '@vocab' : 'https://schema.org/'
                },
                '@type' : 'CreativeWork',
                '@id' : record[0].identifier() ,
                'name' : record_metadata["title"][0].strip(),
                'description' : ', '.join([str(elem) for elem in record_metadata["description"]]),
                'url' :  url.split(r'/^https?:\/\/.*\w+\.\w+/')[0] +'/handle/'+ record[0].identifier().split(':')[-1],
                'identifier' : {
                    '@id' : "https://hdl.handle.net/" + record[0].identifier().split(':')[-1],
                    '@type' : 'PropertyValue',
                    'propertyID' : 'https://hdl.handle.net/',
                    'value' : record[0].identifier().split(':')[-1],   
                    'url' : "https://hdl.handle.net/" + record[0].identifier().split(':')[-1]
                }
            }
        rec_keys=record_metadata.keys()
        if "creator" in rec_keys and len(record_metadata["creator"]):
            authors=[]
            for x in record_metadata["creator"]:
                author = {
                    '@type' : 'Person',
                        'name' : x
                        }
                authors.append(author)
            JSON['author'] = authors
        contributors=[]
        if "contributors" in rec_keys and len(record_metadata["contributors"]) :

            for x in record_metadata["contributors"]:
                contributor = {
                    '@type' : 'Organization',
                    'name' : x
                    }
                contributors.append(contributor)

        if "publisher" in rec_keys and len(record_metadata["publisher"]) :

            for x in record_metadata["publisher"]:
                publisher = {
                    '@type' : 'Organization',
                    'name' : x
                    }
                contributors.append(publisher)
            
        if (len(contributors)):
            JSON['contributor'] = contributors
                
        if "date" in rec_keys and len(record_metadata["date"]):
            JSON['datePublished'] = record_metadata["date"][-1]
        if "language" in rec_keys and len(record_metadata["language"]):
            JSON['inLanguage'] = record_metadata["language"]
        if "subject" in rec_keys and len(record_metadata["subject"]):
            JSON['keywords'] = record_metadata["subject"]
        if "spatial" in rec_keys and len(record_metadata["spatial"]):
            JSON['spatialCoverage'] = record_metadata["spatial"]
        if "temporal" in rec_keys and len(record_metadata["temporal"]):
            JSON['temporalCoverage'] = record_metadata["temporal"]
        if "source" in rec_keys and len(record_metadata["source"]):
            JSON['isPartOf'] = record_metadata["source"]
       
        if "relation"in rec_keys and len(record_metadata["relation"]):
            JSON['isPartOf'] = record_metadata["relation"]
        if "coverage" in rec_keys and len(record_metadata["coverage"]):
            JSON['coverage'] = record_metadata["coverage"]
        if "rights" in rec_keys and len(record_metadata["rights"]):
            JSON['license'] = record_metadata["rights"]

        if "format" in rec_keys and len(record_metadata["format"]) and len(list(filter((lambda x: x.find("/")),record_metadata["format"]))):
            JSON['encodingFormat'] = list(filter((lambda x: x.find("/")),record_metadata["format"]))[0]

        
        response = json.dumps(JSON).replace('\"', '"')
    except:
        print("Error parsing JSONLD")
        response = None
    return response

def record_exist(name, record,force_update,url,domain):
    try:
        nombre=str(record[1].getField('title')[0]).strip().replace("'","").replace('"',"").replace("\\","").replace("[","").replace("]","")
        if nombre=='':
            return True
        queryset=Document.objects.get((Q(nombre__icontains = nombre.split("(")[0].split(")")[0]) | Q(url= "https://hdl.handle.net/" + record[0].identifier().split(':')[-1]) | Q(url= list(filter(lambda x: x.startswith('http'), record[1].getField('identifier')))[0])))
        print("ESTE ES EL NUEVO PRINT:::1"+queryset)
        if queryset.exists():
            if force_update:
                queryset.delete()
                doc ={} 
                doc["nombre"] = nombre
                doc["id_nombre"] = nombre.encode('ascii', 'ignore').decode().upper().replace(" ","").replace(".","").replace(",","").replace("/","").replace("(","").replace(")","")
                doc["imagen"]= '/static/main/index/images/doc-default.png'
                doc["url"]= list(filter(lambda x: x.startswith('http'), record[1].getField('identifier')))[0] if list(filter(lambda x: x.startswith('http'), record[1].getField('identifier'))) !=[] else "https://hdl.handle.net/" + record[0].identifier().split(':')[-1]
                doc["fecha"]= record[1].getField('date')[0] if record[1].getField('date')!=[] else ""
                doc["resumen"]= ', '.join([str(elem) for elem in record[1].getField('description')])[:3000] if record[1].getField('description')!= [] else "N/D"
                doc["pais"]= ', '.join([str(elem) for elem in record[1].getField('coverage')])[:300] if record[1].getField('coverage')!= [] else "."
                doc["source"]=  name
                # doc["shared"]= shared
                doc["method_harvesting"] = 'OAI_PMH'
                doc["json_ld"]=parse_JSONLD(record,url)
                serializer=DocumentsSerializer(data=doc)
                if serializer.is_valid():
                    serializer.save()
                    return True
                print(doc)
            elif queryset.filter(json_ld__icontains="context").exists() :
                return True
            else:
                json_ld_new=parse_JSONLD(record, url)
                doc_ex= queryset.exclude(json_ld__icontains="context").update(json_ld=json_ld_new)
                return True
        queryset= Document.objects.filter(id_nombre__icontains = nombre.encode('ascii', 'ignore').decode().upper().replace(" ","").replace(".","").replace(",","").replace("/","").replace("(","").replace(")",""))
        print("ESTE ES EL NUEVO PRINT::2"+queryset)
        if queryset.exists() :
            if force_update:
                queryset.delete()
                doc ={} 
                doc["nombre"] = nombre 
                doc["id_nombre"] = nombre.encode('ascii', 'ignore').decode().upper().replace(" ","").replace(".","").replace(",","").replace("/","").replace("(","").replace(")","")
                doc["imagen"]= '/static/main/index/images/doc-default.png'
                doc["url"]= list(filter(lambda x: x.startswith('http'), record[1].getField('identifier')))[0] if list(filter(lambda x: x.startswith('http'), record[1].getField('identifier'))) !=[] else "https://hdl.handle.net/" + record[0].identifier().split(':')[-1]
                doc["fecha"]= record[1].getField('date')[-1] if record[1].getField('date')!=[] else ""
                doc["resumen"]= ', '.join([str(elem) for elem in record[1].getField('description')])[:3000] if record[1].getField('description')!= [] else "N/D"
                doc["pais"]= ', '.join([str(elem) for elem in record[1].getField('coverage')])[:300] if record[1].getField('coverage')!= [] else "."
                doc["source"]= name
                # doc["shared"]= shared
                doc["method_harvesting"] = 'OAI_PMH'
                doc["json_ld"]=parse_JSONLD(record,url)

                print(":::::::::::::::::::::::::::::::::::",doc)

                serializer=DocumentsSerializer(data=doc)
                if serializer.is_valid():
                    serializer.save()
            return True
        return False
    except:
        print("ESTE ES EL NUEVO PRINT::3")
        return False

def WebServiceOAI(name,domain,complement,limit,prefix,dataset,update, acumulado):
    URL = domain + complement
    force_update= True if update=="Yes" else False
    registry = MetadataRegistry()
    registry.registerReader(prefix, oai_dc_reader)
    client = Client(URL, registry)
    doc_length = 0
    num_records = 0
    documents=[]
    records=[]
    errors=[]
    repeated_records=[]
    if dataset!="":
        records=client.listRecords(metadataPrefix=prefix, set=dataset)
    else:
        records= client.listRecords(metadataPrefix=prefix)
    for record in records:
        try:
            if num_records%200==0:
                print("num records:::::",num_records)
            if record[0].isDeleted() or record[1].getMap().keys()==[] :
                num_records+=1
                continue
            if record_exist(name, record, force_update,URL,domain) :
                if force_update:
                    doc_length+=1
                num_records+=1 
                repeated_records.append(str(record[1].getField('title')[0]).strip().replace("'","").replace("\"","").replace("\\","").replace("[","").replace("]",""))
                continue
            else:
                print("num records::11:::")
                doc ={}
                doc["nombre"] = str(record[1].getField('title')[0]).strip().replace("'","").replace("\"","").replace("\\","").replace("[","").replace("]","")
                doc["id_nombre"] = doc["nombre"].encode('ascii', 'ignore').decode().upper().replace(" ","").replace(".","").replace(",","").replace("/","").replace("(","").replace(")","")
                doc["imagen"]= '/static/main/index/images/doc-default.png'
                doc["url"]= list(filter(lambda x: x.startswith('http'), record[1].getField('identifier')))[0] if list(filter(lambda x: x.startswith('http'), record[1].getField('identifier'))) !=[] else "https://hdl.handle.net/" + record[0].identifier().split(':')[-1]
                doc["fecha"]= record[1].getField('date')[-1] if record[1].getField('date')!=[] else ""
                doc["resumen"]= ', '.join([str(elem) for elem in record[1].getField('description')])[:3000] if record[1].getField('description')!= [] else "N/D"
                doc["pais"]= ', '.join([str(elem) for elem in record[1].getField('coverage')])[:300] if record[1].getField('coverage')!= [] else "."
                doc["source"]= name
                doc["method_harvesting"] = 'OAI_PMH'
                doc["json_ld"]=parse_JSONLD(record,URL)
                serializer=DocumentsSerializer(data=doc)
                print("num records::15:::")
                if serializer.is_valid():
                    serializer.save()
                if serializer.errors:
                    errors.append(serializer.errors)
                doc_length+=1
                num_records+=1 
            if limit>0 and doc_length>=limit:
                break
        except Exception as e:
            print(e)
            print(num_records)
            errors.append({"metadata":record[1].getMap(), "header":record[0].identifier()})
            continue
    response={
        "data":". <br/><b>Documents Inserted: </b>" + str(doc_length) + "<br/> <b>Documents harvested: </b>" + str(num_records),
        "Error":errors,
        "acumulado": str(num_records)
    }
    
    if repeated_records!=[]:
        response["data"]=response["data"]+"<br/><b>Repeated records: </b> <br/> <ul><li> " + "</li><li>".join(repeated_records) + "</li></ul>"
    return (response)

@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def WebServiceCreate(request):
    data = json.loads(request.body.decode('utf-8'))
    mensaje = "Error"
    web = None
    if (data.get("id")==''):
        web = Webservicechm()
        web.name = data.get("name")   
        web.endpoint = data.get("endpoint")  
        web.category = data.get("category")  
        web.pagination = data.get("pagination")
        web.page_key = data.get("key_page")
        web.page_init = data.get("init_page")
        web.page_end = data.get("end_page")
        web.method = data.get("method")
        web.save()

        save_header(data.get("headers"),web)
        save_category_webservice(data.get("categoryField"),web)
        mensaje = "was created correctly"
    else:
        Webservicechm.objects.filter(id= int(data.get('id'))).update(
            name = data.get("name"),   
            endpoint = data.get("endpoint"), 
            category = data.get("category"),  
            pagination = data.get("pagination"),
            page_key = data.get("key_page"),
            page_init = data.get("init_page"),
            page_end = data.get("end_page"),
            method = data.get("method"),  
        )
        web = Webservicechm.objects.get(id = int(data.get('id')))
        Headers.objects.filter(webservice =web ).delete()
        save_header(data.get("headers"),web)
        DetailField.objects.filter(webservice =web ).delete()
        save_category_webservice(data.get("categoryField"),web)
        mensaje = "was edited correctly"
    
    
    serializer_class = WebServiceSerializer(web)   
    return Response({'data_web':serializer_class.data,'result':mensaje}, status=200)

def save_category_webservice(categoryField,web):
    for cfield in categoryField:
        field = DetailField()
        field.field_chm = cfield['fieldlocal']
        field.field_web = cfield['fieldweb']
        field.webservice = web
        field.save()

def save_header(headers,web):
    for header in headers:
        head = Headers()
        head.key = header['headerkey']
        head.value = header['headervalue']
        head.webservice = web
        head.save()

@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def ScrapingCreate(request):
    data = json.loads(request.body.decode('utf-8'))
    mensaje = "Error"
    scr = None
    if (data.get("id")==''):
        scr = Scraping()
        scr.name = data.get("name")   
        scr.dominio = data.get("dominio")  
        scr.category = data.get("category")  
        scr.starturl = data.get("starturl")
        scr.rootItem = data.get("rootItem")
        scr.pagination = data.get("pagination")
        scr.page_key = data.get("key_page")
        scr.page_init = data.get("init_page")
        scr.page_end = data.get("end_page")
        scr.increment = data.get("increment")
        scr.connector = data.get("connector")
        scr.classItem = data.get("classItem")
        scr.save()
        
        save_categoryField_scraping(data.get("categoryField"),scr)
        mensaje = "Successfully saved"
    else:
        Scraping.objects.filter(id=int(data.get("id") )).update(
            name = data.get("name"),   
            dominio = data.get("dominio"),  
            category = data.get("category"),  
            pagination = data.get("pagination"),
            page_key = data.get("key_page"),
            page_init = data.get("init_page"),
            page_end = data.get("end_page"),
            increment = data.get("increment"),
            connector = data.get("connector"),
            starturl = data.get("starturl"),
            rootItem = data.get("rootItem"),
            classItem = data.get("classItem")
        )
        scr = Scraping.objects.get(id = int(data.get("id")))

        ScrapingField.objects.filter(scraping=scr).delete()
        save_categoryField_scraping(data.get("categoryField"),scr)
        mensaje = "Successfully edited"
    
    
    serializer_class = ScrapingSerializer(scr)   
    return Response({'data_scr':serializer_class.data,'result':mensaje}, status=200)


def save_categoryField_scraping(categoryField,scr):
    for cfield in categoryField:
        field = ScrapingField()
        field.field_chm = cfield['fieldlocal']
        field.field_web = cfield['fieldweb']
        field.type_field = cfield['typefield']
        field.size_text = cfield['sizetext']
        field.root_item = cfield['rootitem']
        field.scraping = scr
        field.save()

@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def OaiRepoCreate(request):
    data = json.loads(request.body.decode('utf-8'))
    mensaje = "Error"
    oai = None
    cant = 0
    if (data.get("id")==''):
        oai = OaiRepo()
        oai.name=data.get("name")
        oai.category=data.get("category")
        oai.metadata_prefix=data.get("metadata_prefix")
        oai.domain=data.get("domain")
        oai.url_complement=data.get("url_complement")
        oai.data_set=data.get("data_set")
        oai.limit=data.get("limit")
        oai.acumulado=0
        if oai.domain=="http://aquadocs.org/":
            oai.shared=False
        else:
            oai.shared=True
        oai.save()
        mensaje = "was created correctly"
        
    else:
        OaiRepo.objects.filter(id= int(data.get('id'))).update(
            name=data.get("name"),
            category=data.get("category"),
            metadata_prefix=data.get("metadata_prefix"),
            domain=data.get("domain"),
            url_complement=data.get("url_complement"),
            data_set=data.get("data_set"),
            update=data.get("update"),
            limit=data.get("limit"),
            # acumulado = data.get("acumulado")
            
        )
        oai=OaiRepo.objects.get(id = int(data.get('id')))
        mensaje = "was edited correctly"
    serializer_class = OaiRepoSerializer(oai)   
    return Response({'data_oai':serializer_class.data,'result':mensaje}, status=200)

@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def list_admin(request):
    data = {}
    menu = request.query_params.get('menu')
    print(request.query_params.get('menu'))
    # print(menu)
    serializer_class = get_serializer_class(menu)
    # print(get_serializer_class(menu))
    if serializer_class:
        # print("seria")
        fields = get_fields_list(menu) 
        data = get_data_list(request, menu, serializer_class, fields)
    return Response(data, status=200)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def refreshWebService(request):
    data = json.loads(request.body.decode('utf-8'))
    print("Esta es la data::::::::::", data)
    if(data.get('menu')=='webservice'):
        try:
            web = Webservicechm.objects.get(id=data.get("id"))
            if (web.category=='csw'):
                csw = GetXMLcsw(web.endpoint,True)
                datos = csw.get_csw()
                cant = 0
                Platformgeo.objects.filter(source=web.name).delete()
                for dato in datos:
                    
                    geo = Platformgeo()
                    geo.id_nombre = dato['title'].upper().replace(" ","")
                    geo.nombre = dato['title']
                    geo.pais = dato['country'] if 'country' in dato else ''
                    geo.resumen = dato['abstract']
                    geo.url = dato['url']
                    geo.category = dato['topicCategory']
                    date = dato['date'].replace('T',' ')
                    date = date.replace('Z', '')
                    date = date.replace('-', '/')
                    geo.date = date
                    geo.source = web.name
                    geo.source_id = web.id
                    geo.method_harvesting = "CSW"
                    cant += 1
                    geo.save()
                web.acumulado = cant
                web.save()
        except Exception as err: 
            print(err)
            return Response({'result':'There was an error while harvesting, please communicate with the administrator'}, status=400)
    elif(data.get('menu')=='scraping'):
        try:
            direct = os.path.join(BASE_DIR, 'apps/webScraping')
            os.chdir(direct)
            path = sys.path
            path_env=''
            for p in path:
                if "site-packages" in p:
                    path_env = p[0:p.find("lib/")]+"bin/"
                    break
            subprocess.call(path_env+'scrapy crawl scrapingCHM -a id_scraping='+str(data.get("id")), shell=True)
        except Exception:
            return Response({'result':'There was an error while harvesting, please communicate with the administrator'}, status=400)
        return Response({'result':'Scraping Refreshed succesfull'}, status=200)
    elif(data.get('menu')=='oai'):
        try:
            oai = OaiRepo.objects.filter(id=data.get("id")).values()[0]
            # print(oai, "OAIIIIIIIIIIIIIIIIII")
            response = WebServiceOAI(name=oai["name"],domain=oai["domain"],complement=oai["url_complement"],prefix=oai["metadata_prefix"],dataset=oai["data_set"],limit=oai["limit"],update=oai["update"], acumulado=oai["acumulado"])
            OaiRepo.objects.filter(id=data.get("id")).update(acumulado=response.get("acumulado"))
            OaiRepo.objects.filter(id=data.get("id")).update(last_harvest=datetime.now())
            # print (response, "RESPONSEEEEEEEEEEEEEEEEEEEE")
            return Response({'result':'Harvest succesfull'+ response["data"], 'data':response}, status=200)

        except Exception as e:
            return Response({'text':'There was an error while harvesting, please communicate with the administrator <br/> Error:  ' + str(e)}, status=400)
    return Response({'result':'Web service Refreshed succesfull'}, status=200)


@api_view(['DELETE'])
@permission_classes((IsAuthenticated,))
def deleteSource(request):
    model ={
        'experts':People,
        'csw':Platformgeo,
        'documents':Document,
        'training':Training,
        'laboratories': Laboratory,
        'institutions': Institution,
        'geo': Platformgeo,
        'vess': Vessel}

    data = json.loads(request.body.decode('utf-8'))
    if(data.get('menu')=='webservice'):
        try: 
            web = Webservicechm.objects.get(id=data.get("id"))
            model[web.category].objects.filter(source_id=web.id,method_harvesting="webservice").delete()
            Webservicechm.objects.filter(id=data.get("id")).delete()
            return Response({'result':'Web service deleted succesfull'}, status=200)
        except Exception:
            return Response({'result':'There was an error while deleted, please communicate with the administrator'}, status=200)
    if(data.get('menu')=='scraping'):
        try:
            scr = Scraping.objects.get(id=data.get("id"))
            model[scr.category].objects.filter(source_id=scr.id,method_harvesting='scraping').delete()
            Scraping.objects.filter(id=data.get("id")).delete()
            return Response({'result':'Scraping deleted succesfull'}, status=200)
        except Exception:
            return Response({'result':'There was an error while deleted, please communicate with the administrator'}, status=200)
    
    if(data.get('menu')=='apireader'):
        try: 
            rdr = ApiReader.objects.get(id=data.get("id"))
            model[rdr.category].objects.filter(source_id=rdr.id,method_harvesting="apireader").delete()
            ApiReader.objects.filter(id=data.get("id")).delete()
            return Response({'result':'Api Reader deleted succesfull'}, status=200)
        except Exception:
            return Response({'result':'There was an error while deleted, please communicate with the administrator'}, status=200)
    
    if(data.get('menu')=='upload'):            
        try:            
            uploa = UpdloadFiled.objects.get(id=data.get("id"))                        
            res_key = [a for a in model.keys() if  a in uploa.category.lower()] 
            if len(res_key)>0:
                model[res_key[0]].objects.filter(source_id=uploa.id,method_harvesting='upload').delete()
                UpdloadFiled.objects.filter(id=data.get("id")).delete()
                return Response({'result':'UploadFile deleted succesfull'}, status=200)
            else:
                return Response({'result':'UploadFile no option found to delete'}, status=200)
        except Exception as ex:            
            return Response({'result':ex}, status=200)


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def get_webservice(request):
    id_web = request.query_params.get('id_web')
    web = Webservicechm.objects.get(id=id_web)
    header = Headers.objects.filter(webservice=web)
    detField = DetailField.objects.filter(webservice=web)
    serializerWeb = WebServiceSerializer(web)
    serializerHeader = HeadersSerializer(header,many=True)
    serializerFields = DetailfiledSerializer(detField,many=True)
    return Response(
            {
                'data_web':serializerWeb.data, 
                'data_header': serializerHeader.data,
                'data_fields': serializerFields.data
            },
            status=200
    )

@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def get_oai(request):
    id_oai = request.query_params.get('id_oai')
    oai = OaiRepo.objects.get(id=id_oai)
    oai_serialized = OaiRepoSerializer(oai)
    return Response(
            {
                'data_oai':oai_serialized.data
            },
            status=200
    )

@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def get_scraping(request):
    id_scr = request.query_params.get('id_scr')
    scr = Scraping.objects.get(id=id_scr)
    fields = ScrapingField.objects.filter(scraping=scr)
    
    serializerScr = ScrapingSerializer(scr)
    serializerFields = ScrapingFieldSerializer(fields,many=True)
    return Response(
            {
                'data_src':serializerScr.data,
                'data_fields': serializerFields.data
            },
            status=200
    )


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def save_data_update_file(request):
    array_data_fail = []
    array_data_s= 0  
    array_data_r= 0  
    invalid_sheets = []
    res_sheets = []
    try:
        excel_file = request.FILES["file"]
        name_file = request.FILES["file"].name
    except Exception as e:
        return Response({'error':True, 'msg':'Error empty file field','class':'alert-danger','seg_alert':9000}, status=200) 
        
    type_worksheet = request.POST.get('category')
    type_worksheet = type_worksheet.lower()

    try:                
        wb = openpyxl.load_workbook(excel_file)
    except Exception as e:
        return Response({'error':True, 'msg':'Error when trying to open excel','class':'alert-danger','type':'open','seg_alert':9000}, status=200)    
    
    sheets = wb.sheetnames
    invalid_sheets = validate_sheets(sheets,wb,type_worksheet)
    if(invalid_sheets is not None):   
        if(invalid_sheets['data'] is not None):
            res_sheets = [a for a in sheets if a not in invalid_sheets['data']]        
        else:
            return Response({'error':True, 'msg':invalid_sheets['msg'],'class':'alert-danger','type':'open','seg_alert':9000}, status=200)    
    else:
        res_sheets = sheets

    if(len(res_sheets)>0):
        obj_model_ = type_to_model(type_worksheet)
        obj_saved_ = store_data_admin_uploadFiled(UpdloadFiled,obj_model_,name_file,array_data_s,array_data_r,array_data_fail,{})
        for sheet in res_sheets:
            filters = {}
            array_data = []                
            res = {}
            model = None
            try:                
                worksheet = wb[sheet]
            except Exception as e:
                return Response({
                'error':True, 
                'msg':'Error when trying to open the worksheet '+sheet,
                'class':'alert-danger',
                'seg_alert':9000
            }, status=200)  

            filters = get_filters(worksheet)                                    
            model = instance_models(filters, type_worksheet)                
            
            res = get_data_excel(filters, worksheet, type_worksheet)            
            array_data = res['data']
            array_data_s += len(array_data) 
            array_data_fail += res['data_fail']            
            
            if(len(array_data)>0):                            
                for data in array_data:
                    if(save_data(model,data,obj_saved_)==1):
                        # print(array_data_)
                        array_data_s = (array_data_s - 1)
                        array_data_r = (array_data_r + 1)
        
        store_data_admin_uploadFiled(UpdloadFiled,model,name_file,array_data_s,array_data_r,array_data_fail,filters,obj_saved_)
        if(invalid_sheets is not None): 
            if(invalid_sheets['data'] is not None):            
                str1 = '.  ,'.join(invalid_sheets['data'])
                str2 = '.  ,'.join(res_sheets)
                return Response({
                    'error':False,
                    'msg':'The sheets '+str1+ ' do not comply with the columns to be stored. It was possible to store the sheets '+ str2 +' of the excel file', 
                    'class':'alert-warning', 
                    'array_fail_data':array_data_fail, 
                    'obj_fail_headers':filters,
                    'seg_alert':9000}, status=200)
        else:
            return Response({
                'error':False,
                'msg':'Saved records', 
                'class':'alert-success', 
                'array_fail_data':array_data_fail, 
                'obj_fail_headers':filters,
                'seg_alert':9000
                }, status=200)

    else:
        return Response({                
                    'error': True, 
                    'msg':'The sheets of the excel file do not comply with the columns to be stored',
                    'class':'alert-danger',
                    'seg_alert':9000
                }, status=200)
            
    # return Response({'error':False,'msg':'Datos guardados', 'class':'alert-success', 'array_fail_data':array_data_fail, 'obj_fail_headers':filters}, status=200)


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def get_user(request):
    id_scr = request.query_params.get('id_scr')
    scr = User.objects.get(id=id_scr)
    scr_serialized = UserSerializer(scr)
    return Response(
            {
                'data_src':scr_serialized.data
            },
            status=200
    )

@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def user_save(request):
    data = json.loads(request.body.decode('utf-8'))
    obj_id = data.get("id")
    obj = None
    res = {
        "data_web":{},
        "result":""
    }
    properties = [
        'id',
        'username',
        'is_superuser',
        'email',
        'first_name',
        'last_name',
        'is_active'
        ]
    booleans = ['is_superuser','is_active',]
    if not obj_id:
        obj = User()
        obj.set_password(data.get('password'))
    else:
        obj = User.objects.get(pk=obj_id) 
    if obj:
        obj = obj_set_data(obj, data, properties, booleans)
        try:
            obj.save()
        except Exception as err:
            print(err)
            return Response({}, status = 500)
        res["result"] = "Action was executed correctly"
        res['data_web'] = UserSerializer(obj).data
    else:
        print('Obj does not defined')
    return Response(res, status=200)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def ApiReaderCreate(request):
    data=json.loads(request.body.decode('utf-8'))
    mensaje = "Error"
    print(data)
    rdr= None
    if (data.get("id")==''):
      
        var= ApiReader.objects.last().id
        rdr=ApiReader()
        rdr.id= var+1
        rdr.name = data.get("name")
        rdr.dominio = data.get("dominio")
        rdr.category = data.get("category")
        rdr.item= data.get("item")
        rdr.classItem = data.get("classItem")
        rdr.save()

        save_categoryField_apireader(data.get("categoryField"), rdr)
        mensaje = "Successfully saved"
    else:
        ApiReader.objects.filter(id= int(data.get("id"))).update(
            name = data.get("name"),
            dominio = data.get("dominio"),
            category = data.get("category"),
            item= data.get("item"),
            classItem = data.get("classItem")
        )
        rdr=ApiReader.objects.get(id = int(data.get("id")))

        ApiReaderField.objects.filter(apireader=rdr).delete()
        save_categoryField_apireader(data.get("categoryField"),rdr)
        mensaje = "Successfully edited"

    serializer_class=ApiReaderSerializer(rdr)
    return Response({'data_scr':serializer_class.data,'result':mensaje}, status=200)

def save_categoryField_apireader(categoryField,rdr):
    for cfield in categoryField:
        field=ApiReaderField()
        field.field_chm = cfield['fieldlocal']
        field.item= cfield['item']
        field.type_field= cfield['typefield']
        field.size_text= cfield['sizetext']
        field.apireader= rdr
        field.save()

@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def get_apireader(request):
    rdr = ApiReader.objects.all()
    serializerRdr=ApiReaderSerializer(rdr,many=True)
    # serializerFields=ApiReaderFieldSerializer(fields,many=True)
    # 'data_fields':serializerFields.data
    return Response({'data_rdr':serializerRdr.data}, status=200)

@api_view(['DELETE'])
@permission_classes((IsAuthenticated,))
def deleterdr(request):

    data = json.loads(request.body.decode('utf-8'))
    # rdrField = ApiReader.objects.get(id=data.get("id"))
    # ApiReaderField.objects.filter(apireader=rdrField).delete()
    ApiReader.objects.get(id=data.get("id")).delete()
    return Response({'result':f'Web service deleted succesfull'}, status=200)
   
@api_view(['POST'])
@permission_classes(())
def updaterdr(request):
    data = json.loads(request.body.decode('utf-8'))
    obj = ApiReader.objects.get(id=data.get("id"))
    fields=ApiReaderField.objects.filter(apireader=obj)
    
    # Haciendo peticion a la api
    response = get_api(obj.dominio)

    #Convertir el JSON string into a dictionary
    json_dicti = json.loads(response)

    # Guardar en base de datos
    if obj.category == "experts":
        print("es expertos se debe guardar en esta tabla")
        # arr=json_dicti['results']['data']
        arr=json_dicti['dataset']
        for n in arr:
            filtered_data = []
            for nameField in fields:
                aux = n[nameField.item]  if nameField.item in n  else ""
                app_data = {
                    nameField.field_chm: aux
                } 
                filtered_data.append(app_data)
            # print('22222222222AUXILIAAAAAAAAAARRRRR',filtered_data)
            auxObj=People()
            auxObj.imagen=filtered_data[0]["imagen"] 
            auxObj.nombre=filtered_data[1]["nombre"]
            auxObj.url=filtered_data[2]["url"]
            auxObj.institucion=filtered_data[3]["institucion"]
            auxObj.ocupacion=filtered_data[4]["ocupacion"]
            auxObj.pais=filtered_data[5]["pais"]
            auxObj.source=obj.name
            auxObj.aptitudes=""
            auxObj.source_id=2
            auxObj.id_nombre=filtered_data[1]["nombre"]
            # print(auxObj,'AUXILIAAAAAAAAAARRRRR')
            auxObj.save()      

    elif obj.category == "documents":
        print("es Documents se debe guardar en esta tabla")
        arr=json_dicti['dataset']
        for n in arr:
            filtered_data = []
            for nameField in fields:
                aux = n[nameField.item]  if nameField.item in n  else ""
                app_data = {
                    nameField.field_chm: aux
                } 
            #     filtered_data.append(app_data)
            # print('22222222222AUXILIAAAAAAAAAARRRRR',filtered_data)
            auxObj=Document()
            auxObj.imagen=filtered_data[0]["imagen"] 
            auxObj.nombre=filtered_data[1]["nombre"]
            auxObj.url=filtered_data[2]["url"]
            auxObj.fecha=filtered_data[3]["fecha"]
            auxObj.resumen=filtered_data[4]["resumen"]
            auxObj.pais=filtered_data[5]["pais"]
            auxObj.source=filtered_data[6]["source"]
            auxObj.json_ld=filtered_data[7]["json_ld"]
            auxObj.source_id=2
            auxObj.id_nombre=filtered_data[1]["nombre"]
            # print(auxObj,'AUXILIAAAAAAAAAARRRRR')
            auxObj.save()    
            
    elif obj.category == "training":
        print("Si es Training se debe guardar en esta tabla")
       # arr=json_dicti['results']['data']
        arr=json_dicti['dataset']
        for n in arr:
            filtered_data = []
            for nameField in fields:
                aux = n[nameField.item]  if nameField.item in n  else ""
                app_data = {
                    nameField.field_chm: aux
                } 
                filtered_data.append(app_data)
            # print('22222222222AUXILIAAAAAAAAAARRRRR',filtered_data)
            auxObj=Training()
            auxObj.imagen=filtered_data[0]["imagen"] 
            auxObj.nombre=filtered_data[1]["nombre"]
            auxObj.url=filtered_data[2]["url"]
            auxObj.tipo=filtered_data[3]["tipo"]
            auxObj.email=filtered_data[4]["email"]
            auxObj.ubicacion=filtered_data[5]["ubicacion"]
            auxObj.fecha=filtered_data[6]["fecha"]
            auxObj.institucion=filtered_data[7]["institucion"]
            auxObj.source_id=2
            auxObj.source=obj.name
            auxObj.id_nombre=filtered_data[1]["nombre"]
            # print(auxObj,'AUXILIAAAAAAAAAARRRRR')
            auxObj.save()    

    elif obj.category == "laboratories":
        print("Si es Laborator se debe guardar en esta tabla")
        # arr=json_dicti['results']['data']
        arr=json_dicti['dataset']

       
        for n in arr:
            filtered_data = []
            for nameField in fields:
                aux = n[nameField.item]  if nameField.item in n  else ""
                app_data = {
                    nameField.field_chm: aux
                } 
                filtered_data.append(app_data)
            # print('22222222222AUXILIAAAAAAAAAARRRRR',filtered_data)
            auxObj=Laboratory()
            auxObj.imagen=filtered_data[0]["imagen"] 
            auxObj.nombre=filtered_data[1]["nombre"]
            auxObj.url=filtered_data[2]["url"]
            auxObj.resumen=filtered_data[3]["resumen"]
            auxObj.pais=filtered_data[4]["pais"]
            auxObj.entidad=filtered_data[5]["entidad"]
            auxObj.source=obj.name
            auxObj.source_id=2
            auxObj.id_nombre=filtered_data[1]["nombre"]
            # print(auxObj,'AUXILIAAAAAAAAAARRRRR')
            auxObj.save()    

    elif obj.category == "institutions":
        print("Sies Instituti se debe guardar en esta tabla")
       # arr=json_dicti['results']['data']
        arr=json_dicti['dataset']

       
        for n in arr:
            filtered_data = []
            for nameField in fields:
                aux = n[nameField.item]  if nameField.item in n  else ""
                app_data = {
                    nameField.field_chm: aux
                } 
                filtered_data.append(app_data)
            # print('22222222222AUXILIAAAAAAAAAARRRRR',filtered_data)
            auxObj=Institution()
            auxObj.imagen=filtered_data[0]["imagen"] 
            auxObj.nombre=filtered_data[1]["nombre"]
            auxObj.url=filtered_data[2]["url"]
            auxObj.direccion=filtered_data[3]["direccion"]
            auxObj.pais=filtered_data[4]["pais"]
            auxObj.source=obj.name
            auxObj.source_id=2
            auxObj.id_nombre=filtered_data[1]["nombre"]
            # print(auxObj,'AUXILIAAAAAAAAAARRRRR')
            auxObj.save()       

    elif obj.category == "geo":
        # arr=json_dicti['results']['data']
        arr=json_dicti['dataset']

       
        for n in arr:
            filtered_data = []
            for nameField in fields:
                aux = n[nameField.item]  if nameField.item in n  else ""
                app_data = {
                    nameField.field_chm: aux
                } 
                filtered_data.append(app_data)
            # print('22222222222AUXILIAAAAAAAAAARRRRR',filtered_data)
            auxObj=Platformgeo()
            auxObj.imagen='https://sibcolombia.net/wp-content/uploads/2016/06/Logo-invemar.png'
            auxObj.nombre=filtered_data[0]["nombre"]
            auxObj.url=filtered_data[1]["url"]
            auxObj.category=filtered_data[2]["category"] 
            auxObj.resumen=filtered_data[3]["resumen"]
            auxObj.date=filtered_data[4]["date"]
            auxObj.source=obj.name
            auxObj.pais="Colombia"
            auxObj.source_id=2
            auxObj.id_nombre=filtered_data[0]["nombre"]
            print(auxObj,'AUXILIAAAAAAAAAARRRRR')
            auxObj.save()    

    elif obj.category == "vess":
        print("Si es Vessels se debe guardar en esta tabla")
       # arr=json_dicti['results']['data']
        arr=json_dicti['dataset']

       
        for n in arr:
            filtered_data = []
            for nameField in fields:
                aux = n[nameField.item]  if nameField.item in n  else ""
                app_data = {
                    nameField.field_chm: aux
                } 
                filtered_data.append(app_data)
            # print('22222222222AUXILIAAAAAAAAAARRRRR',filtered_data)
            auxObj=Vessel()
            auxObj.nombre=filtered_data[0]["nombre"]
            auxObj.url=filtered_data[1]["url"]
            auxObj.pais=filtered_data[2]["pais"]
            auxObj.institucion=filtered_data[3]["institucion"]
            auxObj.descripcion=filtered_data[4]["descripcion"]
            auxObj.equipi_nav=filtered_data[5]["equipi_nav"]
            auxObj.cantidad_trip=filtered_data[6]["cantidad_trip"]
            auxObj.source=obj.name
            auxObj.categoria=filtered_data[8]["categoria"]
            auxObj.estado=filtered_data[9]["estado"]
            auxObj.actividad=filtered_data[10]["actividad"]
            auxObj.envergadura=filtered_data[11]["envergadura"]
            auxObj.source_id=2
            auxObj.id_nombre=filtered_data[1]["nombre"]
            # print(auxObj,'AUXILIAAAAAAAAAARRRRR')
            auxObj.save()    
            
    return Response({'result':f'Ejecutando Harvesting en Api:{obj.dominio}'}, status=200)

def get_api(url):
    return requests.request("GET", url).text