from oaipmh.client import Client
from oaipmh.metadata import MetadataRegistry, MetadataReader
import json

oai_dc_reader = MetadataReader(
    fields={
    'title':       ('textList', 'oai_dc:dc/dc:title/text()'),
    'creator':     ('textList', 'oai_dc:dc/dc:creator/text()'),
    'subject':     ('textList', 'oai_dc:dc/dc:subject/text()'),
    'description': ('textList', 'oai_dc:dc/dc:description/text()'),
    'publisher':   ('textList', 'oai_dc:dc/dc:publisher/text()'),
    'contributor': ('textList', 'oai_dc:dc/dc:contributor/text()'),
    'date':        ('textList', 'oai_dc:dc/dc:date/text()'),
    'type':        ('textList', 'oai_dc:dc/dc:type/text()'),
    'format':      ('textList', 'oai_dc:dc/dc:format/text()'),
    'identifier':  ('textList', 'oai_dc:dc/dc:identifier/text()'),
    'source':      ('textList', 'oai_dc:dc/dc:source/text()'),
    'language':    ('textList', 'oai_dc:dc/dc:language/text()'),
    'relation':    ('textList', 'oai_dc:dc/dc:relation/text()'),
    'coverage':    ('textList', 'oai_dc:dc/dc:coverage/text()'),
    'rights':      ('textList', 'oai_dc:dc/dc:rights/text()')
    },
    namespaces={
    'oai_dc': 'http://www.openarchives.org/OAI/2.0/oai_dc/',
    'dc' : 'http://purl.org/dc/elements/1.1/'}
    )

def parse_JSONLD(record, url):
    record_metadata=record[1].getMap()
    JSON = {
            '@context' : {
                '@vocab' : 'https://schema.org/'
            },
            '@type' : 'CreativeWork',
            '@id' : record[0].identifier() ,
            'name' : record_metadata["title"][0].strip(),
            'description' : ', '.join([str(elem) for elem in record_metadata["description"]]),
            'url' :  url.split(r'/^https?:\/\/.*\w+\.\w+/')[0] +'/handle/'+ record[0].identifier().split(':')[-1],
            'identifier' : {
                '@id' : "https://hdl.handle.net/" + record[0].identifier().split(':')[-1],
                '@type' : 'PropertyValue',
                'propertyID' : 'https://hdl.handle.net/',
                'value' : record[0].identifier().split(':')[-1],   
                'url' : "https://hdl.handle.net/" + record[0].identifier().split(':')[-1]
            }
        }
    rec_keys=record_metadata.keys()
    if "creator" in rec_keys and len(record_metadata["creator"]):
        authors=[]
        for x in record_metadata["creator"]:
            author = {
                '@type' : 'Person',
                    'name' : x
                    }
            authors.append(author)
        JSON['author'] = authors
    contributors=[]
    if "contributors" in rec_keys and len(record_metadata["contributors"]) :

        for x in record_metadata["contributors"]:
            contributor = {
                '@type' : 'Organization',
                'name' : x
                }
            contributors.append(contributor)
    if "publisher" in rec_keys and len(record_metadata["publisher"]) :
        for x in record_metadata["publisher"]:
            publisher = {
                '@type' : 'Organization',
                'name' : x
                }
            contributors.append(publisher)        
    if (len(contributors)):
        JSON['contributor'] = contributors            
    if "date" in rec_keys and len(record_metadata["date"]):
        JSON['datePublished'] = record_metadata["date"][0]
    if "language" in rec_keys and len(record_metadata["language"]):
        JSON['inLanguage'] = record_metadata["language"]
    if "subject" in rec_keys and len(record_metadata["subject"]):
        JSON['keywords'] = record_metadata["subject"]
    if "rights" in rec_keys and len(record_metadata["rights"]):
        JSON['license'] = record_metadata["rights"]
    # if "spatial" in rec_keys and len(record_metadata["spatial"]):
    #     JSON['spatialCoverage'] = record_metadata["spatial"]
    # if "temporal" in rec_keys and len(record_metadata["temporal"]):
    #     JSON['temporalCoverage'] = record_metadata["temporal"]
    if "format" in rec_keys and len(record_metadata["format"]):
        JSON['encodingFormat'] = record_metadata["format"]
    if "source" in rec_keys and len(record_metadata["source"]):
        JSON['isPartOf'] = record_metadata["source"]
    if "relation"in rec_keys and len(record_metadata["relation"]):
        JSON['isPartOf'] = record_metadata["relation"]
    if "coverage" in rec_keys and len(record_metadata["coverage"]):
        JSON['coverage'] = record_metadata["coverage"]
    if "rights" in rec_keys and len(record_metadata["rights"]):
        JSON['license'] = record_metadata["rights"]
    
    print(json.dumps(JSON))
    return json.dumps(JSON).replace('\"', '"')

def WebServiceOAI(domain,complement,limit,prefix,dataset):
    URL = domain + complement
    registry = MetadataRegistry()
    registry.registerReader(prefix, oai_dc_reader)
    client = Client(URL, registry)
    documents=[]
    records=[]
    if dataset!="":
        records=client.listRecords(metadataPrefix=prefix, set=dataset)
    else:
        records= client.listRecords(metadataPrefix=prefix)

    for record in records:
            doc ={}
            doc["nombre"] = record[1].getField('title')[0]
            doc["id_nombre"]= doc["nombre"].replace(" ","_")
            doc["imagen"]= '/static/main/index/images/doc-default.png'
            doc["url"]= list(filter(lambda x: x.startswith('http'), record[1].getField('identifier')))[0]
            doc["fecha"]= record[1].getField('date')[0] if record[1].getField('date')!=[] else ""
            doc["resumen"]= ', '.join([str(elem) for elem in record[1].getField('description')])[:3000]
            doc["pais"]= ', '.join([str(elem) for elem in record[1].getField('coverage')]) if record[1].getField('coverage')!= [] else "."
            doc["source"]= ', '.join([str(elem) for elem in record[1].getField('source')]) if record[1].getField('source')!=[] else domain
            doc["method_harvesting"] = 'OIA'
            doc["json_ld"]=parse_JSONLD(record,URL)
            documents.append(doc)        
        if limit>0 and len(documents)>=limit:
            
            break
    return documents