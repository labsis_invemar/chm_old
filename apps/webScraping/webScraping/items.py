# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class People(scrapy.Item):
    # define the fields for your item here like:
    id_nombre = scrapy.Field()      
    nombre = scrapy.Field()         
    url = scrapy.Field()            
    institucion = scrapy.Field()    
    ocupacion = scrapy.Field()      
    aptitudes = scrapy.Field() 
    categoria = scrapy.Field()     
    imagen = scrapy.Field()              
    pais = scrapy.Field()  
    source = scrapy.Field()  
    source_id = scrapy.Field()  
    method_harvesting = scrapy.Field()  

class Institution(scrapy.Item):
    # define the fields for your item here like:
    id_nombre = scrapy.Field()
    nombre = scrapy.Field()
    url = scrapy.Field()
    direccion = scrapy.Field()
    imagen = scrapy.Field()
    categoria = scrapy.Field()
    pais = scrapy.Field()
    source = scrapy.Field() 
    source_id = scrapy.Field() 
    method_harvesting = scrapy.Field() 
    
class Document(scrapy.Item):
    id_nombre = scrapy.Field()
    nombre = scrapy.Field()
    resumen = scrapy.Field()
    imagen = scrapy.Field()
    url = scrapy.Field()
    fecha = scrapy.Field()
    categoria = scrapy.Field()
    pais = scrapy.Field()
    source = scrapy.Field() 
    source_id = scrapy.Field() 
    method_harvesting = scrapy.Field() 

class Training(scrapy.Item):
    id_nombre = scrapy.Field()
    nombre = scrapy.Field()
    resumen = scrapy.Field()
    imagen = scrapy.Field()
    url = scrapy.Field()
    fecha = scrapy.Field()
    ubicacion = scrapy.Field()
    categoria = scrapy.Field()
    order = scrapy.Field()
    institucion = scrapy.Field()
    tipo = scrapy.Field()
    email = scrapy.Field()
    source = scrapy.Field()  
    source_id = scrapy.Field()
    method_harvesting = scrapy.Field() 

class Laboratory(scrapy.Item):
    id_nombre = scrapy.Field()
    imagen = scrapy.Field()
    nombre = scrapy.Field()
    url = scrapy.Field()
    resumen = scrapy.Field()
    pais = scrapy.Field()
    entidad = scrapy.Field()
    source = scrapy.Field()  
    source_id = scrapy.Field()
    method_harvesting = scrapy.Field() 
