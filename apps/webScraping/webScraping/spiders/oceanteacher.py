import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.exceptions import CloseSpider
from webScraping.items import Training
import datetime
from pprint import pprint

class oceanteacher(CrawlSpider):    
    name = 'oceanteacher'
    ubicacion = ''
    institucion = ''    
    allowed_domain = ["https://classroom.oceanteacher.org"]
    start_urls = ['https://classroom.oceanteacher.org/tag/index.php?tc=1&tag=RTC%20Colombia&ta=2&excl=1']

    def parse(self, response):        
        
        self.ubicacion = response.css('section.span9 div h2 ').extract()
        self.ubicacion = self.ubicacion[0].replace('<h2>',"").replace('</h2>',"")
        
        self.institucion = response.css('div.container h1 ').extract()
        self.institucion = self.institucion[0].replace('<h1>',"").replace('</h1>',"")
        


        item_links = response.css('.coursename>a::attr(href)').extract()                
        for a in item_links:
            if a != "":
                yield scrapy.Request(a, callback=self.parse_detail_page)

    def search_value(self,cont,response):
        arr_name = [
        'normalize-space(//*[@id="page"]/div[2]/h1/text())',
        'normalize-space(//*[@id="page-header"]/div/div/div/div[2]/div/div/h1/text())',
        'normalize-space(//*[@id="page-header"]/div/div/div/div[1]/div/div/div/h1/text())',
        'normalize-space(//*[@id="yui_3_17_2_1_1558996651487_32"]/div[1]/div/div/div/h1/text())',
        'normalize-space(//*[@id="page-header"]/div/div/div/div[1]/div/div/div/h1/text())'
        ]        
        # print('hola3-------',arr_name[cont])
        if(cont>=len(arr_name)):
            return  None
        else:
            try:
                nombre_curso = response.xpath(arr_name[cont]).extract()[0]
            except Exception as ex:
                nombre_curso = response.xpath(arr_name[cont]).extract()
            if len(nombre_curso) == 0 :
                cont = cont + 1
                return self.search_value(cont,response)
            else:
                return nombre_curso

    def parse_detail_page(self, response):
        fechas = [
            '//*[@id="section-0"]/div[3]/div[2]/div/p[7]','//*[@id="yui_3_17_2_1_1552086675932_50"]',
            '//*[@id="section-0"]/div[3]/div[2]/div/p[3]','//*[@id="yui_3_17_2_1_1552086685083_50"]'
        ]



        nombre_curso = self.search_value(0,response)        
        
        if(nombre_curso is not None):

            for url in fechas:
                fecha_curso = response.xpath('normalize-space('+url+')').extract()[0]
                if len(fecha_curso) >0:
                    fecha_curso = fecha_curso.lower()
                    if ('vent' not in fecha_curso and 'que' not in fecha_curso 
                    and 'todos' not in fecha_curso and 'marco' not in fecha_curso and 'desarr' not in fecha_curso):
                        break

            tipo = 'No Formal'
            url = response.url
                
            
            ws_item = Training()
            ws_item["id_nombre"] = nombre_curso.upper().replace(" ","")
            ws_item["nombre"] = nombre_curso
            ws_item["tipo"] = 'No Formal'
            ws_item["ubicacion"] = self.ubicacion
            ws_item["email"]='---'
            ws_item["fecha"]= fecha_curso
            ws_item["institucion"]= self.institucion
            ws_item["imagen"] = "/static/main/index/images/training-default.png"
            ws_item["url"] = url
            ws_item["categoria"] = "4"
            ws_item["order"] = "1"    
            yield ws_item
        
            
         
# Ejemplo #
"""   
https://scrapy.readthedocs.io/en/latest/topics/spiders.html#scrapy.spider.Spider.start_requests
 def start_requests(self):
    yield Request('http://a.com', self.parse_a)
    yield Request('http://b.com', self.parse_b)
    yield Request('http://c.com', self.parse_data)

    def parse_a(self, response):
        links = LinkExtractor(
            # ... extract links from http://a.com
        ).extract_links(response)
        return (Request(url=link.url, callback=self.parse_data) for link in links)

    def parse_b(self, response):
        links = LinkExtractor(
            # ... extract links from http://b.com
        ).extract_links(response)
        return (Request(url=link.url, callback=self.parse_data) for link in links)
"""
# fin Ejemplo
