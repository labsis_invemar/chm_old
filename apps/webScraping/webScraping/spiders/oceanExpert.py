import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.exceptions import CloseSpider
from webScraping.items import People, Institution
import re

class oceanExpert(CrawlSpider):
    arr_paises=[
        'Argentina',
        'Colombia', 
        'Brazil',
        'Ecuador'
        
        ]
    urls_institutcion = [ 'https://www.oceanexpert.net/advanceSearch?action=browse&countryName='+pais[0]+'&type=experts&filterCountry='+pais+'&page=1' for pais in arr_paises]
    name = 'oceanExpert'
    item_count = 0
    allowed_domain = ["https://www.oceanexpert.net/"]
    start_urls = urls_institutcion

    # rules = {
    #     Rule(LinkExtractor(allow = (),restrict_xpaths = ('//a[@rel="next"]')  )),
    #     Rule(LinkExtractor(allow = (),restrict_xpaths = ('//p[@class="group inner list-group-item-text"]')),
    #     callback = 'parse_item',follow = False)
    # }
    def parse(self, response):
        url = None
        number_pagination = response.css('ul[class*="pagination"]>li:nth-last-child(2)>a::text').extract()        
        item_links = response.css('a[rel=next]::attr(href)').extract()        
        if(len(number_pagination)>0):                  
            print('number--------------------->',number_pagination)
            print('link--------------------->',item_links[0])
            if item_links[0].endswith('page=2'):
                url = item_links[0][:-6]               
            print('url---------------------->',url)             
            for a in range(int(number_pagination[0])):                
                yield scrapy.Request('https://www.oceanexpert.net'+url+'page='+str(a+1), callback=self.parse_card)

    def parse_card(self, response):
        print('entre-------dsadsa-----------')
        item_links = response.css('p.group.inner.list-group-item-text a::attr(href)').extract()
        #item_links = response.xpath('normalize-space(//a[contains(@class, "main-course-item")]/@href)').extract()
        print("link-emagister: ",item_links)
        for a in item_links:
            if a != "":
                yield scrapy.Request('https://www.oceanexpert.net'+a, callback=self.parse_item)
    
    
    def parse_item(self, response):
        imagenP = response.xpath('normalize-space(/html/body/div[1]/div/section[2]/div/div[1]/div[1]/div/img/@data-original)').extract()
        nombreP = response.xpath('normalize-space(/html/body/div[1]/div/section[2]/div/div[1]/div[1]/div/h2)').extract()
        nombreI = response.xpath('normalize-space(/html/body/div/div/section[2]/div/div[1]/div[1]/div/p[1])').extract()
        url = response.xpath('normalize-space(/html/body/div[1]/div/section[2]/div/div[1]/div[1]/div/h4/strong)').extract()
        imagenI = response.xpath('normalize-space(/html/body/div/div/section[2]/div/div[1]/div[1]/div/div/a/img/@src)').extract()
        
        
        
        nombre = ""
        imagen = ""
        if nombreP[0] != "":
            ws_item = People()
            nombre = nombreP[0]
            imagen =  "https://www.oceanexpert.net" + imagenP[0] if imagenP[0] != "" else "/static/main/index/images/user-default.png"
            url = "https://www.oceanexpert.net/expert/" +url[0]
            ws_item["institucion"] = response.xpath('normalize-space(/html/body/div[1]/div/section[2]/div/div[1]/div[1]/div/p[3]/a)').extract()[0]
            ws_item["ocupacion"] = response.xpath('normalize-space(/html/body/div[1]/div/section[2]/div/div[1]/div[1]/div/p[1])').extract()[0]
            ws_item["aptitudes"] = response.xpath('normalize-space(//*[@id="profile"]/div[8]/p)').extract()[0]
            # ws_item["pais"]= response.xpath('normalize-space(/html/body/div[1]/div/section[2]/div/div[1]/div[2]/div/p[1])').extract()[0]
            ws_item["categoria"] = '1'
        elif nombreI[0] != "": 
            ws_item = Institution()
            nombre = nombreI[0]
            imagen = "https://www.oceanexpert.net"+imagenI[0] if imagenI[0] != "" else "/static/main/index/images/inst-default.png"
            url = "https://www.oceanexpert.net/institution/" +url[0]
            ws_item["direccion"] = response.xpath('normalize-space(/html/body/div/div/section[2]/div/div[1]/div[2]/div/div/p)').extract()[0]
            ws_item["categoria"] = '2'
        ws_item["nombre"] = nombre
        ws_item["imagen"] = imagen
        ws_item["url"] = url
        nombre = nombre.replace(" ","")
        ws_item["id_nombre"] = nombre.upper()
        #ws_item["descripcion"] = response.xpath('normalize-space(//div[@class="box-body"])').extract()
        self.item_count += 1
        if self.item_count > 10:
            raise CloseSpider('item_exceeded')
        yield ws_item

# Ejemplo #
"""   
https://scrapy.readthedocs.io/en/latest/topics/spiders.html#scrapy.spider.Spider.start_requests
 def start_requests(self):
    yield Request('http://a.com', self.parse_a)
    yield Request('http://b.com', self.parse_b)
    yield Request('http://c.com', self.parse_data)

    def parse_a(self, response):
        links = LinkExtractor(
            # ... extract links from http://a.com
        ).extract_links(response)
        return (Request(url=link.url, callback=self.parse_data) for link in links)

    def parse_b(self, response):
        links = LinkExtractor(
            # ... extract links from http://b.com
        ).extract_links(response)
        return (Request(url=link.url, callback=self.parse_data) for link in links)
"""
# fin Ejemplo
