import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.exceptions import CloseSpider
from webScraping.items import *
import pymongo
from scrapy.conf import settings
import re

class ScrapingCHM(CrawlSpider):
    name = 'scrapingCHM'
    def __init__(self,id_scraping=''):
        connection = pymongo.MongoClient(
            settings['MONGODB_SERVER'],
            settings['MONGODB_PORT']
        )
        super(ScrapingCHM, self).__init__()
        self.db = connection[settings['MONGODB_DB']]
        self.collection = self.db['admins_scraping']
        self.collection2 = self.db['admins_scrapingfield']
        myquery = { "id": int(id_scraping) }
        self.scraping = self.collection.find_one(myquery)
        fieldquery = {"scraping_id": int(id_scraping) }
        self.scrapingField = self.collection2.find(fieldquery)
        self.list_field = list(self.scrapingField)
        self.allowed_domain = [self.scraping["dominio"]]
        self.start_urls = [self.scraping["starturl"]]
        # print("llego al scrapnig: ")
        
    
    item_count = 0
    
    def parse(self, response):          
        if self.scraping["pagination"] == 'yes': 
            a = int(self.scraping["page_init"])
            while a < int(self.scraping["page_end"]):        
            #for a in range(int(self.scraping["page_end"])):  
                a +=int(self.scraping["increment"])              
                yield scrapy.Request(self.scraping["starturl"]+self.scraping["connector"]+self.scraping["page_key"]+'='+str(a+1), callback=self.parse_card)

    def parse_card(self, response):
        item_links = response.css(self.scraping["classItem"]).extract()
        for a in item_links:
            if a != "":
                # print("item_link: ",a)
                yield scrapy.Request(self.scraping["rootItem"]+a, callback=self.parse_item)
    
    
    def parse_item(self, response):
        ws_item = None
        id_item = ''
        imagen_default = ''
        if(self.scraping["category"] == "experts"):
            ws_item = People()
            imagen_default = "/static/main/index/images/user-default.png"
            ws_item["categoria"] = '1'
        elif(self.scraping["category"] == "institutions"):
            ws_item = Institution()
            imagen_default = "/static/main/index/images/inst-default.png"
            ws_item["categoria"] = '2'
        elif(self.scraping["category"] == "documents"):
            ws_item = Document()
            imagen_default = "/static/main/index/images/doc-default.png"
            ws_item["categoria"] = '3'
        
        
        for field in self.list_field:
            # print("field: ",field)
            field_web = response.xpath('normalize-space('+field["field_web"]+')').extract()
            if field["field_chm"] == "imagen":
                imagen = field["root_item"] + field_web[0] if field_web[0] != "" else imagen_default
                ws_item["imagen"] = imagen
            if field["field_chm"] == "nombre":
                ws_item["nombre"] = field_web[0]
                id_item = field_web[0].replace(" ","")
                id_item = id_item.upper()
            if field["field_chm"] == "institucion":
                ws_item["institucion"] = field_web[0]
            if field["field_chm"] == "ocupacion":
                ws_item["ocupacion"] = field_web[0]
            if field["field_chm"] == "pais":
                if "Please login to see the email" not in field_web[0]:
                    ws_item["pais"] = field_web[0]
            if field["field_chm"] == "direccion":
                ws_item["direccion"] = field_web[0]
            if field["field_chm"] == "fecha":
                ws_item["fecha"] = field_web[0][-4:]
            if field["field_chm"] == "resumen":
                ws_item["resumen"] = field_web[0][0:200]+"..."
          


        ws_item["source"] = self.scraping["name"]
        ws_item["source_id"] = self.scraping["id"]
        ws_item["method_harvesting"] = 'scraping'
        ws_item["url"] = response.url
        ws_item["id_nombre"] = id_item

        self.item_count += 1
        if self.item_count > 500:
            raise CloseSpider('item_exceeded')
        yield ws_item
