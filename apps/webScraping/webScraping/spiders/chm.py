import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.exceptions import CloseSpider


class CHMspider(CrawlSpider):

    name = 'chm'

    def __init__(self, *args, **kwargs):
        self.url = kwargs.get('url')
        self.domain = kwargs.get('domain')
        self.start_urls = [self.url]
        self.allowed_domains = [self.domain]

        CHMspider.rules = [
            Rule(LinkExtractor(unique=True),callback='parse_item')
        ]
        super(CHMspider, self).__init__(*args, **kwargs)

    def parse_item(slef, response):
        i = {}
        i['url'] = response.url
        return i

