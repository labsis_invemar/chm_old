import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.exceptions import CloseSpider
from webScraping.items import Document

class oceanDocs(CrawlSpider):
    name = 'oceanDocs'
    item_count = 0
    allowed_domain = ["https://www.oceandocs.org"]
    start_urls = ['https://www.oceandocs.org/handle/1834/1338/recent-submissions?offset=0']
    rules = {                                                     
        Rule(LinkExtractor(allow = (),restrict_xpaths = ('//*[@id="aspect_discovery_recentSubmissions_RecentSubmissionTransformer_div_main-recent-submissions"]/div[3]/ul/li[2]/a')  )),
        Rule(LinkExtractor(allow = (),restrict_xpaths = ('//*[@id="aspect_discovery_recentSubmissions_RecentSubmissionTransformer_div_recent-submissions"]/ul/li[1]/div/div[2]/div/h4/a')),
        callback = 'parse_item',follow = False)
    }

    def parse_item(self, response):
        imagen = response.xpath('normalize-space(//*[@id="aspect_artifactbrowser_ItemViewer_div_item-view"]/div/div/div[1]/div[1]/div[1]/div/img/@src)').extract()[0]
        nombre = response.xpath('normalize-space(//*[@id="aspect_artifactbrowser_ItemViewer_div_item-view"]/div/h2)').extract()[0]
        resumen = response.xpath('normalize-space(//*[@id="aspect_artifactbrowser_ItemViewer_div_item-view"]/div/div/div[2]/div[1]/div)').extract()[0]
        fecha = response.xpath('normalize-space(//*[@id="aspect_artifactbrowser_ItemViewer_div_item-view"]/div/div/div[1]/div[2])').extract()[0]
        pais = response.xpath('normalize-space(/html/body/div[1]/div/div/div/ul/li[3]/a)').extract()[0]
        if pais == 'SPINCAM':
            pais = response.xpath('normalize-space(/html/body/div[1]/div/div/div/ul/li[4]/a)').extract()[0]
        url = response.url
        ws_item = Document()
        ws_item["nombre"] = nombre
        nombre = nombre.replace(" ","")
        nombre = nombre[0:20]
        ws_item["id_nombre"] = nombre.upper()
        ws_item["resumen"] = resumen[0:200]+"..."
        ws_item["url"] = url
        ws_item["imagen"] = "https://www.oceandocs.org"+ imagen if imagen != "" else "/static/main/index/images/doc-default.png"
        ws_item["fecha"] = fecha[-4:]
        ws_item["categoria"] = "3"
        ws_item["pais"] = pais

        self.item_count += 1
        if self.item_count > 200:
            raise CloseSpider('item_exceeded')
        yield ws_item



class oceanDocsDC(CrawlSpider):
    name = 'oceanDocsDC'
    item_count = 0
    start_urls = ['https://aquadocs.org/oai/request?verb=ListRecords&metadataPrefix=oai_dc']
    allowed_domain = ["https://www.oceandocs.org"]
    rules = {                                                     
        Rule(LinkExtractor(allow = (),restrict_xpaths = ('//*[@id="aspect_discovery_recentSubmissions_RecentSubmissionTransformer_div_main-recent-submissions"]/div[3]/ul/li[2]/a')  )),
        Rule(LinkExtractor(allow = (),restrict_xpaths = ('//*[@id="aspect_discovery_recentSubmissions_RecentSubmissionTransformer_div_recent-submissions"]/ul/li[1]/div/div[2]/div/h4/a')),
        callback = 'parse_item',follow = False)
    }

    def parse_item(self, response):
        imagen = response.xpath('normalize-space(//*[@id="aspect_artifactbrowser_ItemViewer_div_item-view"]/div/div/div[1]/div[1]/div[1]/div/img/@src)').extract()[0]
        nombre = response.xpath('normalize-space(//*[@id="aspect_artifactbrowser_ItemViewer_div_item-view"]/div/h2)').extract()[0]
        resumen = response.xpath('normalize-space(//*[@id="aspect_artifactbrowser_ItemViewer_div_item-view"]/div/div/div[2]/div[1]/div)').extract()[0]
        fecha = response.xpath('normalize-space(//*[@id="aspect_artifactbrowser_ItemViewer_div_item-view"]/div/div/div[1]/div[2])').extract()[0]
        pais = response.xpath('normalize-space(/html/body/div[1]/div/div/div/ul/li[3]/a)').extract()[0]
        if pais == 'SPINCAM':
            pais = response.xpath('normalize-space(/html/body/div[1]/div/div/div/ul/li[4]/a)').extract()[0]
        url = response.url
        ws_item = Document()
        ws_item["nombre"] = nombre
        nombre = nombre.replace(" ","")
        nombre = nombre[0:20]
        ws_item["id_nombre"] = nombre.upper()
        ws_item["resumen"] = resumen[0:200]+"..."
        ws_item["url"] = url
        ws_item["imagen"] = "https://www.oceandocs.org"+ imagen if imagen != "" else "/static/main/index/images/doc-default.png"
        ws_item["fecha"] = fecha[-4:]
        ws_item["categoria"] = "3"
        ws_item["pais"] = pais

        self.item_count += 1
        if self.item_count > 200:
            raise CloseSpider('item_exceeded')
        yield ws_item
# Ejemplo #
"""   
https://scrapy.readthedocs.io/en/latest/topics/spiders.html#scrapy.spider.Spider.start_requests
 def start_requests(self):
    yield Request('http://a.com', self.parse_a)
    yield Request('http://b.com', self.parse_b)
    yield Request('http://c.com', self.parse_data)

    def parse_a(self, response):
        links = LinkExtractor(
            # ... extract links from http://a.com
        ).extract_links(response)
        return (Request(url=link.url, callback=self.parse_data) for link in links)

    def parse_b(self, response):
        links = LinkExtractor(
            # ... extract links from http://b.com
        ).extract_links(response)
        return (Request(url=link.url, callback=self.parse_data) for link in links)
"""
# fin Ejemplo
