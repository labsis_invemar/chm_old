import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.exceptions import CloseSpider
from webScraping.items import Training
import datetime

class emagister(CrawlSpider):
    name = 'emagister'
    item_count = 0
    allowed_domain = ["https://www.emagister.com"]
    start_urls = ['https://www.emagister.com/cursos-oceanografia-online-kwonline-37455.htm']
    #rules = {
    #    Rule(LinkExtractor(allow =()), restrict_xpaths = ("/html/body/div[1]/div/main/div/div/section/div/ul/li[3]/div/span"), callback = 'parse_item',follow = False)
    #}
#//a[contains(@class, "main-course-item")]/@href
    def parse(self, response):

        item_links = response.css('.course-title-link::attr(href)').extract()
        #item_links = response.xpath('normalize-space(//a[contains(@class, "main-course-item")]/@href)').extract()
        print("link-emagister: ",item_links)
        for a in item_links:
            if a != "":
                yield scrapy.Request(a, callback=self.parse_detail_page)


    def parse_detail_page(self, response):
        imagen = response.xpath('normalize-space(/html/body/div[1]/div/main/div/div/div[1]/aside/section[2]/div/ul/li[1]/span/img/@src)').extract()[0]
        nombre = response.xpath('normalize-space(/html/body/div[1]/div/main/div/div/div[1]/article/section[1]/h1)').extract()[0]
        resumen = response.xpath('normalize-space(//*[@id="course-details-id"]/div/div[1]/p)').extract()[0]
        fecha = response.xpath('normalize-space(//*[@id="course-details-id"]/div/div[3]/div[2]/div[1]/table/tbody/tr/td[1]/span)').extract()[0]
        ubicacion = response.xpath('normalize-space(//*[@id="course-details-id"]/div/div[3]/div[2]/div[1]/table/tbody/tr/td[2]/div/div/span)').extract()[0]
        url = response.url
        ws_item = Training()
        ws_item["nombre"] = nombre
        nombre = nombre.replace(" ","")
        nombre = nombre[0:20]
        ws_item["id_nombre"] = nombre.upper()
        ws_item["resumen"] = resumen[0:200]+"..."
        ws_item["url"] = url
        ws_item["imagen"] = imagen if imagen != "" else "/static/main/index/images/training-default.png"
        ws_item["fecha"] = fecha
        ws_item["ubicacion"] = ubicacion
        ws_item["categoria"] = "4"
        ws_item["order"] = "1"

        self.item_count += 1
        if self.item_count > 200:
            raise CloseSpider('item_exceeded')
        yield ws_item

# Ejemplo #
"""   
https://scrapy.readthedocs.io/en/latest/topics/spiders.html#scrapy.spider.Spider.start_requests
 def start_requests(self):
    yield Request('http://a.com', self.parse_a)
    yield Request('http://b.com', self.parse_b)
    yield Request('http://c.com', self.parse_data)

    def parse_a(self, response):
        links = LinkExtractor(
            # ... extract links from http://a.com
        ).extract_links(response)
        return (Request(url=link.url, callback=self.parse_data) for link in links)

    def parse_b(self, response):
        links = LinkExtractor(
            # ... extract links from http://b.com
        ).extract_links(response)
        return (Request(url=link.url, callback=self.parse_data) for link in links)
"""
# fin Ejemplo
