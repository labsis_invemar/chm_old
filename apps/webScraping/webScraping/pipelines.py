# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import pymongo
from scrapy.conf import settings
from scrapy.exceptions import DropItem
from scrapy import log

import json


class MongoDBPipeline(object):

    def __init__(self):
        connection = pymongo.MongoClient(
            settings['MONGODB_SERVER'],
            settings['MONGODB_PORT']
        )
        self.db = connection[settings['MONGODB_DB']]
        

    def process_item(self, item, spider):
        valid = True
        for data in item:
            if not data:
                valid = False
                raise DropItem("Missing {0}!".format(data))
        if valid:
            if item['categoria'] == '1':
                self.collection = self.db['main_people']
            elif item['categoria'] == '2':
                self.collection = self.db['main_institution']
            elif item['categoria'] == '3':
                self.collection = self.db['main_document']
            elif item['categoria'] == '4':
                self.collection = self.db['main_training']

            dato = self.collection.find_one({'id_nombre':item['id_nombre']}) 
            
            if dato: 
                self.collection.update({'id_nombre':item['id_nombre']}, dict(item), True) 
                
            else:
                
                self.collection.insert(dict(item))
                collection2 = self.db['admins_scraping']
                dato2 = collection2.find_one({'name':item['source']})
                
                myquery = { 'name':item['source']}
                newvalues = { "$set": { 'acumulado':int(dato2["acumulado"])+1 } }
                collection2.update_one(myquery, newvalues) 
                
            #self.collection.update({'id_nombre':item['id_nombre']}, dict(item), True)
            log.msg("Question added to MongoDB database!",
                    level=log.DEBUG, spider=spider)
        return item