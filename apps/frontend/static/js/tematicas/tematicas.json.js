export default {
    arr_tematicas_up: [{
            title: "Experts",
            class: 'experts',
            icon: 'icon-experts',
            description: "Information on people.",
            class_col: 'col-md-4 col-10',
            show: true
        },
        {
            title: "Documents",
            class: 'documents',
            icon: 'icon-documentos',
            description: " Mainly grey literature, manuals and guidelines.",
            class_col: 'col-md-4 col-10',
            show: true
        },
        {
            title: "Training",
            class: 'training',
            icon: 'icon-entren',
            description: " Training and education opportunities",
            class_col: 'col-md-4 col-10',
            show: true
        },
        {
            title: "Laboratories",
            class: 'laboratories',
            icon: 'icon-lab',
            description: "Information on Laboratories.",
            class_col: 'col-md-3 col-10',
            show: true
        }, {
            title: "Institutions",
            class: 'institutions',
            icon: 'icon-instituciones',
            description: "  Information on institutions.",
            class_col: 'col-md-3 col-10',
            show: true
        }, {
            title: "Geospatial information",
            class: 'geo',
            icon: 'icon-geo',
            description: "  Maps and atlases.",
            class_col: 'col-md-3 col-10',
            show: true
        }, {
            title: "Vessels",
            class: 'vess',
            icon: 'icon-boat',
            description: " Information of vessels.",
            class_col: 'col-md-3 col-10',
            show: true
        }
    ]

}