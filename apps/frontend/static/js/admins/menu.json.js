export default {
  arr_menu_up: [
    {
      title: 'Web service',
      class: 'webservice',
      icon: 'icon-webservice',
      description: 'Create web service',
      button: 'Add new Web Service',
      class_col: 'col-md-4 col-10',
    },
    {
      title: 'OAI Repository',
      class: 'oai',
      icon: 'icon-webservice',
      description: 'Create new OAI Repository harvester',
      button: 'Add new repository',
      class_col: 'col-md-4 col-10',
    },
    {
      title: 'Scraping',
      class: 'scraping',
      icon: 'icon-scraping',
      button: 'Add new Scraping',
      description: 'Create Scraping',
      class_col: 'col-md-4 col-10',
    },
    {
      title: 'Upload File',
      class: 'upload',
      icon: 'icon-scraping',
      button: 'Add new File',
      description: 'Create Scraping',
      class_col: 'col-md-4 col-10',
    },
    {
      title: 'Users',
      class: 'users',
      icon: 'icon-user',
      description: 'Create User',
      class_col: 'col-md-4 col-10',
    },
    ,
     {
        title: "Api Reader",
        class: 'apireader',
        icon:'icon-apireader',
        description: "Create Api Reader",
        class_col: 'col-md-4 col-10'
    },
  ],
}
