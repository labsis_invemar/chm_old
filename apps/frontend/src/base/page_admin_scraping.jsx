import React from 'react';

import HeaderPageAdmins from '../components/header/header_page_admins.jsx';
import Scraping from '../components/admins/form_scraping.jsx';
// import Pagination from './Pagination';
// import StockList from './StockList';
// import SearchStock from './SearchStock';

export default class PageScraping extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            id:undefined,
            menu:undefined,
            name:undefined,
            result:undefined
        }

        
    }
    componentWillMount(){
        let id_scr = this.props.location.search.split("?=")[1];
        this.setState({
            id: id_scr
        });

        if(this.props.location.state){
            
            if(this.props.location.state.name){
                // console.log("this.props.location.state.name: ",this.props.location.state.name)
                this.setState({name:this.props.location.state.name});
            }
            if(this.props.location.state.result){
                // console.log("this.props.location.state.result: ",this.props.location.state.result)
                this.setState({result:this.props.location.state.result});
            }
        }
    }

    componentDidMount() {
        if(this.props.location.state){
            
            if(this.props.location.state.menu){
                this.setState({menu:this.props.location.state.menu})
            }
        }
        //console.log("this.state", this.state)
    }


    render() {

        return (
            <div>
                <HeaderPageAdmins menu={this.state.menu} modulo="form"  user={this.props.user}/>
                <Scraping {...this.state} />
            </div>
        )
    }
}    