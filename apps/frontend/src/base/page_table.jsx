import React from 'react';
import HeaderPageTable from '../components/header/header_page_table.jsx';
import Table from '../components/table/table.jsx';


export default class PageTable extends React.Component {

    constructor(props) {
        super(props);
        // console.log('pagina padre', props);

        this.state = {
            tematica: undefined,
            search: undefined,
            search_temathic: {},
            page_current: 1
        }

        // this.childHeaderPHandler = this.childHeaderPHandler.bind(this);
        this.childTableHandler = this.childTableHandler.bind(this);
        this.childHeaderHandler = this.childHeaderHandler.bind(this);
    }

    componentDidMount() {

        this.props.emmmiter.emit('foo', { "show": true });
        let tematica = null;
        let search = null;
        let page_current = 1;
        // console.log(('-------- constriuctor', this.props.location));
        if (this.props.location.state) {
            if (this.props.location.state.tematica) {
                tematica = this.props.location.state.tematica;
                localStorage.setItem('tematica', JSON.stringify(tematica));
            }
            if (this.props.location.state.search) {
                search = this.props.location.state.search;
                localStorage.setItem('search', JSON.stringify(search));
            }
            if (this.props.location.state.card) {
                localStorage.setItem('search', JSON.stringify(null));
            }
        } else {
            tematica = JSON.parse(localStorage.getItem('tematica'));
            search = JSON.parse(localStorage.getItem('search'));
            page_current = JSON.parse(localStorage.getItem('page_current'));
        }
        this.setState({
            tematica: tematica,
            search: search,
            page_current: page_current
        })
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.location.search != this.props.location.search) {
            let tematica = nextProps.location.search.split("?=")[1];
            this.setState({
                tematica: tematica,
                search: decodeURIComponent(this.state.search)
            })
        }


    }

    childTableHandler(data) {
        // console.log('---manejador tabla--', data);

        this.setState({
            search_temathic: data.search_temathic,
            tematica: data.categoria,
            page_current: data.page_current,
            search: data.search
        }, () => {
            localStorage.setItem('tematica', JSON.stringify(data.categoria));
            localStorage.setItem('search', JSON.stringify(data.search));
            localStorage.setItem('page_current', JSON.stringify(data.page_current));
        })

    }

    childHeaderHandler(data) {
        // console.log('manejador encabezado---', data);

        this.setState({
            page_current: data.page_current,
            tematica: data.tematica
        })

    }


    render() {

        return (
            <div>
                <HeaderPageTable {...this.state} action={this.childHeaderHandler} emmmiter={this.props.emmmiter} />
                <Table {...this.state} action={this.childTableHandler} emmmiter={this.props.emmmiter} />
            </div>
        )
    }
}    