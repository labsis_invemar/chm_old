import React from 'react';
import HeaderPageStatistics from '../components/header/header_page_statistics.jsx';
import TableStatistics from '../components/table/table_statistics.jsx';
import axios from 'axios';
import initial from '../providers/initial.jsx';


export default class PageStatistics extends React.Component {

    constructor(props) {
        super(props);
        // console.log('pagina Stadisticas', props);

        this.state = {
            option: 1,
            sta_paises: [],
            sta_document: [],
            data_header: [],
            data: [],
            show_loader: true
        }

        this.childHeaderHandler = this.childHeaderHandler.bind(this);
        this.get_data_ajax = this.get_data_ajax.bind(this);
        this.builder_satate = this.builder_satate.bind(this);

    }

    childHeaderHandler(e) {

        this.setState({ option: e.option, show_loader: true }, () => {
            // console.log('padre estadsitica ---', this.state.option);
            this.get_data_ajax(this.state.option);
        })

    }

    componentDidMount() {
        this.get_data_ajax(this.state.option);
    }

    get_data_ajax(data) {
        axios.get(initial.url + 'statistics/' + this.state.option + '/data/', {
            params: data
        }).then((response) => {
            //handle success
            this.builder_satate(response);

        }).catch((response) => {
            console.log('error ----', response);
        })
    }

    builder_satate(response) {

        // console.log(' ---- data --', response.data);
        this.setState({
            sta_paises: response.data.sta_paises,
            sta_document: response.data.sta_document,
            data_header: response.data.data_header,
            data: response.data.data,
            show_loader: false
        })
    }



    render() {
        return (
            <div>
                <HeaderPageStatistics action={this.childHeaderHandler} />
                <TableStatistics data={this.state.data} header={this.state.data_header} option={this.state.option} show_loader={this.state.show_loader} />

            </div>
        )
    }
}