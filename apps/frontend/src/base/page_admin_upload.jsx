import React from 'react';

import HeaderPageAdmins from '../components/header/header_page_admins.jsx';
import FormUploadF from '../components/admins/form_uploadf.jsx';
//import LoadFile from '../components/admins/form_loadfile.jsx';


export default class PageUpload extends React.Component {

    constructor(props) {
        super(props);
        // console.log('pagina padre', props);

        this.state = {
            data: undefined,
            menu: undefined,
        }

    }

    componentWillMount() {
    }

    componentDidMount() {
        if (this.props.location.state) {

            if (this.props.location.state.menu) {
                this.setState({ menu: this.props.location.state.menu })
            }
        }
        //console.log("this.state", this.state)
    }


    render() {

        return (
            <div>
                <HeaderPageAdmins user={this.props.user} menu={this.state.menu} modulo="form" />
                <FormUploadF {...this.state} />
            </div>
        )
    }
}    