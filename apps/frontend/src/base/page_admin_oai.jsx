import React from 'react';

import HeaderPageAdmins from '../components/header/header_page_admins.jsx';
import FormOai from '../components/admins/form_oai.jsx';
//import LoadFile from '../components/admins/form_loadfile.jsx';


export default class PageUpload extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: undefined,
            menu: undefined,
        }

    }

    componentWillMount() {
        let id_oai = this.props.location.search.split("?=")[1];
        this.setState({
            id: id_oai
        });
        if(this.props.location.state){
            if(this.props.location.state.name){
                this.setState({name:this.props.location.state.name});
            }
            if(this.props.location.state.result){
                this.setState({result:this.props.location.state.result});
            }
        }
    }

    componentDidMount() {
        if (this.props.location.state) {

            if (this.props.location.state.menu) {
                this.setState({ menu: this.props.location.state.menu })
            }
        }
        //console.log("this.state", this.state)
    }


    render() {

        return (
            <div>
                <HeaderPageAdmins user={this.props.user} menu={this.state.menu} modulo="form" />
                <FormOai {...this.state} />
            </div>
        )
    }
}    