import React from 'react';
import HeaderPageAdmins from '../components/header/header_page_admins.jsx';
import FormUsers from '../components/admins/form_users.jsx';


export default class PageAdminUsers extends React.Component {

    constructor(props) {
        super(props);
        // console.log('pagina padre', props);
        this.state = {
            id:undefined,
            menu:undefined,
            name:undefined,
            result:undefined
        }

        
    }

    componentWillMount(){
        let id_scr = this.props.location.search.split("?=")[1];
        this.setState({
            id: id_scr
        });
        if(this.props.location.state){
            if(this.props.location.state.name){
                // console.log("this.props.location.state.name: ",this.props.location.state.name)
                this.setState({name:this.props.location.state.name});
            }
            if(this.props.location.state.result){
                // console.log("this.props.location.state.result: ",this.props.location.state.result)
                this.setState({result:this.props.location.state.result});
            }
        }
    }

    componentDidMount() {
        if(this.props.location.state){            
            if(this.props.location.state.menu){
                this.setState({menu:this.props.location.state.menu})
            }
        }
    }

    render() {
        return (
            <div>
                <HeaderPageAdmins menu={this.state.menu} modulo="form"  user={this.props.user}/>
                <FormUsers {...this.state} />
            </div>
        )
    }
}    