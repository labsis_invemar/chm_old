import React from 'react';

import HeaderPageAdmins from '../components/header/header_page_admins.jsx';
import FormApireader from '../components/admins/form_apireader.jsx';


export default class PageApiReader extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            id:undefined,
            menu:undefined,
            name:undefined,
            result:undefined
        }

        
    }

    componentWillMount(){
        let id_rdr = this.props.location.search.split("?=")[1];
        this.setState({
            id: id_rdr
        });

        if(this.props.location.state){
            
            if(this.props.location.state.name){
                // console.log("this.props.location.state.name: ",this.props.location.state.name)
                this.setState({name:this.props.location.state.name});
            }
            if(this.props.location.state.result){
                // console.log("this.props.location.state.result: ",this.props.location.state.result)
                this.setState({result:this.props.location.state.result});
            }
        }
    }


    componentDidMount() {
        if(this.props.location.state){
            
            if(this.props.location.state.menu){
                this.setState({menu:this.props.location.state.menu})
            }
        }
        //console.log("this.state", this.state)
    }


    render(){
        return(
            <div>
                <HeaderPageAdmins menu={this.state.menu} modulo="form"  user={this.props.user}/>
                <FormApireader {...this.state} />
            </div>
        )
    }
}