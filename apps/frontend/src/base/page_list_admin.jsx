import React from 'react';
import HeaderPageAdmins from '../components/header/header_page_admins.jsx';
import ListAmdin from '../components/admins/list_admin.jsx';
import ApireaderList from '../components/admins/apireader_list.jsx'
// import Pagination from './Pagination';
// import StockList from './StockList';
// import SearchStock from './SearchStock';

export default class PageListAdmin extends React.Component {

    constructor(props) {
        super(props);
        // console.log('pagina padre', props);

        this.state = {
            menu: undefined,
            search: undefined,
            page_current: 1
        }

        // this.childHeaderPHandler = this.childHeaderPHandler.bind(this);
        this.childTableHandler = this.childTableHandler.bind(this);
        this.childHeaderHandler = this.childHeaderHandler.bind(this);
    }

    componentDidMount() {
        // let tematica = this.props.location.search.split("?=")[1];
        // let search = this.props.location.search.split("?=")[2];
        // let page_current = this.props.location.search.split("?=")[3];
        let menu = null;
        let search = null;
        let page_current = 1;
        // console.log('-------- constriuctor', this.props);
        if (this.props.location.state) {
            if (this.props.location.state.menu) {
                menu = this.props.location.state.menu;
                localStorage.setItem('menu', JSON.stringify(menu));
            }
            if (this.props.location.state.search) {
                search = this.props.location.state.search;
                localStorage.setItem('search', JSON.stringify(search));
            }
            if(this.props.location.state.card){
                localStorage.setItem('search', JSON.stringify(null));
            } 
        }else{
            menu = JSON.parse(localStorage.getItem('menu'));
            search = JSON.parse(localStorage.getItem('search'));
            page_current = JSON.parse(localStorage.getItem('page_current'));
        }
        this.setState({
            menu: menu,
            search: search,
            page_current: page_current
        })
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.location.search != this.props.location.search) {
            let menu = nextProps.location.search.split("?=")[1];
            this.setState({
                menu: menu,
                search: decodeURIComponent(this.state.search)
            })
        }

    }

    childTableHandler(data) {
        // console.log('---manejador tabla--', data);

        this.setState({
            menu: data.categoria,
            page_current: data.page_current,
            search: data.search
        },()=>{
                localStorage.setItem('menu', JSON.stringify(data.categoria));
                localStorage.setItem('search_admin', JSON.stringify(data.search));
                localStorage.setItem('page_current_admin', JSON.stringify(data.page_current));
        })

    }

    childHeaderHandler(data) {
        // console.log('manejador encabezado---', data);

        this.setState({
            page_current: data.page_current,
            menu: data.menu
        })

    }


    render() {
        return (
            <div>
                <HeaderPageAdmins {...this.state} modulo="list" action={this.childHeaderHandler}  user={this.props.user} />
                {this.state.menu==="apireader" ?
                    <ApireaderList {...this.state} action={this.childTableHandler} logout={this.props.logout}/> :
                    <ListAmdin {...this.state} action={this.childTableHandler} logout={this.props.logout}/>
                }
            </div>
        )
    }
}    