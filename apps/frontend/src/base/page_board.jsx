import React from 'react';
import BoardBanner from '../components/board/board_banner.jsx';
import BoardCard from '../components/board/board_card.jsx';
import { translate } from "react-translate";
// import Pagination from './Pagination';
// import StockList from './StockList';
// import SearchStock from './SearchStock';

class PageBoard extends React.Component {

    constructor(props) {
        super(props);
        this.state = {}

    }

    //This will run first automatically
    componentWillMount() {
    }


    render() {
        let { t } = this.props;
        return (
            <div>
                <BoardBanner />
                <BoardCard />
                <footer>
                    <div className="container">
                        <div className="row align-items-center">
                            <div className="col">©2019, CHM LAC. {t('All rights reserved')}.</div>
                            <img src={"../../static/images/logos-footer-unesco.svg"} style={{ 'width': '50%' }}></img>
                        </div>
                    </div>
                </footer>
            </div>
        )

    }
}

export default translate('App')(PageBoard);