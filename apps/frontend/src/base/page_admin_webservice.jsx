import React from 'react';

import HeaderPageAdmins from '../components/header/header_page_admins.jsx';
import WebService from '../components/admins/form_webservice.jsx';
// import Pagination from './Pagination';
// import StockList from './StockList';
// import SearchStock from './SearchStock';

export default class PageTable extends React.Component {

    constructor(props) {
        super(props);
        // console.log('pagina padre', props);

        this.state = {
            id: undefined,
            name: undefined,
            menu:undefined,
            result: undefined
        }

    }
    componentWillMount(){
        let id_web = this.props.location.search.split("?=")[1];
        this.setState({
            id: id_web
        });
        if(this.props.location.state){
            if(this.props.location.state.name){
                this.setState({name:this.props.location.state.name});
            }
            if(this.props.location.state.result){
                this.setState({result:this.props.location.state.result});
            }
        }
    }

    componentDidMount() {
        if(this.props.location.state){
            
            if(this.props.location.state.menu){
                this.setState({menu:this.props.location.state.menu});
            }
            
            
        }
        
        /*if(this.props.location.state){
            if (this.props.location.state.id){
                id = this.props.location.state.id;
                localStorage.setItem('id_webservice', JSON.stringify(id));
            }
        }else{
            id = JSON.parse(localStorage.getItem('id_webservice'));
        }*/

    }


    render() {

        return (
            <div>
                <HeaderPageAdmins menu={this.state.menu} modulo="form" user={this.props.user}/>
                <WebService {...this.state} />
            </div>
        )
    }
}    