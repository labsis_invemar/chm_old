import React from 'react';
import ReactDOM from 'react-dom';
import App from './App.jsx';
import initial from './providers/initial.jsx';
// import 'bootstrap/dist/css/bootstrap.min.css';
import { HashRouter } from "react-router-dom";
// if (initial.mode=="production"){
//   console.log=()=>{}
// }
ReactDOM.render(
    <HashRouter> {/* envolvemos nuestra aplicación en el Router  */}       
      <App/>
    </HashRouter>,
  document.getElementById('react-app'),
);