import React from "react"
import axios from 'axios';
import initial from '../../providers/initial.jsx';

export default class CategoryField extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            fields: this.props.fields,
        };
    }

    componentWillMount() {        
    }

    componentWillReceiveProps(nextProps){
        //console.log("nextProps.fields.length: ",nextProps.fields.length)
        //console.log("category: ",nextProps.category)
        if(nextProps.fields.length==0){
            if("-9"!=nextProps.category){
                axios.get(initial.url + 'category_fields/', {
                    params: {'categoria': nextProps.category},
                    headers: {
                        Authorization: localStorage.getItem('token')
                    }
                  }).then((response) => {
                      //handle success
                      //console.log("fields-select: ",response);
                      this.setState({
                        fields: response.data.fields
                      });
                  }).catch((response) => {
                      console.log('error ----', response);
                  });
            }
        }
    }

    Capitalize(str){
        return str.charAt(0).toUpperCase() + str.slice(1);
        }
    

    render() {
        return (
            this.props.categoryField.map((val, idx)=> (
                                        
                <div className="form-row" key={idx}>
                    <div className="col-md-4 mb-3">
                        <label htmlFor={'local-'+idx}>Field Local</label>
                        <select className="form-control"
                        data-id={idx}
                        id={'fieldlocal-'+idx}
                        value={this.props.categoryField[idx].fieldlocal}
                        onChange={this.props.change}
                        name="fieldlocal">
                            <option value="-9">Select Field</option>
                            {
                                this.state.fields.map((field,i) =>(
                                    (field[field.key] != "None")? 
                                    field[field.key].trim()== ""?
                                    <option key={i} value={field.key}>{this.Capitalize(field.key)}</option>:<option key={i} value={field.key}>{field[field.key]}</option>:null
                                    
                                ))
                            }
                            
                        </select>
                    </div>
                    <div className="col-md-4 mb-3">
                        <label htmlFor={'web-'+idx}>Field Web</label>
                        <select className="form-control"
                        
                        data-id={idx}
                        id={'fieldweb-'+idx}
                        value={this.props.categoryField[idx].fieldweb}
                        onChange={this.props.change}
                        name="fieldweb">
                            <option value="-9">Select Field</option>
                            {
                                this.props.datoswebservice.map((val, i) =>(
                                <option key={i} value={val}>{val}</option> 
                                ))
                            }
                            
                        </select>
                    </div>
                    <div className="col-md-1">
                    <button 
                        className="btn btn-outline-danger alignment"
                        type='button' 
                        data-id={idx} 
                        value='remove' 
                        onClick={this.props.remove}>
                        <i className="fa fa-trash"></i> Remove
                    </button>
                    </div>

                </div> 
            ))

        )
    }

}