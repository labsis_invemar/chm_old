import React from "react"


export default class Headers extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    componentWillMount() {        
    }
    render() {
        return (
            
                this.props.headersFields.map((val, idx)=> (
                                        
                    <div className="form-row" key={idx}>
                        
                    <div className="col-md-4 mb-3">
                    <label htmlFor={'headerkey-'+idx}>key</label>
                    <input className="form-control"
                        type="text"
                        name="headerkey"
                        data-id={idx}
                        id={'headerkey-'+idx}
                        value={this.props.headersFields[idx].headerkey}
                        onChange={this.props.change} 
                        
                    />
                    </div>
                    <div className="col-md-4 mb-3">
                    <label htmlFor={'headervalue-'+idx}>Value</label>
                    <input
                        className="form-control"
                        type="text"
                        name="headervalue"
                        data-id={idx}
                        id={'headervalue-'+idx}
                        value={this.props.headersFields[idx].headervalue} 
                        onChange={this.props.change}
                        
                    />
                    </div>
                    <div className="col-md-1">
                    
                    <button 
                    className="btn btn-outline-danger alignment"
                    type='button' 
                    data-id={idx} 
                    value='remove' 
                    onClick={this.props.remove}>
                    <i className="fa fa-trash"></i> Remove
                    </button>
                    </div>
                    </div> 
                ))
                           
            
        )   
            
    }
        
}

