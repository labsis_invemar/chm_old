import React from "react"
import axios from 'axios';
import initial from '../../providers/initial.jsx';

export default class CategoryFieldApiReader extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fields: this.props.fields
        }
    }
    
    componentDidMount() { 
       
    }

    componentWillReceiveProps(nextProps){

        if(nextProps.fields.length==0){
            if("-9"!=nextProps.category){
                axios.get(initial.url + 'category_fields/', {
                    params: {'categoria': nextProps.category},
                    headers: {
                        Authorization: localStorage.getItem('token')
                    }
                    }).then((response) => {
                        //handle success
                      //   console.log("fields-select: ",response);
                        this.setState({
                        fields: response.data.fields
                        });
                    }).catch((response) => {
                        console.log('error ----', response);
                    });
                    }
        }else{
            this.setState({fields:nextProps.fields})
        }
        //console.log("field.lenght:",nextProps.fields.length)
    }

    Capitalize(str){
        return str.charAt(0).toUpperCase() + str.slice(1);
        }

    render(){
        return(
            this.props.categoryField.map((val,idx)=>(

                <div className="form-row" key={idx}>
                    <div className="col-md-3 mb-3">
                        <label htmlFor={'local-'+idx}>Field Local</label>
                        <select className="form-control"
                        data-id={idx}
                        id={'fieldlocal-'+idx}
                        value={this.props.categoryField[idx].fieldlocal}
                        onChange={this.props.change}
                        name="fieldlocal">
                            <option value="-9">Select Field</option>
                            {
                                this.state.fields.map((field,i) =>(
                                    (field[field.key] != "None")? 
                                    field[field.key].trim()== ""?
                                    <option key={i} value={field.key}>{this.Capitalize(field.key)}</option>:<option key={i} value={field.key}>{field[field.key]}</option>:null
                                ))
                            }
                        </select>
                    </div>


                    <div className="col-md-1 mb-3">
                    <label htmlFor={'item-'+idx}>Item</label> 
                    <input
                        className="form-control"
                        type="text"
                        name="item"
                        data-id={idx}
                        id={'item-'+idx}
                        value={this.props.categoryField[idx].item} 
                        onChange={this.props.change}
                    />
                    </div>

                    <div className="col-md-1 mb-3">
                        <label htmlFor={'web-'+idx}>Type Field</label>
                        <select className="form-control"
                        data-id={idx}
                        id={'type-field-'+idx}
                        value={this.props.categoryField[idx].typeField}
                        onChange={this.props.change}
                        name="typefield">
                            <option value="text">Text</option>
                            <option value="pk">PK</option>
                            <option value="image">Image</option>
                            
                        </select>
                    </div>
                    
                    <div className="col-md-1 mb-3">
                    <label htmlFor={'size-text-'+idx}>Size Text</label>
                    <input
                        className="form-control"
                        type="text"
                        name="sizetext"
                        data-id={idx}
                        id={'sizetext-'+idx}
                        value={this.props.categoryField[idx].sizetext} 
                        onChange={this.props.change}
                    />
                    </div>

                    <div className="col-md-1">
                    <button 
                        className="btn btn-outline-danger alignment"
                        type='button' 
                        data-id={idx} 
                        value='remove' 
                        onClick={this.props.remove}>
                        <i className="fa fa-trash"></i> Remove
                    </button>
                    </div>
                
                </div>
            ))
        )
    }

}

