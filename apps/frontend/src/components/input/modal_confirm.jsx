import React from "react"
import Parser from 'html-react-parser';


export default class ModalConfirm extends React.Component {

    constructor(props) {
        super(props);
        // console.log("props constructor",props)
        this.state = {
            result: this.props.result,
            text: this.props.text,
            error:this.props.error,
            show_loader: this.props.show_loader,
            id_element: this.props.id_element

        };
    }

    componentWillMount() {        
    }

    componentWillReceiveProps(nextProps){
        // console.log("entro a props",nextProps )
        this.setState({
            result: nextProps.result,
            text: nextProps.text,
            show_loader: nextProps.show_loader,
            error:nextProps.error,
            id_element: nextProps.id_element 
        })
    }
    render() {
        return (
            <div className="modal fade" id={this.props.idModal} tabIndex="-1" role="dialog" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="harvesting">{this.props.title}</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        
                        {
                            this.state.result?
                                <div className="alert alert-success a-form">
                                   {Parser(this.state.result)}
                                </div>
                            :this.state.show_loader ?
                                <div>{Parser(this.state.text)}
                                <div className="d-flex justify-content-center">
                                    <div className="spinner-grow text-primary" style={{ "width": '3rem', "height": '3rem' }} role="status">
                                        <span className="sr-only">Loading...</span>
                                    </div>
                                </div>
                                </div>
                            :this.state.error ?
                                <div className="alert alert-error a-form">
                                    {Parser(this.state.error)}
                                </div>
                            :<div>{Parser(this.state.text)}</div>
                        }
                        
                    </div>
                    <div className="modal-footer">
                        {
                            
                            this.state.result || this.state.error ?
                            <button type="button" className="btn btn-secondary" onClick={(e)=>this.props.actionOk(e)} data-dismiss="modal">OK</button>
                            :this.state.show_loader ? null:
                            <div><button type="button" className="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="button" className="btn btn-primary" onClick={(e)=>this.props.action(e,this.state.id_element)}>Yes</button>
                            </div>
                        }
                        </div>
                    </div>
                </div>
            </div>                  
               
                           
            
        )   
            
    }
        
}