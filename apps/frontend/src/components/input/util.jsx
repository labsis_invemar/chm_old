import axios from 'axios';
import initial from '../../providers/initial.jsx';

export function get_field_category(category){
        // console.log("llego a util con cat: ", category)
        if("-9"!=category){
            axios({
                method: 'post',
                url: initial.url + 'category_fields/',
                data: { 'categoria': category},
                xsrfCookieName: initial.csrftoken,
                xsrfHeaderName: initial.xcsrftoken,
                processData: false,
                contentType: false,
            }).then((response) => {
                //handle success
                // console.log("util-ok: ", response)
                return response.data.fields;
                
                /*this.setState({
                    fields: response.data.fields
                })*/
            }).catch((response) => {
                console.log("util-error: ", response)
                //handle error
                return 'error ----', response
                //console.log('error ----', response);
            });
            }
    }

   