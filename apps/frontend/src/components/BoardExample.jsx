import React from 'react';
import axios from 'axios';
// import Pagination from './Pagination';
// import StockList from './StockList';
// import SearchStock from './SearchStock';

class Board extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            stocks: [],
            totalPage: '',
            currentPage: 1,
            searching: false,
            editing: false,
            deleting: false,
            searchData: '',
            id: '',
        }

        this.tickerRef = React.createRef();
        this.openRef = React.createRef();
        this.closeRef = React.createRef();
        this.volumeRef = React.createRef();
        this.searchBoxRef = React.createRef();
        this.searchAllRef = React.createRef();
    }

    //This will run first automatically
    componentWillMount() {
        // console.log('hola mundo');
        
        // axios.get('http://localhost:8000/api/s').then(res => {
        //     console.log('respuesta',res);
            
        //     // var totalPage = (res.data.count) / (res.data.results.length)
        //     // this.setState({
        //     //     stocks: res.data.results,
        //     //     totalPage: totalPage
        //     // })
        // });
    }

    // Event for every change on input texts
    handleChange = event => {
        this.setState({
            id: event.target.value,
            ticker: event.target.value,
            open: event.target.value,
            close: event.target.value,
            volume: event.target.value,
            searchData: event.target.value
        });
    }

    //Adding Stocks
    addStock = event => {
        axios.post('http://127.0.0.1:8000/stocks/add', {
            ticker: this.tickerRef.current.value,
            open: this.openRef.current.value,
            close: this.closeRef.current.value,
            volume: this.volumeRef.current.value
        }).then(res => {
            return axios.get('http://127.0.0.1:8000/stocks/');
        })
            .then((res) => {
                var totalPage = (res.data.count) / (res.data.results.length)
                this.setState({
                    stocks: res.data.results,
                    totalPage: totalPage,
                    currentPage: 1,
                })

            })
        //reset text field after add
        event.target.reset()
    }

    //deleting by clicking on action
    deleteAction(idnumber, isSearching) {
        axios
            .delete(`http://127.0.0.1:8000/stocks/delete/${idnumber}/`)
            .then(res => {
                return axios.get('http://127.0.0.1:8000/stocks/');
            })
            .then((res) => {
                var totalPage = (res.data.count) / (res.data.results.length)
                this.setState({
                    stocks: res.data.results,
                    totalPage: totalPage,
                    currentPage: 1,
                })
            })
    }

    //Edit stocks
    editStocks(id, ticker, open, close, volume) {
        this.tickerRef.current.value = ticker
        this.openRef.current.value = open
        this.closeRef.current.value = close
        this.volumeRef.current.value = volume
        this.setState({
            editing: true,
            id: id
        })

    }

    //UpdateStock
    updateStock() {
        axios.put(`http://127.0.0.1:8000/stocks/edit/${this.state.id}/`, {
            ticker: this.tickerRef.current.value,
            open: this.openRef.current.value,
            close: this.closeRef.current.value,
            volume: this.volumeRef.current.value,
        })
            .then(res => {
                this.setState({ editing: false })
                this.clearRef();
                return axios.get('http://127.0.0.1:8000/stocks/');

            })
            .then((res) => {
                var totalPage = (res.data.count) / (res.data.results.length)
                this.setState({
                    stocks: res.data.results,
                    totalPage: totalPage,
                    currentPage: 1,
                })
            })
            .catch(function (error) {
                return
                // console.log(error);
            });
    }

    //Call this for clearing the input fields
    clearRef() {
        this.tickerRef.current.value = ''
        this.openRef.current.value = ''
        this.closeRef.current.value = ''
        this.volumeRef.current.value = ''
    }

    cancelUpdate() {
        this.setState({
            editing: false,
        })
        this.clearRef()
    }

    //Pagination
    displayPage = number => {
        axios.get(`http://127.0.0.1:8000/stocks/?page=${number}`).then(res => {
            this.setState({
                stocks: res.data.results,
                currentPage: number,
            })
        });
    }


    //Add stocks
    addForm() {
        return (
            <div>
                <h1>Add Stock</h1>
                <form onSubmit={this.addStock} id="addForm">
                <input className= "title" required type="text" placeholder="Ticker" ref={this.tickerRef} /> 	&nbsp;
                <input required type="number" placeholder="Open Price" ref={this.openRef} /> 	&nbsp;
                <input required type="number" placeholder="Close Price" ref={this.closeRef} /> &nbsp;
                <input required type="number" placeholder="24h Volume" ref={this.volumeRef} /> &nbsp;
                <button type="submit"> ADD </button>
                </form>
                <hr />

                {this.searchAllForm()}
                <hr />
                {/*Displaying all the stock list */}

                

                
            </div>
        );
    }

    //Update form
    editForm() {
        return (
            <div>
                <h1>Update Stock</h1>

                <form onSubmit={this.updateStock.bind(this)} id="addForm">
                    <input required type="text" placeholder="Ticker" ref={this.tickerRef} /> &nbsp;
                <input required type="number" placeholder="Open Price" ref={this.openRef} /> &nbsp;
                <input required type="number" placeholder="Close Price" ref={this.closeRef} /> &nbsp;
                <input required type="number" placeholder="24h Volume" ref={this.volumeRef} /> &nbsp;
                <button type="submit"> UPDATE </button>
                </form>
                <button type="submit" onClick={this.cancelUpdate.bind(this)}> CANCEL </button>
                <hr />

            </div>
        );
    }
    searchAll = event => {
        this.setState({
            searching: true
        })
        // this.searchingResult()
    }
    resetSearch() {
        this.setState(
            { searching: false }
        )
        axios.get('http://127.0.0.1:8000/stocks/').then(res => {
            var totalPage = (res.data.count) / (res.data.results.length)
            this.setState({
                stocks: res.data.results,
                totalPage: totalPage
            })
        });

    }
    searchAllForm() {
        return (
            <div>
                <form onSubmit={this.searchAll.bind(this)}>
                    <br />
                    <input required type="text" onChange={this.handleChange} placeholder="Search.." ref={this.searchAllRef} />
                    <button type="submit">Search</button>
                </form>
            </div>
        );
    }



    render() {

        // if (this.state.searching) {
        //     return this.searchingResult()
        // }


        if (this.state.editing) {
            return this.editForm()
        } else {
            return this.addForm()
        }

    }
}
export default Board;