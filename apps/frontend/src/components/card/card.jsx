import React from 'react';
import { withRouter } from "react-router-dom";
import axios from 'axios';
import initial from '../../providers/initial.jsx';
import { translate } from "react-translate";
// import StockList from './StockList';
// import SearchStock from './SearchStock';


class Card extends React.Component {

    constructor(props) {
        super(props);
        // console.log(props);
        this.state = {
            totalRecords: '',
        }
        this.handleClick = this.handleClick.bind(this);
    }

    componentDidMount() {
        // var bodyFormData = new FormData();
        // bodyFormData.append('userName', 'Fred');
        axios.get(initial.url + 'category/' + this.props.class +'/counter/', {
            params: { 'categoria': this.props.class }
        }).then((response) => {
            //handle success
            // console.log(response);
            this.setState({
                totalRecords: response.data.data
            })
        }).catch((response) => {
            //handle error
            console.log('error----', response);
        });
    }

    handleClick(currentElement) {
        this.props.history.push({
            pathname: '/table',
            // search: '?=' + this.props.class,
            state: {tematica:this.props.class, card:true}
        });
    }

    render() {
        let { t } = this.props;
        return (
            <div className={this.props.class_col}>
                <a onClick={this.handleClick.bind(this)} alt={t(this.props.title)} className="link-cat">
                    <div className={"card categoria text-center " + this.props.class}>
                        <div className="card-body body-services">
                            <h3>{t(this.props.title)}</h3>
                            <i className={this.props.icon}></i><span className="cifra">{this.state.totalRecords.length}</span>
                            <p className="card-text">
                                {t(this.props.title+"_desc")}
                            </p>
                        </div>
                    </div>
                </a>
            </div>
        )
    }
}

export default withRouter (translate('App')(Card))