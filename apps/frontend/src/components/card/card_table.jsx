import React from 'react';
import { withRouter } from "react-router-dom";
import axios from 'axios';
import initial from '../../providers/initial.jsx';
import { translate } from "react-translate";
// import StockList from './StockList';
// import SearchStock from './SearchStock';


class CardTable extends React.Component {

    constructor(props) {
        super(props);
        // console.log(props);

        this.state = {
            data_keys: [],
            data_values: {},
            type_card: null,
            data_headers: []
        }
        this.handleClick = this.handleClick.bind(this);
    }

    componentDidMount() {

    }

    componentWillReceiveProps(nextProps) {
        // console.log('----cambie--', nextProps);
        let tpc = null;
        if (nextProps.type_card == 'vess') {
            tpc = "vessels";
        } else if (nextProps.type_card == 'geo') {
            tpc = "geospatial";
        } else {
            tpc = nextProps.type_card;
        }
        this.setState({
            data_keys: nextProps.data_keys,
            data_values: nextProps.data_values,
            type_card: tpc,
            data_headers: nextProps.data_headers
        })

    }

    handleClick(currentElement) {

    }

    infoCreate(type_card) {
        // console.log('--------', type_card);
        let { t } = this.props;
        var lis_div = [];
        this.state.data_keys.map((value, i) => {
            if (value == 'nombre') {
                lis_div.push(
                    <div key={i} className="col-md-10" >
                        <h5 className="title-exp mt-0">{this.state.data_values[value]}
                            &nbsp;
                        <span style={{ "fontStyle": "italic", "fontSize": "small", "color": "gray" }}>
                                {this.state.data_values['pais']}
                            </span>
                            &nbsp;-&nbsp;
                        <span className={"badge badge-pill badge-" + this.state.type_card}>{t(this.state.type_card)}</span>
                        </h5>
                        {/* <h5 className="title-exp mt-0">
                            {this.state.data_values[value]} &nbsp;<span className={"badge badge-pill badge-" + this.state.type_card}>{this.state.type_card}</span>
                        </h5> */}
                    </div>
                )
            }
            else if (value == 'url' && this.state.data_values[value]) {
                lis_div.push(<div key={i} className="col-md-2 d-none d-md-block">
                    <span><a className="text-info" href={this.state.data_values[value]} target="_blank">{t('View more')}</a> </span>
                </div>)
                lis_div.push(<div key={i + 101} className="col-md-2 d-block d-sm-block d-md-block d-lg-none d-xl-none order-last">
                    <span><a className="text-info" href={this.state.data_values[value]} target="_blank">{t('View more')}</a> </span>
                </div>)
            }
            else if (value == 'json_ld' && this.state.data_values[value]) {
                lis_div.push( <script type="application/ld+json">
                    {JSON.stringify(JSON.parse(this.state.data_values[value]))}
                </script>)
            }
            else if (value != 'imagen' && value != 'pais' && value != 'url' && value != 'json_ld') {
                let desc = this.state.data_headers.find((header) => { return header.key == value });
                let title_ = desc[value];
                if (title_.charAt(0) == '_') {
                    title_ = title_.substr(1);
                }
                let val_ = this.state.data_values[value] ? this.state.data_values[value] : 'D/N'
                lis_div.push(
                    <div key={i} className="col-md-12" >
                        <span>
                            <span className="font-italic text-info">{t(title_)}:</span>  {val_}
                        </span>
                    </div>
                )
            }

        })
        return lis_div;
    }

    render() {
        return (
            <div className="it">
                <div className="media">
                    {this.state.data_values['imagen'] ?
                        <div className="divimage mr-3 img-thumbnail d-none d-sm-block" style={{ 'background': 'url(' + this.state.data_values["imagen"] + ')' }}></div>
                        : null}
                    <div className="media-body">
                        <div className="row">
                            {this.infoCreate(this.state.type_card)}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default translate('App')(CardTable)