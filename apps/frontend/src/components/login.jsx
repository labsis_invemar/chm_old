import React from "react";
import axios from 'axios';
import Parser from 'html-react-parser';
import { withRouter } from "react-router-dom";
import initial from '../providers/initial.jsx';
import { translate } from "react-translate";


class FormLogin extends React.Component {

  constructor(props){
    super(props);
    this.state = {
        username:"",
        password:""
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

   handleChange(e){
      if(e.target.type === "checkbox"){
        let value = "";
        if(e.target.checked){
            value = 1;
        }
        this.setState(
            { [e.target.name]: value }
        );
      }else{
        this.setState(
            { [e.target.name]: e.target.value }
        );
      }
  }

  update_checkbox(key){
    if(key in this.state && this.state[key]){
        let tag = document.getElementById(key);
        //console.log(tag);
        if(tag && tag.type === "checkbox"){
            tag.checked = true;
        }
      }
    }

  handleSubmit(btn) {
    if(this.handleValidation()){
      axios({
        method: 'post',
        url: initial.url + 'auth/login/',
        data: this.state,
        xsrfCookieName: initial.csrftoken,
        xsrfHeaderName: initial.xcsrftoken,
        processData: false,
        contentType: false
      }).then((response) => {
        //console.log(response);
        this.option_save(btn,response)
      }).catch((error) => {
            console.error(error);
            this.set_result_message(null)
      });
    }
  }

  handleValidation(){
    let fields = ['username','password'];
    let formIsValid = true;
    let datos = this.state
    fields.forEach(function(key) {
        if(!(key in datos) || datos[key]==="" || datos[key]==="-9"){
            console.log(key+ ' value is not valid...', datos[key]);
            formIsValid = false;
            document.getElementById(key).style.border = "1px solid red";
        }
   });
   return formIsValid;
  }

  option_save(btn, response){
      this.props.history.push({
        pathname: '/list_admin',
        state: {
            menu: "users"
        }
      });
    this.props.login(response.data.user, response.data.token);
    this.set_result_message(response);        
  }

  set_result_message(response=null, error_message="Invalid credentials"){
    if(response){
        this.setState({
            result:"The User <strong>"+this.state.name+"</strong> "+response.data.result,
            tipo_result: "alert-success"
        })
        if(response.data && response.data.data_scr && response.data.data_scr.id){
            this.setState({
                id: response.data.data_scr.id
            })
        }
    }else{
        this.setState({
            result:error_message,
            tipo_result: "alert-danger"
        })
    }
  }
  
   render() {
    let { t } = this.props;
    return (
        <div className="container alignment">
            <br/><br/><br/>
            <div className="card"> 
                <div className="card-header">{t("Login")}</div>
                <div className="card-body">
                    <form>
                        <div className="form-row">
                            <div className="col-md-4 mb-3" >
                                <label htmlFor="username">{t("Username")}</label>
                                <input className="form-control" type="text" name="username" id="username" onChange={this.handleChange} value={this.state.username} required/>
                            </div>
                            <div className="col-md-4 mb-3" >
                                <label htmlFor="name">{t("Password")}</label>
                                <input className="form-control" type="password" name="password" id="password" onChange={this.handleChange} value={this.state.password} required/>
                            </div>
                        </div>
                        {
                            this.state.result?
                            <div className={"alert "+ this.state.tipo_result+" a-form"}>
                            <button type="button" onClick={(e)=>{this.setState({result:undefined})}} className="close" >&times;</button>
                            {Parser(this.state.result)}</div>:null
                        }
                        <div className="card panel-button">
                            <div className="card-body">
                                <button onClick={(e)=>{this.handleSubmit()}} type="button" className="btn btn-primary margin-bottom-btn float-right">
                                   {t("Login")}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> 
    );
  }
}

export default withRouter (translate('App')(FormLogin));