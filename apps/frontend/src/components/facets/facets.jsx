import React from 'react';
import axios from 'axios';
// import Select from 'react-select';
import initial from '../../providers/initial.jsx';
import createFilterOptions from 'react-select-fast-filter-options';
// Make sure to import default styles.
// This only needs to be done once; probably during bootstrapping process.
import 'react-select-v1/dist/react-select.css'
import 'react-virtualized-select/styles.css'
// Then import the virtualized Select HOC
import VirtualizedSelect from 'react-virtualized-select'
import { translate } from "react-translate";


class Facets extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            selectedOption: null,
            categoria: '',
            facets_data: {},
            search: undefined,
        }

        this.handleChange = this.handleChange.bind(this)
    }


    componentDidMount() {

    }

    componentWillReceiveProps(nextProps) {
        // console.log('-----los props desde la tablet', nextProps);

        if ((this.state.categoria != nextProps.categoria) || (this.state.search != nextProps.search)) {
            this.setState({
                categoria: nextProps.categoria,
                facets_data: {},
                search: nextProps.search,
            }, () => {
                this.get_data_ajax({
                    'categoria': this.state.categoria,
                    'search': this.state.search
                });
            })
        }
    }

    get_data_ajax(data) {
        axios.get(initial.url + 'category/' + data.categoria + '/facets/', {
            params: data
        }).then((response) => {
            //handle success
            this.builder_satate(response);
        }).catch((response) => {
            console.log('error ----', response);
        })
    }

    builder_satate(response) {
        // console.log('repsuesta facets--->', response);

        this.setState({
            data: response.data.data
        }, () => {
            // console.log('->>>>>', this.state.data);
        })
    }

    handleChange(event, key) {
        // esto es para el envio de la informacion a la tabla para que busque        
        // console.log('soy el eventoo', event);
        // console.log('soy el  key', key);
        let facets_data = Object.assign({}, this.state.facets_data);    //creating copy of object

        if (event.length > 0) {
            facets_data[key] = event;
        } else {
            delete facets_data[key];
        }

        this.setState({ facets_data }, () => {
            this.props.action(this.state);
        });

        /*if (Object.keys(this.state.facets_data).length === 0) {
            this.setState({
                facets_data: {}
            }, () => {
                // this.props.action(this.state);
            })
        } else {
            // this.props.action(this.state);
        }*/

    }

    facets_Create() {
        var lis_td = [];
        const { facets_data, data } = this.state;
        let options;
        let filterOptions;
        let { t } = this.props;
        data.map((data, i) => {
            //console.log('data...', data);
            if (data.name.charAt(0) != '_' && data.name!='json_ld' && data.name!='Name') {
                options = data.options;
                filterOptions = createFilterOptions({ options });
                lis_td.push(
                    <div className="px-3" key={i + this.state.categoria}>
                        <span className="nameFilter">{t(data.name)}</span>
                        <VirtualizedSelect
                            multi
                            value={facets_data[data.key_field]}
                            onChange={(e) => this.handleChange(e, data.key_field)}
                            options={options}
                            filterOptions={filterOptions}
                            placeholder={t('Select')+'...'}
                        />
                    </div>
                )
            }                        
        })
        return lis_td
    }

    render() {
        // const { selectedOption } = this.state;
        return (
            <div id="block-views-terms-block-1" className="block block-views clearfix">
                <div className="inner tb-terminal">
                    {this.facets_Create()}
                </div>
            </div>
        )
    }

}

export default translate ('App')(Facets)