import React from "react";
//import PropTypes from "prop-types";
//import { withRouter } from "react-router-dom";
import axios from 'axios';
import initial from '../../providers/initial.jsx';
import CategoryField from "../input/select_field_scraping.jsx";
import tematicas from "../../../static/js/tematicas/tematicas.json.js";
import Workbook from 'react-excel-workbook';

export default class FormUploadF extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            show_loader: false,
            class_alert: '',
            show_alert: false,
            msg_alert: '',
            fields: {},
            errors: {},
            name_file: "Upload file",
            array_fail_data: [],
            obj_fail_headers: {}
        }

        this.sendData = this.sendData.bind(this);
        this.handleFileUpload = this.handleFileUpload.bind(this);
        this.handleChangeCategory = this.handleChangeCategory.bind(this);
        this.send_data_ajax = this.send_data_ajax.bind(this);
        this.handleChange = this.handleChange.bind(this);

    }


    handleFileUpload(event) {
        let target = event.target;
        if (target.files.length > 0) {
            // console.log('---------------------', target.files[0]);
            this.setState({ profilePic: target.files[0] });

            /*
            const files = Array.from(target.files);
            console.log('---------------------', target.files[0]);
            this._formData.append('file', 'sanitago')
            
            let file = target.files[0];
            let size = file.size / 1024 / 1024;
            let reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = (e) => {
                console.log('listo para guardar en un objeto formdata');
            }
            */

        }

    }

    handleChangeCategory(event) {
        // console.log('---------', event.target.value);
        this.setState({
            category: event.target.value
        })

    }

    send_data_ajax(data) {
        axios({
            method: 'post',
            url: initial.url + 'admins/uploadfile/',
            data: data,
            headers: {
                'Content-Type': 'application/json',
                Authorization: localStorage.getItem('token')
            },
            xsrfCookieName: initial.csrftoken,
            xsrfHeaderName: initial.xcsrftoken,
        }).then((response) => {
            //handle success            
            this.builder_satate(response);

        }).catch((error) => {
            //handle error
            console.log(error);

        });
    }

    builder_satate(response) {
        // console.log(' ---- data --', response);
        this.setState({
            show_loader: false,
            class_alert: response.data.class,
            show_alert: true,
            msg_alert: response.data.msg,
            obj_fail_headers: response.data.obj_fail_headers,
            array_fail_data: response.data.array_fail_data
        }, () => {
                this.close_alert(response.data.seg_alert);
        })
    }

    close_alert(seg_alert) {
        setTimeout(
            function () {
                this.setState({ show_alert: false });
            }.bind(this),
            seg_alert
        );
    }

    handleValidation() {
        return new Promise((resolve, reject) => {
            let fields = this.state.fields;
            let errors = {};
            let formIsValid = true;

            //Name
            if (!fields["category"]) {
                formIsValid = false;
                errors["category"] = "Cannot be empty";
            }

            if (!fields["file"]) {
                formIsValid = false;
                errors["file"] = "Cannot be empty";
            }

            this.setState({ errors: errors }, () => {
                // console.log('errores-------', this.state);
                // console.log('errores-------', formIsValid);
                resolve(formIsValid);
            });
        });


    }

    handleChange(e, field) {
        let fields = this.state.fields;
        if (field == 'file') {
            fields[field] = e.target.files[0];
            this.setState({ name_file: e.target.files[0].name })
        } else {
            fields[field] = e.target.value;
        }
        this.setState({ fields });
    }


    sendData() {
        this.handleValidation().then((val) => {
            if (val) {
                this.setState({
                    show_loader: true
                }, () => {
                    let formData = new FormData();
                    for (const key in this.state.fields) {
                        // console.log('key--------->', key);
                        formData.append(key, this.state.fields[key])
                    }
                    // formData.append('profile_pic', this.state.fields['file']);
                    // formData.append('type', this.state.fields['category']);
                    this.send_data_ajax(formData);
                })
            }

        })
    }

    options_create() {
        var list_options = [];
        list_options.push(<option key={""} value="">Select Category</option>)
        tematicas.arr_tematicas_up.map((tematica, i) => {
            if (tematica.show) list_options.push(<option key={i} value={tematica.class}>{tematica.title}</option>)
        })

        return list_options;
    }

    header_fail_create() {
        var list_heaf = [];
        for (const key in this.state.obj_fail_headers) {
            list_heaf.push(<th key={key} >
                <a style={{ 'color': '#ffffff' }} href="#">{key}</a>
            </th>)

        }
        return list_heaf;
    }

    table_faild_create() {

        const example = (

            <Workbook filename="data_invalid.xlsx" element={<button className="btn btn-outline-primary margin-bottom-btn"><i className="fa fa-download" aria-hidden="true"></i>Download invalid data</button>}>
                <Workbook.Sheet data={this.state.array_fail_data} name="Sheet A">
                    {this.table_faild_colums_create()}
                </Workbook.Sheet>
            </Workbook>

        )
        return example;
    }

    table_faild_colums_create() {
        var list_colum = []
        for (const key in this.state.obj_fail_headers) {
            list_colum.push(
                <Workbook.Column key={key} label={key} value={key} />
            )
        }
        return list_colum;
    }

    render() {

        return (
            <div className="container alignment">

                <div className="card">
                    <div className="card-header">Upload File</div>
                    <div className="card-body">
                        {this.state.show_alert ? <div className={"alert " + this.state.class_alert} role="alert">
                            {this.state.msg_alert}
                        </div> : null}
                        {this.state.show_loader ?
                            <div className="d-flex justify-content-center spinner-header">
                                <div className="spinner-grow text-primary" style={{ "width": '3rem', "height": '3rem' }} role="status">
                                    <span className="sr-only">Loading...</span>
                                </div>
                                <span>Please wait...</span>
                            </div> :
                            <div>
                                <form noValidate>
                                    <div className="form-row">
                                        <div className="col-md-4 mb-3" >
                                            <label htmlFor="files">File</label>
                                            <label style={{ "cursor": "pointer", "margin": "0px" }}
                                                className="form-control"
                                                htmlFor="files">
                                                {this.state.name_file}
                                            </label>
                                            <span style={{ color: "red" }}>{this.state.errors["file"]}</span>
                                            <input id="files" style={{ "visibility": "hidden", "height": "10px" }} className="form-control" type="file" name="file" onChange={(e) => { this.handleChange(e, "file") }} required />
                                        </div>
                                        <div className="col-md-4 mb-3">
                                            <label htmlFor="Category">Category</label>
                                            <select
                                                className="form-control"
                                                name="category"
                                                // value={this.state.category}
                                                onChange={(e) => { this.handleChange(e, "category") }}
                                                required
                                                value={this.state.fields["category"]}
                                            >
                                                {this.options_create()}
                                            </select>
                                            <span style={{ color: "red" }}>{this.state.errors["category"]}</span>
                                        </div>
                                    </div>
                                    <div className="form-row">
                                        <button type="button" className="btn btn-outline-primary margin-bottom-btn" onClick={this.sendData}> <i className="fa fa-plus"></i>Upload</button>
                                        {this.state.array_fail_data && this.state.array_fail_data.length > 0 ? this.table_faild_create() : null}
                                    </div>
                                </form>
                            </div>
                        }
                    </div>
                </div>
            </div>
        )
    }
}