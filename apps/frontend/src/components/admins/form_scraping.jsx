import React from "react";
//import PropTypes from "prop-types";
//import { withRouter } from "react-router-dom";
import axios from 'axios';
import initial from '../../providers/initial.jsx';
import CategoryField from "../input/select_field_scraping.jsx";
import Parser from 'html-react-parser';
import { withRouter } from "react-router-dom";
import tematicas from "../../../static/js/tematicas/tematicas.json.js";


class FormScraping extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      name: "",
      dominio: "",
      starturl: "",
      rootItem: "",
      classItem: "",
      category:"-9",
      pagination: "no",
      key_page: "",
      init_page:"",
      end_page:"",
      increment:"",
      connector:"",
      id:"",
      fields: [],
      categoryField:[{fieldlocal:"",fieldweb:"",typefield:"text",sizetext:"",rootitem:""}],
      tipo_result:undefined,
      result:undefined,
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeCategory = this.handleChangeCategory.bind(this);
    this.addCategory = this.addCategory.bind(this);
    
    this.removeCategory = this.removeCategory.bind(this);
    
  }

  
  componentDidMount(e){
    if(this.props.id){
      axios.get(initial.url + 'admins/get_scraping/', {
        params: {'id_scr':this.props.id},
        headers: {
            Authorization: localStorage.getItem('token')
        }
      }).then((response) => {
          //handle success
          this.builder_satate(response);
          // console.log("response-get-scraping:",response)
      }).catch((response) => {
          console.log('error ----', response);
      })
    }

    if(this.props.name){
      this.setState({
        result:"The Scraping <strong>"+this.props.name+"</strong> "+this.props.result, 
        tipo_result: "alert-success"
      })
    }
  }


  builder_satate(response){

    let categoryField = [];
    
    response.data.data_fields.map((field,i)=>{
      categoryField.push({
        fieldlocal:field.field_chm,
        fieldweb:field.field_web,
        rootitem:field.root_item,
        typefield:field.type_field,
        sizetext:field.size_text,
        })
    });
    // console.log("response.data.data_src: ",response.data.data_src)
    this.setState({
      name: response.data.data_src.name,
      dominio: response.data.data_src.dominio,
      starturl: response.data.data_src.starturl,
      rootItem: response.data.data_src.rootItem,
      classItem: response.data.data_src.classItem,
      pagination: response.data.data_src.pagination,
      key_page: response.data.data_src.page_key,
      init_page:response.data.data_src.page_init,
      end_page:response.data.data_src.page_end,
      increment:response.data.data_src.increment,
      connector:response.data.data_src.connector,
      category: response.data.data_src.category,
      id: response.data.data_src.id
    })
    
    this.setState({categoryField});

  }


  addCategory(e){
    this.setState((prevState) => ({
      categoryField: [...prevState.categoryField, {fieldlocal:"", fieldweb:"",typefield:"text",sizetext:"",rootitem:""}],
    }));
  }

  removeCategory(e){
    let categoryField = [...this.state.categoryField];
    categoryField.splice(e.target.dataset.id,1);
    this.setState({ categoryField });
 }
 

  handleChange(e){
    //console.log("e.target.name:"+e.target.name+"e.target.value:"+e.target.value)
   
    if(["fieldlocal", "fieldweb","typefield","sizetext","rootitem"].includes(e.target.name)){
      let categoryField = [...this.state.categoryField]
      categoryField[e.target.dataset.id][e.target.name] = e.target.value
      this.setState({ categoryField }, () => console.log(this.state.categoryField))
    } else {
      this.setState({ [e.target.name]: e.target.value });
    }
    
  }

  handleChangeCategory(e){
    if("-9"!=e.target.value){
      axios.get(initial.url + 'category_fields/', {
        params: {'categoria': e.target.value},
        headers: {
          Authorization: localStorage.getItem('token')
        }
      }).then((response) => {
          //handle success
          // console.log("fields-select: ",response);
          this.setState({
            fields: response.data.fields
          });
      }).catch((response) => {
          console.log('error ----', response);
      });
      /*axios({
          method: 'post',
          url: initial.url + 'category_fields/',
          data: { 'categoria': e.target.value},
          xsrfCookieName: initial.csrftoken,
          xsrfHeaderName: initial.xcsrftoken,
          processData: false,
          contentType: false,
      }).then((response) => {
          //handle success
          console.log("response.data.fields: ",response.data.fields)
          this.setState({
              fields: response.data.fields
          })
      }).catch((response) => {
          //handle error
          console.log('error ----', response);
      });*/
      }
  }

 

 


  handleSubmit(btn) {
    //e.preventDefault();
    if(this.handleValidation()){
      axios({
        method: 'post',
        url: initial.url + 'admins/scraping/',
        data: { 
          name: this.state.name,
          category: this.state.category,
          dominio: this.state.dominio,
          rootItem: this.state.rootItem,
          starturl: this.state.starturl,
          pagination: this.state.pagination,
          key_page: this.state.key_page,
          init_page:this.state.init_page,
          end_page:this.state.end_page,
          increment: this.state.increment,
          connector: this.state.connector,
          classItem: this.state.classItem,
          id: this.state.id,
          categoryField: this.state.categoryField
        },
        xsrfCookieName: initial.csrftoken,
        xsrfHeaderName: initial.xcsrftoken,
        processData: false,
        contentType: false,
        headers: {
            Authorization: localStorage.getItem('token')
        }
      }).then((response) => {
        //handle success
        //console.log(response);
        //console.log("create_scraping:",response.data.data_scr.id)
        this.option_save(btn,response)
        /*this.setState({
          result: response.data.result,
          id: response.data.data_scr.id
        })*/

      }).catch((error) => {
        //handle error
        console.log(error
        );
        
      });
    }else{
      this.setState({
        tipo_result:"alert-danger",
        result:"Required fields cannot be empty"
      })
    }

    
  }

  handleValidation(){
  
    let formIsValid = true;

    let datos = this.state
    Object.keys(datos).forEach(function(key) {
      if(["name","dominio","starturl","classItem","category"].includes(key)){
        if(datos[key]=="" || datos[key]=="-9"){
          // console.log("key: "+key+" datos[key]: "+ datos[key])
          formIsValid = false;
        }
      }
    });
    
    this.state.categoryField.map((val,i)=>{
     
      if(this.state.categoryField[i].fieldlocal=="-9" || this.state.categoryField[i].fieldweb=="" || this.state.categoryField[i].fieldlocal=="" || this.state.categoryField[i].sizetext==""){
        // console.log("this.state.categoryField",this.state.categoryField)
        formIsValid = false;
      }
    })
    
   return formIsValid;
  }
  

  option_save(btn,response){
    // console.log("btn: ",btn);
    if(btn =="3"){
      this.props.history.push({pathname: '/link'});
      this.props.history.push({
        pathname: '/scraping',
        state: {menu: "scraping",name:this.state.name,result:response.data.result}
      });
    }else if (btn =="1"){
      this.props.history.push({
        pathname: '/list_admin',
        state: {menu: "scraping"}
        });
    }else{
      this.setState({
        id: response.data.data_scr.id,
        result:"The Scraping <strong>"+this.state.name+"</strong> "+response.data.result,
        tipo_result: "alert-success"
      })
    }
  }


  render() {
    
    return (
      <div className="container alignment">
        
        <div className="card"> 
        <div className="card-header">Create Scraping</div>
        <div className="card-body">
        <form>
          <div className="form-row">
          <div className="col-md-4 mb-3" >
            <label htmlFor="name">Name</label>
            <input className="form-control" type="text" name="name" onChange={this.handleChange} value={this.state.name} required/>
          </div>
          <div className="col-md-4 mb-3">
            <label htmlFor="Category">Category</label>
            
            <select className="form-control" name="category" value={this.state.category} onChange={(e) => { this.handleChange(e); this.handleChangeCategory(e);}} >
              <option value="-9">Select category</option>
              {
                tematicas.arr_tematicas_up.map((tematica, i) => (
                  tematica? 
                  <option key={i} value={tematica.class}>{tematica.title}</option>:null
                ))
              }
            </select>
            
          </div>
          <div className="col-md-4 mb-3" >
            <label htmlFor="method">Dominio</label>
            <input className="form-control" type="text" name="dominio" onChange={this.handleChange} value={this.state.dominio} required/>
         
          </div>
          </div>

          <div className="form-row">
          <div className="col-md-4 mb-3">
          <label className="radio-inline"><span className="haspage" >this has pagination?</span>  </label>
          <label className="radio-inline haspage">
          <input type="radio"  name="pagination" value="yes" checked={this.state.pagination === 'yes'} onChange={this.handleChange} />Yes
          </label>
          <label className="radio-inline">
            <input type="radio"  name="pagination" value="no" checked={this.state.pagination === 'no'} onChange={this.handleChange} />No
          </label>
            </div>
          </div>
          
         
          {
            this.state.pagination =="yes"?
            <div className="form-row">
              <div className="col-md-2 mb-3">
                  <label htmlFor="end_page">Key page </label>
                  <input className="form-control" type="text" name="key_page" onChange={this.handleChange} value={this.state.key_page} required />
            </div>
              <div className="col-md-1 mb-3">
                <label htmlFor="ini_page">Init page </label>
                <input className="form-control" type="text" name="init_page" onChange={this.handleChange} value={this.state.init_page} required />
              </div>
              <div className="col-md-1 mb-3">
                  <label htmlFor="end_page">End page </label>
                  <input className="form-control" type="text" name="end_page" onChange={this.handleChange} value={this.state.end_page} required />
              </div>
              <div className="col-md-1 mb-3">
                  <label htmlFor="end_page">Increment</label>
                  <input className="form-control" type="text" name="increment" onChange={this.handleChange} value={this.state.increment} required />
              </div>
              <div className="col-md-1 mb-3">
                  <label htmlFor="end_page">Connector</label>
                  <input className="form-control" type="text" name="connector" onChange={this.handleChange} value={this.state.connector} required />
              </div>
              
              
            </div>
              :null
          }


          <div className="form-row">
          <div className="col-md-4 mb-3" >
            <label htmlFor="url">Start url</label>
            <input className="form-control" type="text" name="starturl" onChange={this.handleChange} value={this.state.starturl} required />
          </div>
          <div className="col-md-4 mb-3" >
            <label htmlFor="url">Root URL item</label>
            <input className="form-control" type="text" name="rootItem" onChange={this.handleChange} value={this.state.rootItem} required />
          </div>
          <div className="col-md-4 mb-3" >
            <label htmlFor="url">Class item Link</label>
            <input className="form-control" type="text" name="classItem" onChange={this.handleChange} value={this.state.classItem} required />
          </div>
          
        </div>

          <h5 className="subtitle-adm">Fields of the Categories</h5>
          <hr className="hr1" />
          <CategoryField categoryField={this.state.categoryField} fields={this.state.fields} category={this.state.category} change={this.handleChange} remove={this.removeCategory} />
          <div className="form-row">
          <button type="button" className="btn btn-outline-primary margin-bottom-btn" onClick={this.addCategory}> <i className="fa fa-plus"></i>Add Field Category</button>
          </div>
          

          {
            this.state.result?
            <div className={"alert "+ this.state.tipo_result+" a-form"}>
            <button type="button" onClick={(e)=>{this.setState({result:undefined})}} className="close" >&times;</button>
            {Parser(this.state.result)}</div>:null
          }
          
          <div className="card panel-button">
            <div className="card-body">
            <button onClick={(e)=>{this.handleSubmit(1)}} type="button" className="btn btn-primary margin-bottom-btn float-right">
              Save
            </button>
            <button onClick={(e)=>{this.handleSubmit(2)}} type="button" className="btn btn-primary margin-bottom-btn float-right">
              Save and continue editing
            </button>
            <button onClick={(e)=>{this.handleSubmit(3)}} type="button" className="btn btn-primary margin-bottom-btn float-right">
              Save and add another
            </button>
            
            
            </div>
          </div>
          

          
        </form>
        </div>
        </div>
        </div> 
        
      
    );
  }
}

export default withRouter(FormScraping)