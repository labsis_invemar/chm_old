import React from "react";
//import PropTypes from "prop-types";
//import { withRouter } from "react-router-dom";
import "babel-polyfill";
import axios from 'axios';
import initial from '../../providers/initial.jsx';
import { withRouter } from "react-router-dom";
import Parser from 'html-react-parser';
import tematicas from "../../../static/js/tematicas/tematicas.json.js";


class FormOai extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      name: "",
      category: "-9",
      metadata_prefix:"-9",
      domain:"",
      url_complement:"",
      data_set:"",
      limit:"0",
      id: "",
      update: "No",
      result: undefined,
      tipo_result: undefined,
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  componentDidMount(e) {

    if (this.props.id) {

      axios.get(initial.url + 'admins/get_oai/', {
        params: { 'id_oai': this.props.id },
        headers: {
          Authorization: localStorage.getItem('token')
        }
      }).then((response) => {
        //handle success
        //console.log("get_webservice: ",response)
        this.builder_satate(e, response);
      }).catch((response) => {
        console.log('error ----', response);
      })
    }
    if (this.props.name) {
      this.setState({
        result: "The OAI repository <strong>" + this.props.name + "</strong> " + this.props.result,
        tipo_result: "alert-success"
      })
    }



  }

  builder_satate(e, response) {
    
    this.setState({
      name: response.data.data_oai.name,
      category: response.data.data_oai.category,
      metadata_prefix: response.data.data_oai.metadata_prefix,
      domain: response.data.data_oai.domain,
      url_complement: response.data.data_oai.url_complement,
      data_set: response.data.data_oai.data_set,
      limit: response.data.data_oai.limit,
      update: response.data.data_oai.update,
      id: response.data.data_oai.id
    })

  }


  handleChange(e) {
      this.setState({ [e.target.name]: e.target.value });
  }

  handleValidation() {

    let formIsValid = true;

    let datos = this.state

    Object.keys(datos).forEach(function (key) {
      if (["name", "metadata_prefix","domain","url_complement","category"].includes(key)) {
        if (datos[key] == "" || datos[key] == "-9") {
          formIsValid = false;
        }
      }
    });

    return formIsValid;
  }




  first_obj(obj) {
    for (var a in obj) return a;
  }


  handleSubmit(btn) {
    //e.preventDefault();
    if (this.handleValidation()) {
      axios({
        method: 'post',
        url: initial.url + 'admins/oai/',
        data: {
          name: this.state.name,
          category: this.state.category,
          metadata_prefix: this.state.metadata_prefix,
          domain: this.state.domain,
          url_complement: this.state.url_complement,
          data_set: this.state.data_set,
          limit: this.state.limit,
          update: this.state.update,
          id: this.state.id,
        },
        xsrfCookieName: initial.csrftoken,
        xsrfHeaderName: initial.xcsrftoken,
        processData: false,
        contentType: false,
        headers: {
          Authorization: localStorage.getItem('token')
        }
      }).then((response) => {
        //handle success
        //console.log(response);
        this.option_save(btn, response);
        /*this.setState({
          result: response.data.result
        })*/
      }).catch((error) => {
        //handle error
        console.log(error);

      });
    } else {

      this.setState({
        tipo_result: "alert-danger",
        result: "Required fields cannot be empty"
      })
    }
  }

  option_save(btn, response) {
    if (btn == "3") {
      this.props.history.push({ pathname: '/link' });
      this.props.history.push({
        pathname: '/oai',
        state: { menu: "oai", name: this.state.name, result: response.data.result }
      });
    } else if (btn == "1") {
      this.props.history.push({
        pathname: '/list_admin',
        state: { menu: "oai" }
      });
    } else {
      this.setState({
        id: response.data.data_oai.id,
        result: "The OAI Repository <strong>" + this.state.name + "</strong> " + response.data.result,
        tipo_result: "alert-success"
      })
    }
  }


  render() {

    return (
      <div className="container alignment">

        <div className="card">
          <div className="card-header">Create OAI Repository for Harvest</div>
          <div className="card-body">
            <form>

              <div className="form-row">
                <div className="col-md-4 mb-3" >
                  <label htmlFor="name">Name</label>
                  <input className="form-control" type="text" name="name" onChange={this.handleChange} value={this.state.name} required />
                </div>
                <div className="col-md-4 mb-3">
                  <label htmlFor="category">Category</label>

                  <select className="form-control" name="category" value={this.state.category} onChange={(e) => { this.handleChange(e)}} >
                    <option value="-9">Select Category</option>
                    {/* {
                tematicas.arr_tematicas_up.map((tematica, i) => (
                  tematica? 
                  <option key={i} value={tematica.class}>{tematica.title}</option>:null
                ))
              } */}
                    <option value="documents">Documents</option>
                  </select>

                </div>
                <div className="col-md-4 mb-3" >
                  <label htmlFor="metadata_prefix">Metadata Prefix</label>
                  <select name="metadata_prefix" className="form-control" onChange={this.handleChange} value={this.state.metadata_prefix}>
                    <option value="-9">Select Category</option>
                    <option value="oai_dc">oai_dc</option>
                    {/* <option value="post">POST</option> */}
                  </select >
                </div>
              </div>

              <div className="form-row">
                <div className="col-md-6 mb-3" >
                  <label htmlFor="domain">Domain</label>
                  <input className="form-control" type="text" name="domain" onChange={this.handleChange} value={this.state.domain} required />
                </div>
                <div className="col-md-6 mb-3" >
                  <label htmlFor="url_complement">URL Complement</label>
                  <input className="form-control" type="text" name="url_complement" onChange={this.handleChange} value={this.state.url_complement} required />
                </div>
              </div>

              <div className="form-row">
                <div className="col-md-6 mb-3" >
                  <label htmlFor="data_set">Dataset</label>
                  <input className="form-control" type="text" name="data_set" onChange={this.handleChange} value={this.state.data_set} required />
                </div>
                <div className="col-md-6 mb-3" >
                  <label htmlFor="limit">Limit</label>
                  <input className="form-control" type="text" name="limit" onChange={this.handleChange} value={this.state.limit} required />
                </div>
              </div>
              <div className="form-row">
                <div className="col-md-6 mb-3">
                  <label className="radio-inline"><span className="haspage" >Force Update</span>  </label>
                  <label className="radio-inline haspage">
                    <input type="radio" name="update" value={"Yes"} checked={this.state.update=="Yes"} onChange={this.handleChange}/> Yes
                  </label>
                  <label className="radio-inline">
                    <input type="radio" name="update" value={"No"} checked={this.state.update=="No"} onChange={this.handleChange}/> No
                  </label>
                </div>

                <div className="col-md-6 mb-3">
                  <label className="radio-inline"><span className="haspage" >Shared</span>  </label>
                  <label className="radio-inline haspage">
                    <input type="radio" name="shared" value={"Yes"} checked={this.state.shared=="Yes"} onChange={this.handleChange}/> Yes
                  </label>
                  <label className="radio-inline">
                    <input type="radio" name="shared" value={"No"} checked={this.state.shared=="No"} onChange={this.handleChange}/> No
                  </label>
                </div>
              </div>

              <div className="card panel-button">
                <div className="card-body">
                  <button onClick={(e) => { console.log("save"),this.handleSubmit(1) }} type="button" className="btn btn-primary margin-bottom-btn float-right">
                    Save
                  </button>
                  <button onClick={(e) => { this.handleSubmit(2) }} type="button" className="btn btn-primary margin-bottom-btn float-right">
                    Save and continue editing
                  </button>
                  <button onClick={(e) => { this.handleSubmit(3) }} type="button" className="btn btn-primary margin-bottom-btn float-right">
                    Save and add another
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>


    );
  }
}

export default withRouter(FormOai)