import React from "react";
import axios from 'axios';
import Parser from 'html-react-parser';
import { withRouter } from "react-router-dom";
import initial from '../../providers/initial.jsx';
import CategoryFieldApiReader from "../input/select_field_apireader.jsx"
import tematicas from "../../../static/js/tematicas/tematicas.json.js";

class FormApireader extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            dominio: "",
            item: "",
            category:"-9",
            id:"",
            fields: [],
            categoryField:[{fieldlocal:"",item:"",typefield:"text",sizetext:""}],
            tipo_result:undefined,
            result:undefined,
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeCategory = this.handleChangeCategory.bind(this);
        this.addCategory = this.addCategory.bind(this);
        
        this.removeCategory = this.removeCategory.bind(this);
    }

    componentDidMount(e){
        if(this.props.id){
            axios.get(initial.url + 'admins/get_apireader/', {
            params: {'id_rdr':this.props.id},
            headers: {
            Authorization: localStorage.getItem('token')
        }
        }).then((response) => {
          //handle success
            this.builder_satate(response);
          // console.log("response-get-scraping:",response)
        }).catch((response) => {
            console.log('error ----', response);
        })
    }

    if(this.props.name){
        this.setState({
        result:"The Api Reader <strong>"+this.props.name+"</strong> "+this.props.result, 
        tipo_result: "alert-success"
        })
    }
    
    }

    builder_satate(response){
        let categoryField=[];
        response.data.data_fields.map((field,i)=>{
            categoryField.push({
                fieldlocal:field.field_chm,
                item:field.item,
                typefield: field.type_field,
                sizetext:field.size_text
            })
        });

        this.setState({
            name: response.data.data_src.name,
            dominio: response.data.data_src.dominio,
            item: response.data.data_src.item,
            category: response.data.data_src.category,
            id: response.data.data_src.id
        })

        this.setState({CategoryFieldApiReader})

    }

    addCategory(e){
        this.setState((prevState) => ({
        categoryField: [...prevState.categoryField, {fieldlocal:"", item:"",typefield:"text",sizetext:""}],
        }));
    }

    removeCategory(e){
    let categoryField = [...this.state.categoryField];
    categoryField.splice(e.target.dataset.id,1);
    this.setState({ categoryField });
    }

    handleChange(e){
    //console.log("e.target.name:"+e.target.name+"e.target.value:"+e.target.value)
        if(["fieldlocal", "fieldweb","typefield","sizetext","item"].includes(e.target.name)){
            let categoryField = [...this.state.categoryField]
            categoryField[e.target.dataset.id][e.target.name] = e.target.value
            this.setState({ categoryField }, () => console.log(this.state.categoryField))
        }
        else {
            this.setState({ [e.target.name]: e.target.value });
        }
    }

    handleChangeCategory(e){
        if("-9"!=e.target.value){
        axios.get(initial.url + 'category_fields/', {
            params: {'categoria': e.target.value},
            headers: {
            Authorization: localStorage.getItem('token')
            }
        }).then((response) => {
            //handle success
            // console.log("fields-select: ",response);
            this.setState({
                fields: response.data.fields
            });
        }).catch((response) => {
            console.log('error ----', response);
        });
    }}

    handleSubmit(btn){


        const dataPrueba = {
                    name: this.state.name,
                    category: this.state.category,
                    dominio: this.state.dominio,
                    item: this.state.item,
                    url: this.state.url,
                    id: this.state.id,
                    categoryField: this.state.categoryField

                }

                console.log(dataPrueba);

        // if (this.handleValidation()){
            axios({
                method:'post',
                url: initial.url + 'admins/apireader/',
                data:{
                    name: this.state.name,
                    category: this.state.category,
                    dominio: this.state.dominio,
                    item: this.state.item,
                    url: this.state.url,
                    id: this.state.id,
                    categoryField: this.state.categoryField

                },
                xsrfCookieName: initial.csrftoken,
                xsrfHeaderName: initial.xcsrftoken,
                processData: false,
                contentType: false,
                headers: {
                    Authorization: localStorage.getItem('token')
                }
            }).then((response) => {
                this.option_save(btn, response)
            }).catch((error) => {
                console.log('error ----', error);
            });
        // }
        // else{
        //     this.setState({
        //         tipo_result: "alert-danger",
        //         result: "Required fields cannot be empty"
        //     })
        // }
    }


    handleValidation(){
    
        let formIsValid = true;

        let datos = this.state
        Object.keys(datos).forEach(function(key) {
        if(["name","dominio","url","item","category"].includes(key)){
            if(datos[key]=="" || datos[key]=="-9"){
            // console.log("key: "+key+" datos[key]: "+ datos[key])
            formIsValid = false;
            }
        }
        });
        
        this.state.categoryField.map((val,i)=>{
        
        if(this.state.categoryField[i].fieldlocal=="-9" || this.state.categoryField[i].item=="" || this.state.categoryField[i].fieldlocal=="" || this.state.categoryField[i].sizetext==""){
            // console.log("this.state.categoryField",this.state.categoryField)
            formIsValid = false;
        }
        })
        
    return formIsValid;
    }


    option_save(btn,response){
   // console.log("btn: ",btn);
    if(btn =="3"){
        this.props.history.push({pathname: '/link'});
        this.props.history.push({
            pathname: '/apireader',
            state: {menu: "apireader",
            name:this.state.name
        }
        });
    }else if (btn =="1"){
        this.props.history.push({
        pathname: '/list_admin',
        state: {menu: "apireader"}
        });
    }else{
        this.setState({
        id: response.data.data_src.id,
        result:"The Api Reader <strong>"+this.state.name+"</strong> "+response.data.result,
        tipo_result: "alert-success"
        })
    }
    }

    set_result_message(response=null, error_message="Error has ocurred"){
        if(response){
            this.setState({
                result:"The User <strong>"+this.state.name+"</strong> "+response.data.result,
                tipo_result: "alert-success"
            })
            if(response.data && response.data.data_src && response.data.data_src.id){
                this.setState({
                    id: response.data.data_src.id
                })
            }
        }else{
            this.setState({
                result:error_message,
                tipo_result: "alert-danger"
            })
        }
    }
    render() {
        return (
            <div className="container alignment">
                <div className="card">
                    <div className="card-header">Create Api Reader</div>
                    <div className="card-body">
                        {/* <h3>HOLAAAAAAAAAAAAAAAAAAAA</h3> */}
                        <form>
                            <div className="form-row">
                                <div className="col-md-4 mb-3" >
                                    <label htmlFor="name">Name</label>
                                    <input className="form-control" type="text" name="name" onChange={this.handleChange} value={this.state.name} required/>
                                </div>
                                <div className="col-md-4 mb-3">
                                    <label htmlFor="Category">Category</label>
                                    
                                    <select className="form-control" name="category" value={this.state.category} onChange={(e) => { this.handleChange(e); this.handleChangeCategory(e);}} >
                                    <option value="-9">Select category</option>
                                    {
                                        tematicas.arr_tematicas_up.map((tematica, i) => (
                                        tematica? 
                                        <option key={i} value={tematica.class}>{tematica.title}</option>:null
                                        ))
                                    }
                                    </select>
                                </div>
                                <div className="col-md-4 mb-3" >
                                <label htmlFor="method">Dominio</label>
                                <input className="form-control" type="text" name="dominio" onChange={this.handleChange} value={this.state.dominio} required/>
                                </div>
                            </div>
                            <h5 className="subtitle-adm">Fields of the Categories</h5>
                            {/* <div className="form-row"> */}
                            <hr className="hr1" />
                            <CategoryFieldApiReader categoryField={this.state.categoryField} 
                            fields={this.state.fields} category={this.state.category} 
                            change={this.handleChange} remove={this.removeCategory} />
                            {/* </div> */}

                            <div className="form-row">
                                <button type="button" className="btn btn-outline-primary margin-bottom-btn" onClick={this.addCategory}> <i className="fa fa-plus"></i>Add Field Category</button>
                            </div>
                            <div className="card panel-button">
                                <div className="card-body">
                                    <button onClick={(e)=>{this.handleSubmit(1)}} type="button" className="btn btn-primary margin-bottom-btn float-right">
                                    Save
                                    </button>
                                    <button onClick={(e)=>{this.handleSubmit(2)}} type="button" className="btn btn-primary margin-bottom-btn float-right">
                                    Save and continue editing
                                    </button>
                                    <button onClick={(e)=>{this.handleSubmit(3)}} type="button" className="btn btn-primary margin-bottom-btn float-right">
                                    Save and add another
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }

}
export default withRouter(FormApireader)