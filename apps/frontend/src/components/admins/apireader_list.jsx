import React from 'react'
import axios from 'axios'
import initial from '../../providers/initial.jsx'
import ModalConfirm from '../input/modal_confirm.jsx'
//import Facets from '../facets /facets.jsx';
import { withRouter } from 'react-router-dom'
import Workbook from 'react-excel-workbook'
class ApireaderList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      headerFields: [],
      data: [],
      table_keys_value: [],
      pagination_num: 0,
      menu: undefined,
      page_current: 1,
      search: undefined,
      show_loader: true,
      show_loader_modal: false,
      id_element: 0,
      name_element: '',
      result_modal: undefined,
      error: undefined,
      text_modal: '',
    }
    this.handleClick = this.handleClick.bind(this)
    this.handleForm = this.handleForm.bind(this)
    this.handleRefreshWeb = this.handleRefreshWeb.bind(this)
    this.confirmHarvesting = this.confirmHarvesting.bind(this)
    this.handleDeleteSource = this.handleDeleteSource.bind(this)
    this.confirmDelete = this.confirmDelete.bind(this)
    this.actionOK = this.actionOK.bind(this)
  }

  componentDidMount() {
    this.getTray()
  }

  getTray() {
    axios
      .get(initial.url + 'admins/get_apireader/', {
        headers: {
          Authorization: localStorage.getItem('token'),
        },
      })
      .then((response) => {
        //handle success
        console.log('Holaaaaaaa', response.data.data_rdr)
        this.setState({ data: response.data.data_rdr })
        //this.builder_satate(response);
      })
      .catch((response) => {
        console.log('status...', response)
        console.log('error ----', response)
      })
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.menu != this.props.menu) {
      this.get_data_ajax({
        menu: nextProps.menu,
        search: nextProps.search,
        page: nextProps.page_current,
      })
    }
  }

  handleClick(e) {
    e.preventDefault()
    let page = e.target.attributes.getNamedItem('data-value').value
    // this.props.history.push({
    //     pathname: '/table',
    //     search: this.state.search && this.state.search != 'undefined' ?
    //         '?=' + this.state.categoria + '?=' + this.state.search + '?=' + page :
    //         '?=' + this.state.categoria + '?=' + '?=' + page,
    //     state: this.state.categoria
    // });
    this.get_data_ajax({
      menu: this.state.menu,
      page: page,
      search: this.state.search,
      is_pagination: true,
    })
  }

  get_data_ajax(data) {
    axios
      .get(initial.url + 'admins/apireader_list/', {
        params: data,
        headers: {
          Authorization: localStorage.getItem('token'),
        },
      })
      .then((response) => {
        //handle success
        this.builder_satate(response)
      })
      .catch((response) => {
        console.log('status...', response)
        console.log('error ----', response)
      })
  }

  builder_satate(response) {
    // console.log(' ---- data --', response.data);

    let table_keys_value_ = []
    response.data.fields.map((value) => {
      if (value[value.key]) {
        table_keys_value_.push(value.key)
      }
    })

    this.setState(
      {
        headerFields: response.data.fields,
        data: response.data.data,
        table_keys_value: table_keys_value_,
        pagination_num: response.data.pagination_num,
        menu: response.data.menu,
        page_current: response.data.page_current,
        search: response.data.search,
        show_loader: false,
      },
      () => {
        //this.props.action(this.state);
      }
    )
  }

  handleForm(e, element_id) {
    e.preventDefault()
    this.props.history.push({
      pathname: '/apireader',
      search: element_id,
      state: { menu: this.state.menu },
    })
  }

  handleRefreshWeb(e, id_element) {
    e.preventDefault()
    var name = this.state.name_element
    this.setState({
      show_loader_modal: true,
      text_modal: 'Api Reader from <strong>' + name + '</strong> in process...',
    })
    axios({
      method: 'post',
      url: initial.url + 'admins/refreshWebService/',
      data: {
        id: id_element,
        menu: this.state.menu,
      },
      xsrfCookieName: initial.csrftoken,
      xsrfHeaderName: initial.xcsrftoken,
      processData: false,
      contentType: false,
      headers: {
        Authorization: localStorage.getItem('token'),
      },
    })
      .then((response) => {
        //   console.log(response);
        this.setState({
          result_modal: response.data.result,
          show_loader_modal: false,
        })
        this.actionOK()
        //this.option_save(btn,response);
      })
      .catch((error) => {
        //handle error
        console.log(error.response)
        this.setState({
          error: error.response.data.text,
          show_loader_modal: false,
        })
        this.actionOK()
      })
  }

  handleDeleteSource(e, id_element) {
    e.preventDefault()
    var name = this.state.name_element
    this.setState({
      show_loader_modal: true,
      text_modal: 'Deleting the source <strong>' + name + '</strong> ...',
    })
    axios({
      method: 'delete',
      url: initial.url + 'admins/deleteSource/',
      data: {
        id: id_element,
        menu: this.state.menu,
      },
      xsrfCookieName: initial.csrftoken,
      xsrfHeaderName: initial.xcsrftoken,
      processData: false,
      contentType: false,
      headers: {
        Authorization: localStorage.getItem('token'),
      },
    })
      .then((response) => {
        //   console.log(response);
        this.setState({
          result_modal: response.data.result,
          show_loader_modal: false,
        })
        this.actionOK()
        //this.option_save(btn,response);
      })
      .catch((error) => {
        //handle error
        console.log(error)
      })
  }

  actionOK(e) {
    this.get_data_ajax({
      menu: this.state.menu,
      page: this.state.page_current,
      search: this.state.search,
      is_pagination: true,
    })
  }

  confirmHarvesting(e, id_element, name_element) {
    // console.log("entro en confirm1"+name_element)
    this.setState({
      result_modal: undefined,
      error: undefined,
      id_element: parseInt(id_element),
      text_modal: 'Confirm process api reader from <strong>' + name_element + '</strong>',
      name_element: name_element,
      show_loader_modal: false,
    })
    // console.log("entro en confirm"+this.state.name_element)
  }
  confirmDelete(e, id_element, name_element) {
    this.setState({
      result_modal: undefined,
      error: undefined,
      id_element: id_element,
      text_modal: 'You are sure you want to delete the source <strong>' + name_element + '</strong>',
      name_element: name_element,
      show_loader_modal: false,
    })
  }

  paginationCreate(page_current) {
    var i = 0
    let cantidad_mostrar = 5

    if (this.state.pagination_num <= 5) {
      cantidad_mostrar = this.state.pagination_num
    } else if (this.state.data.length == 0) {
      cantidad_mostrar = 0
    } else if (page_current == 1) {
      i = parseInt(0)
    } else if (page_current == 2) {
      i = parseInt(page_current) - 2
      cantidad_mostrar = parseInt(page_current) + 2
    } else if (page_current < this.state.pagination_num) {
      i = parseInt(page_current) - 3
      cantidad_mostrar = parseInt(page_current) + 2
      if (cantidad_mostrar > this.state.pagination_num) {
        cantidad_mostrar = this.state.pagination_num
      }
    } else if (page_current == this.state.pagination_num) {
      i = this.state.pagination_num - 4
      cantidad_mostrar = this.state.pagination_num
    }

    var lis = []

    for (i; i < cantidad_mostrar; i++) {
      lis.push(
        <li key={i} className={'page-item ' + (this.state.page_current == i + 1 ? 'active' : ' ')}>
          <a className='page-link' href='#' data-value={i + 1} onClick={this.handleClick}>
            {i + 1}
          </a>
        </li>
      )
    }
    return lis
  }

  tableCreate(data_values) {
    var lis_td = []
    var arr_datadwl = []
    var obj_headdwl = {}

    if (data_values['datadwl'] && data_values['headdwl']) {
      arr_datadwl = eval('(' + data_values['datadwl'] + ')')
      obj_headdwl = eval('(' + data_values['headdwl'] + ')')
    }

    this.state.table_keys_value.map((value, i) => {
      if (value == 'datadwl') {
        lis_td.push(<td key={i}>{this.table_faild_create(arr_datadwl, obj_headdwl, data_values)}</td>)
      } else if (value != 'headdwl') {
        lis_td.push(<td key={i}>{data_values[value]}</td>)
      }
    })

    return lis_td
  }

  table_faild_create(data, header, data_values) {
    const example = (
      <div>
        <Workbook
          filename='data_invalid.xlsx'
          element={
            <button className='btn btn-outline-primary margin-bottom-btn' style={{ marginRight: '10px' }}>
              <i className='fa fa-download' aria-hidden='true'></i> Download invalid data
            </button>
          }
        >
          <Workbook.Sheet data={data} name='Sheet A'>
            {this.table_faild_colums_create(header)}
          </Workbook.Sheet>
        </Workbook>

        <a href='#' data-toggle='modal' data-target='#deleteSourceModal' onClick={(e) => this.confirmDelete(e, data_values['id'], data_values['name'])}>
          <i className='fa fa-trash-o'></i>
        </a>
      </div>
    )
    return example
  }

  table_faild_colums_create(header) {
    var list_colum = []
    for (const key in header) {
      list_colum.push(<Workbook.Column key={key} label={key} value={key} />)
    }
    return list_colum
  }

  handleUpdate(e, id) {
    console.log('<>=>=>=>=>=>', id)
    e.preventDefault()
    var name = 'objeto name'
    this.setState({
      show_loader_modal: true,
      text_modal: 'updating the source <strong>' + name + '</strong> ...',
    })
    axios({
      method: 'POST',
      url: initial.url + 'admins/updaterdr/',
      data: {
        id: id,
      },
      xsrfCookieName: initial.csrftoken,
      xsrfHeaderName: initial.xcsrftoken,
      processData: false,
      contentType: false,
      headers: {
        Authorization: localStorage.getItem('token'),
      },
    })
      .then((response) => {
        // console.log(JSON.stringify(response.data.data,null,2))
        this.setState({
          result_modal: response.data.result,
          show_loader_modal: false,
        })
        //   this.actionOK()
        //this.option_save(btn,response);
      })
      .catch((error) => {
        //handle error
        console.log(error)
      })
  }

  handleDelete(e, id) {
    e.preventDefault()
    var name = 'objeto name'
    this.setState({
      show_loader_modal: true,
      text_modal: 'Deleting <strong>' + name + '</strong> ...',
    })
    axios({
      method: 'delete',
      url: initial.url + 'admins/deleterdr/',
      data: {
        id: id,
      },
      xsrfCookieName: initial.csrftoken,
      xsrfHeaderName: initial.xcsrftoken,
      processData: false,
      contentType: false,
      headers: {
        Authorization: localStorage.getItem('token'),
      },
    })
      .then((response) => {
        console.log(response.data)
        this.getTray()
        //   this.setState({
        //     result_modal: response.data.result,
        //     show_loader_modal: false
        //   })
        //   this.actionOK()
        //this.option_save(btn,response);
      })
      .catch((error) => {
        //handle error
        console.log(error)
      })
  }

  render() {
    return (
      <section className='resultados'>
        <div className='container'>
          {/* <Facets {...this.state} /> */}
          <div className='mt-4 table-container table-responsive bordered'>
            <div className='table-container'>
              {/* {this.state.show_loader ? <div className="d-flex justify-content-center">
                                <div className="spinner-grow text-primary" style={{ "width": '3rem', "height": '3rem' }} role="status">

                                    <span className="sr-only">Loading...</span>
                                </div>
                            </div> : null}
                                {
                                    this.state.result?
                                    <div className={"alert "+ this.state.tipo_result+" a-form"}>
                                    <button type="button" onClick={(e)=>{this.setState({result:undefined})}} className="close" >&times;</button>
                                    {Parser(this.state.result)}</div>:null
                                } */}
              <button type='button' style={{ float: 'right' }} className='btn btn-success margin-bottom-btn' onClick={(e) => this.handleForm(e, '')}>
                <i className='fa fa-plus'></i>

                <span>Add new Web Api Reader</span>
              </button>
              <table className='table table-hover'>
                <thead>
                  <tr className='bg-primary'>
                    {/* {
                                            this.state.headerFields.map((field, i) => (
                                                field[field.key] && field.key != 'datadwl' && field.key != 'headdwl'? <th key={i} >
                                                    <a style={{ 'color': '#ffffff' }} href="#">{field[field.key]}</a>
                                                </th> : null
                                            ))
                                        } */}
                    <th>
                      <a style={{ color: '#ffffff' }} href='#'>
                        Name
                      </a>
                    </th>
                    <th>
                      <a style={{ color: '#ffffff' }} href='#'>
                        End Point
                      </a>
                    </th>
                    <th>
                      <a style={{ color: '#ffffff' }} href='#'>
                        Category
                      </a>
                    </th>
                    <th>
                      <a style={{ color: '#ffffff' }} href='#'>
                        Action
                      </a>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.data.map((items, i) => (
                    <tr key={i}>
                      <td>{items.name}</td>
                      <td>{items.dominio}</td>
                      <td>{items.category}</td>
                      <td>
                        <a href='#' data-toggle='modal' data-target='#deleteSourceModal' onClick={(e) => this.handleDelete(e, items.id)}>
                          <i className='fa fa-trash-o'></i>
                        </a>
                        <a data-toggle='modal' data-target='#harvestingModal' href='#' onClick={(e) => this.handleUpdate(e, items.id)}>
                          <i className='fa fa-refresh'></i>
                        </a>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
              <nav aria-label='Page navigation example'>
                <ul className='pagination justify-content-center'>
                  {this.state.page_current > 3 ? (
                    <li className='page-item'>
                      <a className='page-link' href='#' data-value='1' onClick={this.handleClick}>
                        ❮❮
                      </a>
                    </li>
                  ) : null}
                  {this.state.page_current > 3 ? (
                    <li className='page-item'>
                      <a className='page-link' href='#' data-value={parseInt(this.state.page_current) - 1} onClick={this.handleClick}>
                        ❮
                      </a>
                    </li>
                  ) : null}
                  {this.paginationCreate(this.state.page_current)}
                  {this.state.page_current < parseInt(this.state.pagination_num) - 2 ? (
                    <li className='page-item'>
                      <a className='page-link' href='#' data-value={parseInt(this.state.page_current) + 1} onClick={this.handleClick}>
                        ❯
                      </a>
                    </li>
                  ) : null}
                  {this.state.page_current < parseInt(this.state.pagination_num) - 2 ? (
                    <li className='page-item'>
                      <a className='page-link' href='#' data-value={this.state.pagination_num} onClick={this.handleClick}>
                        ❯❯
                      </a>
                    </li>
                  ) : null}
                </ul>
              </nav>

              <ModalConfirm
                idModal='harvestingModal'
                title='Harvesting'
                id_element={this.state.id}
                result={this.state.result_modal}
                text={this.state.text_modal}
                show_loader={this.state.show_loader_modal}
                action={this.handleUpdate}
                // actionOk={this.actionOK}
                error={this.state.error}
              />

              <ModalConfirm
                idModal='deleteSourceModal'
                title='Delete Source'
                id_element={this.state.id}
                result={this.state.result_modal}
                text={this.state.text_modal}
                show_loader={this.state.show_loader_modal}
                action={this.handleDelete}
                // actionOk={this.actionOK}
                error={this.state.error}
              />
            </div>
          </div>
        </div>
      </section>
    )
  }
}
export default withRouter(ApireaderList)
