import React from "react";
import axios from 'axios';
import Parser from 'html-react-parser';
import { withRouter } from "react-router-dom";
import initial from '../../providers/initial.jsx';


class FormUSers extends React.Component {

  constructor(props){
    super(props);
    this.state = {
        id:null,
        username:"",
        password:"",
        email:"",
        first_name:"", 
        last_name:"", 
        is_superuser:"", 
        is_active:""
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount(e){
    if(this.props.id){
      axios.get(initial.url + 'admins/get_user/', {
        params: {'id_scr':this.props.id},
        headers: {
            Authorization: localStorage.getItem('token')
        }
      }).then((response) => {
          this.builder_state(response);
      }).catch((response) => {
          console.log('Error...', response);
      })
    }
    if(this.props.name){
      this.setState({
        result:"The Scraping <strong>"+this.props.name+"</strong> "+this.props.result, 
        tipo_result: "alert-success"
      })
    }
  }

  builder_state(response){
    //console.log("response.data.data_src: ",response.data.data_src);
    let keys = Object.keys(this.state);
    for(let i=0; i < keys.length; i++){
        let key = keys[i];
        this.state[key] = response.data.data_src[key];
        this.forceUpdate();
        this.update_checkbox(key);
    }
   //console.log('this.state...', this.state);
  }

  handleChange(e){
      if(e.target.type === "checkbox"){
        let value = "";
        if(e.target.checked){
            value = 1;
        }
        this.setState(
            { [e.target.name]: value }
        );
      }else{
        this.setState(
            { [e.target.name]: e.target.value }
        );
      }
  }

  update_checkbox(key){
    if(key in this.state && this.state[key]){
        let tag = document.getElementById(key);
        //console.log(tag);
        if(tag && tag.type === "checkbox"){
            tag.checked = true;
        }
      }
    }

  handleSubmit(btn) {
    if(this.handleValidation()){
      axios({
        method: 'post',
        url: initial.url + 'admins/user/',
        data: this.state,
        xsrfCookieName: initial.csrftoken,
        xsrfHeaderName: initial.xcsrftoken,
        processData: false,
        contentType: false,
        headers: {
            Authorization: localStorage.getItem('token')
        }
      }).then((response) => {
        //console.log(response);
        this.option_save(btn,response)
      }).catch((error) => {
            console.error(error);
            this.set_result_message(null)
      });
    }
  }

  handleValidation(){
    let fields = ['username','password','email','first_name','last_name'];
    let formIsValid = true;
    let datos = this.state
    // console.log('datos...', datos);
    fields.forEach(function(key) {
        if(!(key in datos) || datos[key]==="" || datos[key]==="-9"){
            console.log(key+ ' value is not valid...', datos[key]);
            formIsValid = false;
            document.getElementById(key).style.border = "1px solid red";
        }
   });
   return formIsValid;
  }

  option_save(btn, response){
    // console.log("btn: ",btn);
    if(btn =="3"){
      this.props.history.push({pathname: '/link'});
      this.props.history.push({
        pathname: '/users',
        state: {
            menu: "users",
            name:this.state.name
        }
      });
    }else if (btn =="1"){
      this.props.history.push({
        pathname: '/list_admin',
        state: {
            menu: "users"
        }
        });
    }
    this.set_result_message(response);        
  }

  set_result_message(response=null, error_message="Error has ocurred"){
    if(response){
        this.setState({
            result:"The User <strong>"+this.state.name+"</strong> "+response.data.result,
            tipo_result: "alert-success"
        })
        if(response.data && response.data.data_scr && response.data.data_scr.id){
            this.setState({
                id: response.data.data_scr.id
            })
        }
    }else{
        this.setState({
            result:error_message,
            tipo_result: "alert-danger"
        })
    }
  }

  render() {
    return (
        <div className="container alignment">
            <div className="card"> 
                <div className="card-header">Create User</div>
                <div className="card-body">
                    <form>
                        <div className="form-row">
                            <div className="col-md-4 mb-3" >
                                <label htmlFor="username">Username</label>
                                <input className="form-control" type="text" name="username" id="username" onChange={this.handleChange} value={this.state.username} required/>
                            </div>
                            { !this.props.id ? 
                            <div className="col-md-4 mb-3" >
                                <label htmlFor="name">Password</label>
                                <input className="form-control" type="password" name="password" id="password" onChange={this.handleChange} value={this.state.password} required/>
                            </div>
                            : <div></div>
                            }
                            <div className="col-md-4 mb-3" >
                                <label htmlFor="name">Is SuperUser</label>
                                <input className="form-control" type="checkbox" name="is_superuser" id="is_superuser" onChange={this.handleChange} value={this.state.is_superuser} required/>
                            </div>
                        </div>
                        <div className="form-row">
                            <div className="col-md-4 mb-3" >
                                <label htmlFor="first_name">First name</label>
                                <input className="form-control" type="text" name="first_name" id="first_name" onChange={this.handleChange} value={this.state.first_name} required/>
                            </div>
                            <div className="col-md-4 mb-3" >
                                <label htmlFor="last_name">Last name</label>
                                <input className="form-control" type="text" name="last_name" id="last_name" onChange={this.handleChange} value={this.state.last_name} required/>
                            </div>
                        </div>
                        <div className="form-row">
                            <div className="col-md-4 mb-3" >
                                <label htmlFor="email">Email</label>
                                <input className="form-control" type="email" name="email" id="email" onChange={this.handleChange} value={this.state.email} required/>
                            </div>
                        </div>
                        <div className="form-row">
                            <div className="col-md-4 mb-3" >
                                <label htmlFor="is_active">Is Active</label>
                                <input className="form-control" type="checkbox" name="is_active" id="is_active" onChange={this.handleChange} value={this.state.is_active} required/>
                            </div>
                        </div>
                        {
                            this.state.result?
                            <div className={"alert "+ this.state.tipo_result+" a-form"}>
                            <button type="button" onClick={(e)=>{this.setState({result:undefined})}} className="close" >&times;</button>
                            {Parser(this.state.result)}</div>:null
                        }
                        <div className="card panel-button">
                            <div className="card-body">
                                <button onClick={(e)=>{this.handleSubmit(1)}} type="button" className="btn btn-primary margin-bottom-btn float-right">
                                    Save
                                </button>
                                <button onClick={(e)=>{this.handleSubmit(3)}} type="button" className="btn btn-primary margin-bottom-btn float-right">
                                    Save and add another
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> 
    );
  }
}

export default withRouter(FormUSers)