import React from "react";
//import PropTypes from "prop-types";
//import { withRouter } from "react-router-dom";
import axios from 'axios';
import initial from '../../providers/initial.jsx';
import CategoryField from "../input/select_field_scraping.jsx";
import tematicas from "../../../static/js/tematicas/tematicas.json.js";


export default class FormScraping extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      name: "",
      dominio: "",
      main_link: "",
      nivelurl: [""],
      tipoField: ["PK","image","text"],
      categoryField:[{fieldlocal:"",fieldweb:"",typeField:"",sizetext:""}],
      category:""
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
   
    this.addCategory = this.addCategory.bind(this);
    
    this.removeCategory = this.removeCategory.bind(this);
    
  }

  
  componentDidMount(){
  
  }

  
  addCategory(e){
    this.setState((prevState) => ({
      categoryField: [...prevState.categoryField, {fieldlocal:"", fieldweb:""}],
    }));
  }

  removeCategory(e){
    let categoryField = [...this.state.categoryField];
    categoryField.splice(e.target.dataset.id,1);
    this.setState({ categoryField });
 }
 

  handleChange(e){

    if(["fieldlocal", "fieldweb"].includes(e.target.name)){
      let categoryField = [...this.state.categoryField]
      categoryField[e.target.dataset.id][e.target.name] = e.target.value
      this.setState({ categoryField }, () => console.log(this.state.categoryField))
    } else {
      this.setState({ [e.target.name]: e.target.value });
    }
    
  }

 

 


  handleSubmit(e) {
    e.preventDefault();
    axios({
      method: 'post',
      url: 'http://localhost:8000/api/admins/webservice/',
      data: { 
        name: this.state.name,
        category: this.state.category,
        categoryField: this.state.categoryField},
      xsrfCookieName: initial.csrftoken,
      xsrfHeaderName: initial.xcsrftoken,
      processData: false,
      contentType: false,
      headers: {
          Authorization: localStorage.getItem('token')
      }
    }).then((response) => {
      //handle success
      this.setState({
        result: response.data.result
      })

    }).catch((error) => {
      //handle error
      console.log(error
      );
      
    });

    
  }
  render() {
    
    return (
      <div className="container alignment">
        
        <div className="card"> 
        <div className="card-header">Create Scraping</div>
        <div className="card-body">
        <form onSubmit={this.handleSubmit}>
        
          <div className="form-row">
          <div className="col-md-4 mb-3" >
            <label htmlFor="name">Name</label>
            <input className="form-control" type="text" name="name" onChange={this.handleChange} value={this.state.name} required/>
          </div>
          <div className="col-md-4 mb-3">
            <label htmlFor="Category">Category</label>
            
            <select className="form-control" name="category" value={this.state.category} onChange={this.handleChange} >
              <option value="--9">Seleccionar</option>
              {
                tematicas.arr_tematicas_up.map((tematica, i) => (
                  tematica? 
                  <option key={i} value={tematica.class}>{tematica.title}</option>:null
                ))
              }
            </select>
            
          </div>
          <div className="col-md-4 mb-3" >
            <label htmlFor="method">Dominio</label>
            <input className="form-control" type="text" name="dominio" onChange={this.handleChange} value={this.state.dominio} required/>
         
          </div>
          </div>
          <div className="form-row">
          <div className="col-md-6 mb-3" >
            <label htmlFor="url">Start url</label>
            <input className="form-control" type="text" name="endpoint" onChange={this.handleChange} value={this.state.endpoint} required />
          </div>
          <div className="col-md-6 mb-3" >
            <label htmlFor="url">Class item Link</label>
            <input className="form-control" type="text" name="endpoint" onChange={this.handleChange} value={this.state.endpoint} required />
          </div>
          
        </div>

          <h5 className="subtitle-adm">Fields of the Categories</h5>
          <hr className="hr1" />
          <CategoryField categoryField={this.state.categoryField} category={this.state.category} change={this.handleChange} remove={this.removeCategory} />
          <div className="form-row">
          <button type="button" className="btn btn-outline-primary margin-bottom-btn" onClick={this.addCategory}> <i className="fa fa-plus"></i>Add Field Category</button>
          </div>
          

          <div className="form-row">
            <button type="submit" className="btn btn-outline-primary btn-block margin-bottom-btn">
              Save
            </button>
          </div>
          
          <div>{this.state.result}</div>
          

          
        </form>
        </div>
        </div>
        </div> 
        
      
    );
  }
}
