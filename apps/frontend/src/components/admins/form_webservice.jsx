import React from "react";
//import PropTypes from "prop-types";
//import { withRouter } from "react-router-dom";
import "babel-polyfill";
import axios from 'axios';
import initial from '../../providers/initial.jsx';
import HeaderField from "../input/input_headers.jsx";
import CategoryField from "../input/select_field.jsx";
import { get_field_category } from "../input/util.jsx";
import { withRouter } from "react-router-dom";
import Parser from 'html-react-parser';
import tematicas from "../../../static/js/tematicas/tematicascsw.json.js";


class FormWebService extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      name: "",
      endpoint: "",
      category: "-9",
      method: "get",
      datosWebservice: [""],
      pagination: "no",
      key_page: "",
      init_page: "",
      end_page: "",
      id: "",
      fields: [],
      headerFields: [{ headerkey: "", headervalue: "" }],
      categoryField: [{ fieldlocal: "-9", fieldweb: "-9" }],
      result: undefined,
      tipo_result: undefined,
      datosHide: false
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.addHeader = this.addHeader.bind(this);
    this.addCategory = this.addCategory.bind(this);
    this.removeHeader = this.removeHeader.bind(this);
    this.removeCategory = this.removeCategory.bind(this);
    this.getWebService = this.getWebService.bind(this);
    this.handleChangeCategory = this.handleChangeCategory.bind(this);
  }


  componentDidMount(e) {
    //console.log('--------axios--- props_webservice', this.props.id);

    if (this.props.id) {

      axios.get(initial.url + 'admins/get_webservice/', {
        params: { 'id_web': this.props.id },
        headers: {
          Authorization: localStorage.getItem('token')
        }
      }).then((response) => {
        //handle success
        //console.log("get_webservice: ",response)
        this.builder_satate(e, response);
      }).catch((response) => {
        console.log('error ----', response);
      })
    }
    if (this.props.name) {
      this.setState({
        result: "The web service <strong>" + this.props.name + "</strong> " + this.props.result,
        tipo_result: "alert-success"
      })
    }



  }

  builder_satate(e, response) {

    let headers = [];
    let categoryField = [];
    response.data.data_header.map((header, i) => {
      headers.push({ headerkey: header.key, headervalue: header.value })
    });
    response.data.data_fields.map((field, i) => {
      categoryField.push({ fieldlocal: field.field_chm, fieldweb: field.field_web })
    });
    this.setState({
      name: response.data.data_web.name,
      endpoint: response.data.data_web.endpoint,
      category: response.data.data_web.category,
      method: response.data.data_web.method,
      pagination: response.data.data_web.pagination,
      key_page: response.data.data_web.page_key,
      init_page: response.data.data_web.page_init,
      end_page: response.data.data_web.page_end,
      id: response.data.data_web.id,
      headerFields: headers
    })
    this.getWebService(this);
    this.setState({ categoryField })

  }

  addHeader(e) {
    this.setState((prevState) => ({
      headerFields: [...prevState.headerFields, { headerkey: "", headervalue: "" }],
    }));

  }
  addCategory(e) {
    this.setState((prevState) => ({
      categoryField: [...prevState.categoryField, { fieldlocal: "", fieldweb: "" }],
    }));
  }

  removeCategory(e) {
    let categoryField = [...this.state.categoryField];
    categoryField.splice(e.target.dataset.id, 1);
    this.setState({ categoryField });
  }
  removeHeader(e) {
    let headerFields = [...this.state.headerFields];
    headerFields.splice(e.target.dataset.id, 1);
    this.setState({ headerFields });
  }

  handleChangeCategory(e) {

    if ("-9" != e.target.value) {
      if ("csw" == e.target.value) {
        this.setState({
          datosHide: true
        })
      } else {
        axios.get(initial.url + 'category_fields/', {
          params: { 'categoria': e.target.value },
          headers: {
            Authorization: localStorage.getItem('token')
          }
        }).then((response) => {
          //handle success
          //console.log("fields: ",response);
          this.setState({
            fields: response.data.fields,
            datosHide: false
          });
        }).catch((response) => {
          console.log('error ----', response);
        });
      }
    }
  }



  handleChange(e) {

    if (["headerkey", "headervalue"].includes(e.target.name)) {
      let headerFields = [...this.state.headerFields]
      headerFields[e.target.dataset.id][e.target.name] = e.target.value
      this.setState({ headerFields });
      //console.log(this.state.headerFields);
    } else if (["fieldlocal", "fieldweb"].includes(e.target.name)) {
      let categoryField = [...this.state.categoryField]
      categoryField[e.target.dataset.id][e.target.name] = e.target.value
      this.setState({ categoryField })
      //console.log(this.state.categoryField)
    } else {
      this.setState({ [e.target.name]: e.target.value });
    }

  }

  handleValidation() {

    let formIsValid = true;

    let datos = this.state
    if (!this.state.datosHide) {
      Object.keys(datos).forEach(function (key) {
        if (["name", "endpoint", "category"].includes(key)) {
          if (datos[key] == "" || datos[key] == "-9") {
            formIsValid = false;
          }
        }
      });
      this.state.categoryField.map((val, i) => {
        if (this.state.categoryField[i].fieldlocal == "-9" || this.state.categoryField[i].fieldweb == "-9" || this.state.categoryField[i].fieldlocal == "" || this.state.categoryField[i].fieldweb == "") {
          formIsValid = false;
        }
      })
    }


    return formIsValid;
  }


  getWebService(e) {
    //e.preventDefault();


    let headers_key = {
      Authorization: localStorage.getItem('token'),
      'Access-Control-Allow-Origin': '*'
    }
    this.state.headerFields.map((items, i) => {
      headers_key[items.headerkey] = items.headervalue;
    });
    headers_key['Authorization'] = localStorage.getItem('token')


    axios({
      method: this.state.method,
      url: this.state.endpoint,
      data: {},
      headers: headers_key,
      processData: false,
      contentType: false,
    }).then((response) => {
      //handle success
      //console.log(response);
      let self = this
      let datosWeb = []
      this.consultarWebservice(response.data, self, datosWeb, "");
    }).catch((error) => {
      //handle error
      console.log(error);

    });

  }

  // Metodo para leer el arbol del json del webservice. 
  consultarWebservice(datos, self, datosWeb, name_key) {

    Object.keys(datos).forEach(function (key) {

      name_key = name_key + "." + key
      if (typeof (datos[key]) != "object" || datos[key] == null) {
        datosWeb.push(name_key);
      } else {
        let obj = self.first_obj(datos[key]);
        let obj2 = datos[key][obj];
        if (typeof (obj2) != "object") {
          obj2 = datos[key]
        } else {
          name_key = name_key + "." + obj;
        }
        self.consultarWebservice(obj2, self, datosWeb, name_key);
      }
      let index = name_key.lastIndexOf(".");
      name_key = name_key.slice(0, index);
    })

    let datosWebservice = [...datosWeb]
    this.setState({ datosWebservice });
    //console.log(this.state.datosWebservice)
  }

  first_obj(obj) {
    for (var a in obj) return a;
  }


  handleSubmit(btn) {
    //e.preventDefault();
    this.state.headerFields['token'] = {
      Authorization: localStorage.getItem('token')
    }
    if (this.handleValidation()) {
      axios({
        method: 'post',
        url: initial.url + 'admins/webservice/',
        data: {
          name: this.state.name,
          endpoint: this.state.endpoint,
          category: this.state.category,
          headers: this.state.headerFields,
          pagination: this.state.pagination,
          key_page: this.state.key_page,
          init_page: this.state.init_page,
          end_page: this.state.end_page,
          method: this.state.method,
          id: this.state.id,
          categoryField: this.state.categoryField
        },
        xsrfCookieName: initial.csrftoken,
        xsrfHeaderName: initial.xcsrftoken,
        processData: false,
        contentType: false,
        headers: {
          Authorization: localStorage.getItem('token')
        }
      }).then((response) => {
        //handle success
        //console.log(response);
        this.option_save(btn, response);
        /*this.setState({
          result: response.data.result
        })*/
      }).catch((error) => {
        //handle error
        console.log(error
        );

      });
    } else {

      this.setState({
        tipo_result: "alert-danger",
        result: "Required fields cannot be empty"
      })
    }



  }



  option_save(btn, response) {
    // console.log("btn: ",btn);
    if (btn == "3") {
      this.props.history.push({ pathname: '/link' });
      this.props.history.push({
        pathname: '/webservice',
        state: { menu: "webservice", name: this.state.name, result: response.data.result }
      });
    } else if (btn == "1") {
      this.props.history.push({
        pathname: '/list_admin',
        state: { menu: "webservice" }
      });
    } else {
      this.setState({
        id: response.data.data_web.id,
        result: "The web service <strong>" + this.state.name + "</strong> " + response.data.result,
        tipo_result: "alert-success"
      })
    }
  }


  render() {

    return (
      <div className="container alignment">

        <div className="card">
          <div className="card-header">Create Web Service</div>
          <div className="card-body">
            <form>

              <div className="form-row">
                <div className="col-md-4 mb-3" >
                  <label htmlFor="name">Name</label>
                  <input className="form-control" type="text" name="name" onChange={this.handleChange} value={this.state.name} required />
                </div>
                <div className="col-md-4 mb-3">
                  <label htmlFor="Category">Category</label>

                  <select className="form-control" name="category" value={this.state.category} onChange={(e) => { this.handleChange(e); this.handleChangeCategory(e); }} >
                    <option value="-9">Select Category</option>
                    {
                      tematicas.arr_tematicas_up.map((tematica, i) => (
                        tematica ?
                          <option key={i} value={tematica.class}>{tematica.title}</option> : null
                      ))
                    }
                  </select>

                </div>
                <div className="col-md-4 mb-3" >
                  <label htmlFor="method">Method</label>
                  <select name="method" className="form-control" onChange={this.handleChange} value={this.state.method}>
                    <option value="get">GET</option>
                    <option value="post">POST</option>
                  </select >
                </div>
              </div>
              <div className="form-row">
                <div className="col-md-12 mb-3" >
                  <label htmlFor="url">Url Web Service</label>
                  <input className="form-control" type="text" name="endpoint" onChange={this.handleChange} value={this.state.endpoint} required />
                </div>

              </div>


              <div className="form-row">
                <div className="col-md-4 mb-3">
                  <label className="radio-inline"><span className="haspage" >this has pagination?</span>  </label>
                  <label className="radio-inline haspage">
                    <input type="radio" name="pagination" value="yes" checked={this.state.pagination === 'yes'} onChange={this.handleChange} />Yes
                  </label>
                  <label className="radio-inline">
                    <input type="radio" name="pagination" value="no" checked={this.state.pagination === 'no'} onChange={this.handleChange} />No
                  </label>
                </div>
              </div>
              {
                this.state.pagination == "yes" ?
                  <div className="form-row">
                    <div className="col-md-2 mb-3">
                      <label htmlFor="end_page">Key page </label>
                      <input className="form-control" type="text" name="key_page" onChange={this.handleChange} value={this.state.key_page} required />
                    </div>
                    <div className="col-md-1 mb-3">
                      <label htmlFor="ini_page">Init page </label>
                      <input className="form-control" type="text" name="init_page" onChange={this.handleChange} value={this.state.init_page} required />
                    </div>
                    <div className="col-md-1 mb-3">
                      <label htmlFor="end_page">End page </label>
                      <input className="form-control" type="text" name="end_page" onChange={this.handleChange} value={this.state.end_page} required />
                    </div>
                  </div>
                  : null
              }
              <div className={this.state.datosHide ? 'hidden' : ''}>
                <h5 className="subtitle-adm">Headers</h5>
                <hr className="hr1" />
                <HeaderField headersFields={this.state.headerFields} change={this.handleChange} remove={this.removeHeader} />
                <div className="form-row">
                  <button type="button" className="btn btn-outline-primary margin-bottom-btn" onClick={this.addHeader}><i className="fa fa-plus"></i> Add new Header</button>
                  <button type="button" className="btn btn-outline-success btn-block margin-bottom-btn" onClick={this.getWebService}>Get Web Service</button>
                </div>

                <h5 className="subtitle-adm">Fields of the Categories</h5>
                <hr className="hr1" />
                <CategoryField categoryField={this.state.categoryField} fields={this.state.fields} category={this.state.category} change={this.handleChange} remove={this.removeCategory} datoswebservice={this.state.datosWebservice} />
                <div className="form-row">
                  <button type="button" className="btn btn-outline-primary margin-bottom-btn" onClick={this.addCategory}> <i className="fa fa-plus"></i>Add Field Category</button>
                </div>
              </div>
              {
                this.state.result ?
                  <div className={"alert " + this.state.tipo_result + " a-form"}>
                    <button type="button" onClick={(e) => { this.setState({ result: undefined }) }} className="close" >&times;</button>
                    {Parser(this.state.result)}</div> : null
              }

              <div className="card panel-button">
                <div className="card-body">
                  <button onClick={(e) => { this.handleSubmit(1) }} type="button" className="btn btn-primary margin-bottom-btn float-right">
                    Save
                  </button>
                  <button onClick={(e) => { this.handleSubmit(2) }} type="button" className="btn btn-primary margin-bottom-btn float-right">
                    Save and continue editing
                  </button>
                  <button onClick={(e) => { this.handleSubmit(3) }} type="button" className="btn btn-primary margin-bottom-btn float-right">
                    Save and add another
                  </button>


                </div>
              </div>
            </form>
          </div>
        </div>
      </div>


    );
  }
}

export default withRouter(FormWebService)