import React from 'react';
import { withRouter } from "react-router-dom";
import { translate } from "react-translate";

import tematicas from "../../../static/js/tematicas/tematicas.json.js"; // traemos las tematicas 
class BoardBanner extends React.Component {
    constructor(props) {
        super(props);
        // console.log(props);

        this.state = {
            totalRecords: '',
            search:''
        }
        this.handleClick = this.handleClick.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.search = React.createRef();
        this.selectTematicas = React.createRef();
    }


    handleClick(currentElement) {

        this.props.history.push({
            pathname: '/table',
            // search: '?=' + this.selectTematicas.current.value + '?=' + this.search.current.value,
            // search: '?=experts' + '?=' + this.search.current.value.trim(),
            // search: '?=all' + '?=' + this.search.current.value.trim(),
            state: { tematica: 'all', search: this.state.search.trim() }
        });
    }
    handleChange(event) {

        this.setState({
            search: event.target.value
        })

    }


    render() {
        let { t } = this.props;
        return (
            <section className="banner">
                <div className="container">
                    <div className="row align-items-center pb-5">
                        <div className="col-md-6">
                            <h1 className="title-site">Clearing-House Mechanism LAC</h1>
                            <p>
                            {t('chm_desc')}    
                            </p>
                        </div>
                        <div className="col-md-6">

                            <div className="form-row">
                                <div className="col">
                                    <div className="form-group">
                                        <div className="input-wi">
                                            <input
                                                id="searchtxt"
                                                type="text"
                                                className="form-control"
                                                placeholder={t('Search')}
                                                value={this.state.search}
                                                onChange={this.handleChange}
                                                ref={this.search} />
                                            <i className="fa fa-search" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                                {/* <div className="col-sm-4">
                                    <div className="form-group">
                                        <select id="catsel" style={{ cursor: 'pointer' }} className="bootstrap-select  form-control" ref={this.selectTematicas}>
                                            {tematicas.arr_tematicas_up.map((tematica, i) => (
                                                <option key={i} value={tematica.class}>{tematica.title}</option>
                                            ))}
                                        </select>
                                    </div>
                                </div> */}
                                <div className="col-sm-3">
                                    <button id="searchbtn"
                                        type="button"
                                        className="btn btn-primary"
                                        disabled={!this.state.search}
                                        onClick={this.handleClick.bind(this)}>
                                        {t('Search')}
                                        </button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        )
    }

}

export default withRouter (translate('App')(BoardBanner))