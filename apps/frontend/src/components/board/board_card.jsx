import React from 'react';
import Card from '../card/card.jsx';
import tematicas from "../../../static/js/tematicas/tematicas.json.js"; // traemos las tematicas 
export default class BoardCard extends React.Component {

    constructor(props) {
        super(props);
        this.state = {}

    }

    //This will run first automatically
    componentWillMount() {
    }

    card_create() {
        var list_card = [];
        tematicas.arr_tematicas_up.map((tematica, i) => {
            if (tematica.show) list_card.push(<Card key={i} {...tematica} />)
        })
        return list_card;
    }

    render() {
        return (
            <section className="services">
                <div className="container">
                    <div className="row align-items-center row-cat animated fadeInUp">
                        {this.card_create()}
                    </div>
                </div>
            </section>
        )
    }
}