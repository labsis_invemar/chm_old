import React from 'react';
import { translate } from "react-translate";

class TableStatistics extends React.Component {

    order_ = false;
    constructor(props) {
        super(props);
        // console.log('pagina TableStadisticas', props);

        this.state = {
            data: [],
            header: [],
            total_: 0,
            option: 1,
            show_loader: true
        }

        this.create_headers_table = this.create_headers_table.bind(this);
        this.create_data_table = this.create_data_table.bind(this);
        this.sum_total = this.sum_total.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.data != this.state.data) {

            this.setState({
                data: nextProps.data.sort(this.Sort_arr_(parseInt(nextProps.option))),
                header: nextProps.header,
                total_: this.sum_total(nextProps.data),
                option: parseInt(nextProps.option)
            }, () => {
                console.log('----------', this.state);
            })
        }

        if (nextProps.show_loader != this.state.show_loader) {
            this.setState({
                show_loader: nextProps.show_loader
            })
        }
    }


    create_headers_table() {
        let list_h = [];
        this.state.header.map((e) => {
            let name = e.charAt(0).toUpperCase() + e.slice(1);
            list_h.push(
                <th key={e} >
                    <a style={{ 'color': '#ffffff' }} href="#" onClick={(ev) => {
                        this.handleClick(ev, e)
                    }}>{name}</a>
                </th>
            )
        })
        return list_h;
    }

    handleClick(ev, e) {
        ev.preventDefault();
        this.order_ = !this.order_
        this.setState({
            data: this.state.data.sort(this.dynamicSort(e, this.order_))
        })

    }

    /*create_data_table(obj_data) {
        var lis_td = [];
        this.state.header.map((value, i) => {
            if (obj_data['name'] != 'total') {
                if (obj_data[value] == null && value == 'name') {
                    lis_td.push(
                        <td key={i}>D/N</td>
                    )
                } else if (value == '%') {
                    let val_ = ((parseInt(obj_data['total']) * 100) / parseInt(this.state.total_))
                    lis_td.push(
                        <td key={i}>{val_.toFixed(2)}%</td>
                    )
                } else if (value == 'name') {
                    let name = obj_data[value].charAt(0).toUpperCase() + obj_data[value].slice(1);
                    lis_td.push(
                        <td key={i}>{name}</td>
                    )
                } else if (value == 'country') {
                    let name = obj_data['name'];
                    if (name) name = name.substring(0, 2);
                    lis_td.push(
                        <td key={i}><img src={'https://www.countryflags.io/' + name + '/flat/64.png'} /></td>
                    )
                } else {
                    lis_td.push(
                        <td key={i}>{obj_data[value]}</td>
                    )
                }
            }

        })
        return lis_td
    }*/

    create_data_table(obj_data, category) {
        var lis_td = [];
        this.state.header.map((value, i) => {
            let val_ = this.validate_text_table(obj_data, value);
            if (val_) {
                if (this.state.option == 4 && value == 'country') {
                    lis_td.push(
                        <td key={i}><img src={val_} /></td>
                    )
                } else if (value == '%') {
                    lis_td.push(

                        <td key={i}>
                            <div className="progress">
                                <div
                                    className="progress-bar"
                                    role="progressbar"
                                    aria-valuenow="70"
                                    aria-valuemin="0"
                                    aria-valuemax="100"
                                    style={{ "width": val_ }}>
                                </div>
                            </div>
                            {val_}
                        </td>

                    )
                } else {
                    lis_td.push(
                        <td key={i}>{val_}</td>
                    )
                }
            }
        })
        return lis_td
    }

    validate_text_table(obj_data, value) {
        if (obj_data['name'] != 'total') {
            let htl = '';
            if (value == 'name') {
                if (obj_data[value] == null) {
                    htl = 'D/N';
                } else {
                    let name = obj_data[value].charAt(0).toUpperCase() + obj_data[value].slice(1);
                    htl = name;
                }

            } else if (value == '%') {
                let val_ = ((parseInt(obj_data['total']) * 100) / parseInt(this.state.total_));
                htl = val_.toFixed(2) + '%';

            } else if (value == 'country') {
                if (this.state.option == 4) {
                    let name = obj_data['name'];
                    if (name) name = name.substring(0, 2);
                    htl = 'https://www.countryflags.io/' + name + '/flat/64.png';

                } else {
                    if (obj_data[value] == null || obj_data[value] == "") {
                        htl = 'D/N';
                    } else {
                        let name = obj_data[value].charAt(0).toUpperCase() + obj_data[value].slice(1);
                        htl = name;
                    }

                }
            } else {
                htl = obj_data[value];

            }

            return htl;
        }
    }

    Sort_arr_(option) {
        var this_ = this;
        return function (a, b) {
            if (option == 2) {
                var m1 = a.country,
                    e1 = a.total,
                    c1 = a.category,
                    m2 = b.country,
                    e2 = b.total,
                    c2 = b.category
                return c1 < c2 ? -1 : c1 > c2 ? 1 :
                    e1 > e2 ? -1 : e1 < e2 ? 1 : 0;
                // return (JSON.parse(a.MODEL).metodos.value > JSON.parse(b.MODEL).metodos.value) ? 1 : ((JSON.parse(b.MODEL).metodos.value > JSON.parse(a.MODEL).metodos.value) ? -1 : 0);
            } else {
                return (a.total > b.total) ? -1 : (a.total < b.total) ? 1 : 0;
            }
        }

    }

    dynamicSort(property, may = false) {
        var sortOrder = 1;
        var result;
        if (property[0] === "-") {
            sortOrder = -1;
            property = property.substr(1);
        }
        return function (a, b) {
            /* next line works with strings and numbers, 
             * and you may want to customize it to your needs
             */
            if (may) result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
            if (!may) result = (a[property] > b[property]) ? -1 : (a[property] < b[property]) ? 1 : 0;
            return result * sortOrder;
        }
    }

    sum_total(data) {
        if (data) {
            let obj_total = data.find((value) => {
                if (value['name'] == 'total') {
                    return value
                }
            })
            if (obj_total) {
                return obj_total['total']
            }

            return 0;
        }
        return 0;


    }


    render() {
        let order_m = this.state.option == 1 ? false : true;
        let order_t = this.state.option == 1 ? "total" : "category";
        let { t } = this.props;
        return (
            <div>
                <div className="container alignment">
                    {this.state.show_loader ?
                        <div className="d-flex justify-content-center spinner-header">
                            <div className="spinner-grow text-primary" style={{ "width": '3rem', "height": '3rem' }} role="status">
                                <span className="sr-only">{t('Loading')}...</span>
                            </div>
                            <span>{t('Please wait')}...</span>
                        </div> :
                        <div className="table-container table-responsive bordered">
                            <div className="table-container">
                                <div className="mb-4 total-number">{t('Total number of data sets')}:
                                <span className="span_total">{this.state.total_}
                                    </span>
                                </div>
                                {this.state.total_ > 0 ?
                                    <table className="table table-hover">
                                        <thead>
                                            <tr className="bg-primary">
                                                {this.create_headers_table()}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                this.state.data.map((values, i) => (
                                                    <tr key={i}>
                                                        {this.create_data_table(values)}
                                                    </tr>
                                                ))
                                            }
                                        </tbody>
                                    </table>
                                    : null}
                                <nav aria-label="Page navigation example">
                                    <ul className="pagination justify-content-center">

                                    </ul>
                                </nav>
                            </div>
                        </div>}

                </div>
            </div>
        )
    }
}

export default translate('App')(TableStatistics)