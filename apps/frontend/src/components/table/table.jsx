import React from 'react';
import axios from 'axios';
import initial from '../../providers/initial.jsx';
import Facets from '../facets/facets.jsx';
import { withRouter } from "react-router-dom";
import CardTable from '../card/card_table.jsx';
import { translate } from "react-translate";


class Table extends React.Component {
    _isMounted = true;
    constructor(props) {
        super(props);
        this.state = {
            headerFields: [],
            data: [],
            table_keys_value: [],
            pagination_num: 0,
            categoria: undefined,
            page_current: 1,
            search: undefined,
            search_temathic: {},
            show_loader: true,
            title: '',
            between_data: {},
            facets_data: {}
        }
        this.handleClick = this.handleClick.bind(this);
        this.searchEvent = this.searchEvent.bind(this);
        this.facetsHandler = this.facetsHandler.bind(this);
        this.showFiltersbtn = this.showFiltersbtn.bind(this);
        this.cont = React.createRef();
    }

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.tematica != this.props.tematica) {
            this.state.show_loader = true;
            this.get_data_ajax({
                'categoria': nextProps.tematica,
                'search': nextProps.search,
                'page': nextProps.page_current,
                'facets_data': {}
            })
        }
    }

    componentWillMount() {
        this.props.emmmiter.on('update_search', this.searchEvent);

    }

    componentWillUnmount() {
        this._isMounted = false;
        this.props.emmmiter.removeEventListener('update_search', this.searchEvent);
    }

    searchEvent(event) {
        this.state.show_loader = true;
        this.get_data_ajax({
            'categoria': 'all',
            'search': event,
            'page': 1,
            'facets_data': {}
        })
    }

    handleClick(e) {
        e.preventDefault();
        let page = e.target.attributes.getNamedItem('data-value').value;
        /*this.props.history.push({
            pathname: '/table',
            search: this.state.search && this.state.search != 'undefined' ?
                '?=' + this.state.categoria + '?=' + this.state.search + '?=' + page :
                '?=' + this.state.categoria + '?=' + '?=' + page,
            state: this.state.categoria
        });*/
        if (page != this.state.page_current) {
            this.get_data_ajax({
                'categoria': this.state.categoria,
                'page': page,
                'search': this.state.search,
                'is_pagination': true,
                'facets_data': JSON.stringify(this.state.facets_data)
            })
        }
    }

    get_data_ajax(data) {
        axios.get(initial.url + 'category/' + data.categoria + '/data/', {
            params: data
        }).then((response) => {
            //handle success
            this.builder_satate(response);


        }).catch((response) => {
            console.log('error ----', response);
        })
    }

    builder_satate(response) {

        // console.log(' ---- data --', response.data);


        let table_keys_value_ = [];
        response.data.fields.map((value) => {
            if (value[value.key]) {
                table_keys_value_.push(value.key)
            }

        })

        this.setState({
            headerFields: response.data.fields,
            data: response.data.data,
            table_keys_value: table_keys_value_,
            pagination_num: response.data.pagination_num,
            categoria: response.data.categoria,
            page_current: response.data.page_current,
            search: response.data.search,
            show_loader: false,
            search_temathic: response.data.is_pagination ? this.state.search_temathic : response.data.search_temathic,
            title: response.data.title,
            between_data: response.data.between_data,
            facets_data: response.data.facets_data
        }, () => {
            this.props.emmmiter.emit('search', { search: response.data.search });
            this.props.emmmiter.emit('loader_data', { loader_data: true });
            this.props.action(this.state);

        });


    }

    paginationCreate(page_current) {

        var i = 0;
        let cantidad_mostrar = 5;

        if (this.state.pagination_num <= 5) {
            cantidad_mostrar = this.state.pagination_num;
        } else if (this.state.data.length == 0) {
            cantidad_mostrar = 0;

        } else if (page_current == 1) {
            i = parseInt(0);

        } else if (page_current == 2) {
            i = parseInt(page_current) - 2;
            cantidad_mostrar = parseInt(page_current) + 2;

        } else if (page_current < this.state.pagination_num) {
            i = parseInt(page_current) - 3;
            cantidad_mostrar = parseInt(page_current) + 2;
            if (cantidad_mostrar > this.state.pagination_num) {
                cantidad_mostrar = this.state.pagination_num;
            }

        } else if (page_current == this.state.pagination_num) {
            i = this.state.pagination_num - 4;
            cantidad_mostrar = this.state.pagination_num;

        }

        var lis = [];

        for (i; i < cantidad_mostrar; i++) {
            lis.push(
                <li key={i} className={"page-item " + (this.state.page_current == (i + 1) ? 'active' : ' ')}>
                    <a className="page-link" href="#" data-value={i + 1} onClick={this.handleClick}>{i + 1}</a>
                </li>);
        }
        return lis;
    }


    tableCreate(data_values) {
        var lis_td = [];
        this.state.table_keys_value.map((value, i) => {
            if (value == 'imagen') {
                lis_td.push(
                    <td key={i}>
                        <div className="divimage" style={{ 'background': 'url(' + data_values[value] + ')' }}></div>

                    </td>
                )
            } else if (value == 'nombre' && data_values['url']) {
                lis_td.push(
                    <td key={i}>
                        <a target="blank" href={data_values['url']}>{data_values[value]}</a>
                    </td>
                )
            } else {
                lis_td.push(
                    <td key={i}>{data_values[value]}</td>
                )
            }
        })
        return lis_td
    }

    facetsHandler(data) {
        // console.log('----table get facets--', data.facets_data);
        /*this.setState({
            facets_data: data.facets_data
        }, () => {
            this.get_data_ajax({
                'categoria': this.state.categoria,
                'page': 1,
                'search': this.state.search,
                'is_pagination': true,
                'facets_data': JSON.stringify(this.state.facets_data)
            })

        })*/

        this.get_data_ajax({
            'categoria': this.state.categoria,
            'page': 1,
            'search': this.state.search,
            'is_pagination': true,
            'facets_data': JSON.stringify(data.facets_data)
        })

    }

    showFiltersbtn(event, categoria) {
        if (this.cont.current.className.indexOf("active") != -1) {
            this.cont.current.className = categoria;
        } else {
            this.cont.current.className = "active " + categoria;
        }

    }

    render() {
        let { t } = this.props;
        return (
            <section className="resultados">
                {this.state.show_loader ? <div className="d-flex justify-content-center">
                    <div className="spinner-grow text-primary" style={{ "width": '3rem', "height": '3rem' }} role="status">
                        <span className="sr-only">{t('Loading')}...</span>
                    </div>
                </div> : null}
                {!this.state.show_loader ? <div className="wrapper">
                    <nav id="sidebar" className={"active " + this.state.categoria} ref={this.cont}>
                        <Facets {...this.state} action={this.facetsHandler} />
                    </nav>
                    <div className="container" id="content">
                        <div className="row">
                            <div className="col-md-12">
                                <h4 className={'color-' + this.state.categoria}>{t(this.state.title)}</h4>
                                <p className="lean">
                                    {t('Showing')} {t('results')}
                                <b> {this.state.between_data.inicio_info} - {this.state.between_data.final_info} </b>
                                {t('of')} <b> {this.state.between_data.total} </b> {this.state.search ? <span>{t('for')} <i> {this.state.search} </i></span> : null}
                                </p>
                                <button
                                    onClick={(e) => {
                                        this.showFiltersbtn(e, this.state.categoria)
                                    }}
                                    type="button"
                                    id="sidebarCollapse"
                                    className={"btn btn-primary" + this.state.categoria}>
                                    <i className="fa fa-align-left"></i>
                                    <span>{t('Filters')}</span>
                                </button>
                                <hr />
                                {this.state.data.map((data, i) => (
                                    <CardTable
                                        key={i}
                                        data_values={data}
                                        data_keys={this.state.table_keys_value}
                                        type_card={this.state.categoria}
                                        data_headers={this.state.headerFields} />
                                ))}
                            </div>
                        </div>
                        <nav aria-label="Page navigation example">
                            <ul className="pagination justify-content-center">
                                {this.state.page_current > 3 ? <li className="page-item">
                                    <a className="page-link" href="#" data-value="1" onClick={this.handleClick}>❮❮</a>
                                </li> : null}
                                {this.state.page_current > 3 ? <li className="page-item">
                                    <a
                                        className="page-link"
                                        href="#"
                                        data-value={parseInt(this.state.page_current) - 1}
                                        onClick={this.handleClick}>❮</a>
                                </li> : null}
                                {this.paginationCreate(this.state.page_current)}
                                {this.state.page_current < parseInt(this.state.pagination_num) - 2 ? <li className="page-item">
                                    <a
                                        className="page-link"
                                        href="#"
                                        data-value={parseInt(this.state.page_current) + 1}
                                        onClick={this.handleClick}>❯</a>
                                </li> : null}
                                {this.state.page_current < parseInt(this.state.pagination_num) - 2 ? <li className="page-item">
                                    <a
                                        className="page-link"
                                        href="#"
                                        data-value={this.state.pagination_num}
                                        onClick={this.handleClick}>❯❯</a>
                                </li> : null}
                            </ul>
                        </nav>
                        {/* <div className="table-container table-responsive bordered">
                        <div className="table-container">
                            {this.state.show_loader ? <div className="d-flex justify-content-center">
                                <div className="spinner-grow text-primary" style={{ "width": '3rem', "height": '3rem' }} role="status">

                                    <span className="sr-only">Loading...</span>
                                </div>
                            </div> : null}
                            {!this.state.show_loader ? <table className="table table-hover">
                                <thead>
                                    <tr className="bg-primary">
                                        {
                                            this.state.headerFields.map((field, i) => (
                                                field[field.key] ? <th key={i} >
                                                    <a style={{ 'color': '#ffffff' }} href="#">{field[field.key]}</a>
                                                </th> : null

                                            ))
                                        }
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.data.map((data_values, i) => (
                                        <tr key={i}>
                                            {this.tableCreate(data_values)}
                                        </tr>
                                    ))}
                                </tbody>
                            </table> : null}
                            <nav aria-label="Page navigation example">
                                <ul className="pagination justify-content-center">
                                    {this.state.page_current > 3 ? <li className="page-item">
                                        <a className="page-link" href="#" data-value="1" onClick={this.handleClick}>❮❮</a>
                                    </li> : null}
                                    {this.state.page_current > 3 ? <li className="page-item">
                                        <a className="page-link" href="#" data-value={parseInt(this.state.page_current) - 1} onClick={this.handleClick}>❮</a>
                                    </li> : null}
                                    {this.paginationCreate(this.state.page_current)}
                                    {this.state.page_current < parseInt(this.state.pagination_num) - 2 ? <li className="page-item">
                                        <a className="page-link" href="#" data-value={parseInt(this.state.page_current) + 1} onClick={this.handleClick}>❯</a>
                                    </li> : null}
                                    {this.state.page_current < parseInt(this.state.pagination_num) - 2 ? <li className="page-item">
                                        <a className="page-link" href="#" data-value={this.state.pagination_num} onClick={this.handleClick}>❯❯</a>
                                    </li> : null}
                                </ul>
                            </nav>
                        </div>
                    </div> */}
                    </div>
                </div> : null}
            </section>
        )
    }
}

export default withRouter (translate('App')(Table))