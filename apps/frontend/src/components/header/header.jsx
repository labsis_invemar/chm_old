import React from 'react';
import { withRouter } from "react-router-dom";
import { translate } from "react-translate";


class Header extends React.Component {
    _isMounted = true;
    constructor(props) {
        super(props);

        this.state = {
            show_search: false,
            search: ''
        }

        this.addEvent = this.addEvent.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.handleClickAdmin = this.handleClickAdmin.bind(this);
        this.handleClickLogin = this.handleClickLogin.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleClickSearch = this.handleClickSearch.bind(this);
        this.handleClickStatistics = this.handleClickStatistics.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillMount() {
        this.props.emmmiter.on('foo', this.addEvent);
        this.props.emmmiter.on('search', this.addEvent);
    }

    componentWillUnmount() {
        this._isMounted = false;
        this.props.emmmiter.removeEventListener('foo', this.addEvent);
        this.props.emmmiter.removeEventListener('search', this.addEvent);
    }

    handleChange(event) {
        this.setState({
            search: event.target.value
        })

    }

    addEvent(event) {

        if (event.show) {
            this.setState({
                show_search: true
            })
        }
        if (event.search != null) {
            this.setState({
                search: event.search
            })
        }

    }

    handleClickSearch(e) {
        this.props.emmmiter.emit('update_search', this.state.search.trim())
    }


    handleClick(e) {
        e.preventDefault();
        this.props.history.push({
            pathname: '/list_admin',
            state: { menu: 'webservice' }
        });
    }

    handleClickAdmin(e) {
        e.preventDefault();
        this.props.history.push({
            pathname: '/list_admin/',
            state: { menu: 'webservice' }
        });
    }

    handleClickStatistics(e) {
        e.preventDefault();
        this.props.history.push({
            pathname: '/statistics/',
        });
    }

    handleClickLogin(e) {
        e.preventDefault();
        this.props.history.push({
            pathname: '/login/'
        });
    }

    render() {
        let { t } = this.props;
        return (
            <header className="h fixed-top">
                <nav className="navbar navbar-expand-lg navbar-light bg-light">
                    <div className="container">
                        <button
                            className="navbar-toggler"
                            type="button"
                            data-toggle="collapse"
                            data-target="#navbarTogglerDemo03"
                            aria-controls="navbarTogglerDemo03"
                            aria-expanded="false"
                            aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <a className="navbar-brand" href="">
                            <img className="logo-web" src={"../../static/images/logo-web.svg"} alt="OceanFinder" />
                        </a>

                        <div className="collapse navbar-collapse" id="navbarTogglerDemo03">
                            {this.state.show_search ? <div className="input-wi form-inline my-2 my-lg-0 d-none d-md-block">
                                <input
                                    className="form-control mr-sm-2"
                                    type="search"
                                    placeholder={t("Search")}
                                    aria-label="Search"
                                    value={this.state.search}
                                    onChange={this.handleChange}
                                /><i className="fa fa-search" aria-hidden="true"></i>
                                <button className="btn btn-secundary my-2 my-sm-0"

                                    onClick={this.handleClickSearch}>{t("Search")}</button>
                            </div> : null}
                            <ul className="navbar-nav ml-auto mt-2 mt-lg-0">
                                <li className="nav-item active">
                                    <a className="nav-link" href=""
                                    >{t("Home")} <span className="sr-only">(current)</span></a>
                                </li>
                                <li className="nav-item active">
                                    <a className="nav-link" onClick={this.handleClickStatistics} href=""
                                    >{t("Statistics")} <span className="sr-only">(current)</span></a>
                                </li>
                                {this.props.user_logged() ?
                                    <li className="nav-item active">
                                        <a className="nav-link" onClick={this.handleClickAdmin} href="">Admin <span className="sr-only">(current)</span></a>
                                    </li>
                                    :
                                    <li></li>
                                }
                                <li className="nav-item">
                                    {!this.props.user_logged() ?
                                    <a className="nav-link" onClick={this.handleClickLogin} href="">{t("Login")}</a>
                                    :
                                    <a className="nav-link" onClick={this.props.logout} href="">{t("Logout")}</a>
                                    }
                                </li>
                            </ul>
                            <a
                                className="btn btn-link"
                                data-toggle="tooltip"
                                data-placement="bottom"
                                title="Español"
                                data-locale="es" 
                                onClick={this.props.setLocale}
                            >
                                <img src={"../../static/images/es.svg"} alt="" style={{ width: '20px' }} data-locale="es"/>
                            </a>
                            <a
                                className="btn btn-link"
                                data-toggle="tooltip"
                                data-placement="bottom"
                                title="English"
                                data-locale="en" 
                                onClick={this.props.setLocale}
                            >
                                <img src={"../../static/images/us.svg"} alt="" style={{ width: '20px' }} data-locale="en"/>
                            </a>
                        </div>
                    </div>
                </nav>
            </header>
        )
    }
}
export default withRouter (translate('App')(Header))