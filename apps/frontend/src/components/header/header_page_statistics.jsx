import React from 'react';
import { withRouter } from "react-router-dom";
import { translate } from "react-translate";


class HeaderPageStatistics extends React.Component {
    _isMounted = true;
    constructor(props) {
        super(props);
        // console.log('pagina Header_Stadisticas', props);
        this.state = {
            option: 1
        }

    }

    componentDidMount() {

    }

    componentWillReceiveProps(nextProps) {

    }

    componentWillMount() {

    }

    componentWillUnmount() {

    }

    handleClick(e) {
        e.preventDefault();
        let val_option = event.target.getAttribute('data-value');
        this.setState({ option: val_option }, () => {
            this.props.action(this.state)
        })

    }

    render() {
        let { t } = this.props;
        return (
            <section className="banner-search">
                <div className="container">
                    <ul className="nav nav-tabs">

                        <li className="nav-item">
                            <a
                                className={this.state.option == 1 ? "nav-link active experts" : "nav-link "}
                                href="#"
                                data-value="1"
                                onClick={(e) => {
                                    this.handleClick(e)
                                }}
                            >
                                {t('Data sets by country')}
                                &nbsp;&nbsp;
                            </a>
                        </li>
                        <li className="nav-item">
                            <a
                                className={this.state.option == 3 ? "nav-link active experts" : "nav-link "}
                                href="#"
                                data-value="3"
                                onClick={(e) => {
                                    this.handleClick(e)
                                }}
                            >
                                {t('Data sets by category')}
                                &nbsp;&nbsp;
                            </a>
                        </li>
                        <li className="nav-item">
                            <a
                                className={this.state.option == 2 ? "nav-link active experts" : "nav-link "}
                                href="#"
                                data-value="2"
                                onClick={(e) => {
                                    this.handleClick(e)
                                }}
                            >
                                {t('Data sets by category and country')}
                                &nbsp;&nbsp;
                            </a>
                        </li>
                    </ul >
                </div >
            </section>
        )
    }
}

export default withRouter (translate('App')(HeaderPageStatistics))