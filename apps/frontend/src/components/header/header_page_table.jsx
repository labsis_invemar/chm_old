import React from 'react';
import { withRouter } from "react-router-dom";
import tematicas from "../../../static/js/tematicas/tematicas.json.js"; // traemos las tematicas
import { translate } from "react-translate";


class HeaderPageTable extends React.Component {
    _isMounted = true;
    constructor(props) {
        super(props);
        this.state = {
            search: '',
            temathics: [],
            search_temathic: {},
            tematica: undefined,
            page_current: 1,
            loader_data: true
        }
        this.handleChange = this.handleChange.bind(this);
        this.addEvent = this.addEvent.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.handleClickSearch = this.handleClickSearch.bind(this);
        this.loader_data = this.loader_data.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
        this.setState({
            temathics: tematicas.arr_tematicas_up
        })
    }

    componentWillReceiveProps(nextProps) {
        // console.log('---el header_table--', nextProps);

        this.setState({
            search: nextProps.search != null ? nextProps.search : this.state.search,
            search_temathic: nextProps.search_temathic,
            tematica: nextProps.tematica,
            page_current: nextProps.page_current
        }, () => {
            // console.log('---mira ve', this.state.search_temathic);

        })

    }

    componentWillMount() {
        this.props.emmmiter.on('search', this.addEvent);
        this.props.emmmiter.on('loader_data', this.loader_data);
    }

    componentWillUnmount() {
        this._isMounted = false;
        this.props.emmmiter.removeEventListener('search', this.addEvent);
        this.props.emmmiter.removeEventListener('loader_data', this.loader_data);
    }


    show_number_header(temathic) {
        if (this.state.search_temathic != null && temathic in this.state.search_temathic) {
            return (<span className={this.state.tematica == temathic ? "badge badge-pill badge-primary badge-active" : "badge badge-pill badge-primary " + temathic}>{this.state.search_temathic[temathic]}</span>)
        }
    }

    loader_data(event) {
        // console.log('---------', event);
        this.state.loader_data = true;
    }

    handleChange(event) {

        this.setState({
            search: event.target.value
        })

    }

    addEvent(event) {
        // console.log('-aquii', event);

        if (event.search != null) {
            this.setState({
                search: event.search,
                search_temathic: {}
            })
        }

    }

    handleClickSearch(e) {
        this.props.emmmiter.emit('update_search', this.state.search.trim())
    }

    handleClick(e, tematica) {
        e.preventDefault();
        if (tematica != this.state.tematica) {
            this.setState({
                page_current: 1,
                tematica: tematica
            }, () => {
                this.props.action(this.state)
                this.state.loader_data = false;
            })
        }
    }

    header_create() {
        let { t } = this.props;
        var list_header = [];
        this.state.temathics.map((tematica, i) => {
            if (tematica.show) list_header.push(<li key={i} className="nav-item" style={this.state.loader_data ? {} : { 'cursor': 'not-allowed' }}>
                <a
                    className={this.state.tematica == tematica.class ? "nav-link  active " + tematica.class : "nav-link "}
                    style={this.state.loader_data ? {} : { 'pointerEvents': 'none' }}
                    href="#"
                    data-value={tematica.class}
                    onClick={(e) => {
                        this.handleClick(e, tematica.class)
                    }}>
                    {t(tematica.title)}
                    &nbsp;&nbsp;
                                        {this.show_number_header(tematica.class)}
                </a>
            </li>)
        })
        return list_header;
    }

    render() {
        let { t } = this.props;
        return (
            <section className="banner-search">
                <div className="container">
                    <div className="form-inline my-2 mb-sm-4 my-lg-5 d-block d-sm-block d-md-block d-lg-none d-xl-none">
                        <input
                            className="form-control mr-sm-2"
                            type="searchs"
                            placeholder="Search"
                            aria-label="Search"
                            value={this.state.search}
                            onChange={this.handleChange}
                        />
                        <button className="btn btn-primary btn-block my-2 my-sm-0"
                            // disabled={!this.state.search}
                            onClick={this.handleClickSearch}>{t('Search')}</button>
                    </div>
                    <ul className="nav nav-tabs">
                        {
                            this.header_create()
                        }
                    </ul >
                </div >
            </section>
        )
    }
}

export default withRouter (translate('App')(HeaderPageTable))