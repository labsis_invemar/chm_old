import React from 'react';
import { withRouter } from "react-router-dom";
import menus from "../../../static/js/admins/menu.json.js"; // traemos las tematicas 

class HeaderPageAdmins extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            search: undefined,
            menus: [],
            search_temathic: {},
            menu: undefined,
            page_current: 1
        }

        this.handleClick = this.handleClick.bind(this);
    }

    componentDidMount() {
        this.setState({
            menus: menus.arr_menu_up
        })
    }

    componentWillReceiveProps(nextProps) {
        // console.log('---el header--', nextProps);
        // console.log("entro a nextProps menu: ",nextProps.menu)
        this.setState({
            search: nextProps.search,
            search_temathic: nextProps.search_temathic,
            menu: nextProps.menu,
            page_current: nextProps.page_current
        }, () => {
            // console.log('---mira ve', this.state.search_temathic);
        })

    }



    handleClick(e,menu) {
        e.preventDefault();
        //let menu = e.target.attributes.getNamedItem('data-value').value        
        /*this.props.history.push({
            pathname: '/'+menu,
            search: '',
            state: menu
        });*/
        if("form" == this.props.modulo){
            // console.log("entro en menu")
            this.props.history.push({
            pathname: '/list_admin',
            state: {menu: menu}
            });
        }else if ("list" == this.props.modulo){
            this.setState({
                page_current: 1,
                menu: menu
            }, () => {
                this.props.action(this.state)
            })
        }
       
        //this.props.action(tema)

    }

    render() {
        let only_super_users = ['users'];
        //console.log('user...', this.props.user);
        return (
            <section className="banner-search">
                <div className="container d-none d-sm-block">
                    <ul className="nav nav-tabs">                        
                        { 
                            this.state.menus.map((menu, i) => (
                                only_super_users.includes(menu.title.toLowerCase()) ?
                                    this.props.user.is_superuser ?
                                        <li key={i} className="nav-item">
                                            <a className={this.state.menu == menu.class ? "nav-link  active" : "nav-link "}
                                            href="#" 
                                            data-value={menu.class} 
                                            onClick={(e) => {
                                            this.handleClick(e,menu.class)
                                            }}>{menu.title}</a>
                                        </li>
                                        :
                                        <li></li>
                                :
                                <li key={i} className="nav-item">
                                    <a className={this.state.menu == menu.class ? "nav-link  active" : "nav-link "}
                                    href="#" 
                                    data-value={menu.class} 
                                    onClick={(e) => {
                                    this.handleClick(e,menu.class)
                                    }}>{menu.title}</a>
                                </li>
                            ))
                        }
                    </ul >
                </div >
            </section>
        )
    }
}

export default withRouter(HeaderPageAdmins)