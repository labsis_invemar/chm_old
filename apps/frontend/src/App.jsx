import React from 'react';
import PageBoard from './base/page_board.jsx';
import PageTable from './base/page_table.jsx';
import PageWebService from './base/page_admin_webservice.jsx';
import PageListAdmin from './base/page_list_admin.jsx';
import PageScraping from './base/page_admin_scraping.jsx';
import PageApiReader from './base/page_admin_apireader.jsx';
import PageAdminUsers from './base/page_admin_users.jsx';
import PageUpload from './base/page_admin_upload.jsx';
import PageOai from './base/page_admin_oai.jsx';
import Header from './components/header/header.jsx';
import { Switch, Route , Redirect} from "react-router-dom";
import initial from './providers/initial.jsx';
import { withRouter } from 'react-router-dom';
import Emmiters from './providers/emmiters.jsx';
import Login from "./components/login.jsx";
import PageStatistics from './base/page_statistics.jsx';
import axios from 'axios';
import { TranslatorProvider } from "react-translate";


class App extends React.Component {

    constructor(props) {
        super(props);
        this.emmiters = new Emmiters();
        this.login = this.login.bind(this);
        this.logout = this.logout.bind(this);
        this.user_logged = this.user_logged.bind(this);
        this.user = this.user.bind(this);
        this.setLocale = this.setLocale.bind(this);
        this.getLocale = this.getLocale.bind(this);
        this.locale_is_valid = this.locale_is_valid.bind(this);
        this.state = {
            locale_default:"en"
        }
    }

    componentDidMount() {
        console.log('estoy en modo--- ', initial.mode);

    }

    login(user, token){
        localStorage.setItem('user',  JSON.stringify(user));
        localStorage.setItem('token', token);
        this.props.history.push({
            pathname: '/list_admin',
            state: {
                menu: "webservice"
            }
        });
        
    }

    logout(e=null){
        if(e){
            e.preventDefault();
        }
        if(this.user_logged()){
            axios({
                method: 'post',
                url: initial.url + 'auth/logout/',
                headers: {
                    "Content-Type": "application/json",
                    Authorization: localStorage.getItem('token')
                }
            }).catch((error) => {
                console.error(error);
            });
            localStorage.removeItem('user');
            localStorage.removeItem('token');
            localStorage.clear();
            this.props.history.push({
                pathname: '/'
            });
        }else{
            console.log("User is not logged")
        }
    }

    user_logged(){
        if(localStorage.getItem('user') !== null && localStorage.getItem('token') !== null){
            axios.get(initial.url + 'admins/list_admin/', {
                headers: {
                    Authorization: localStorage.getItem('token')
                }
            }).catch((err) => {
                console.log('Error...', err);
                if(err.response.status === 401){
                    this.logout();
                    return false;
                }
            })
            return true;
        }
        return false;
    }

    user(){
        if(this.user_logged()){
            return JSON.parse(localStorage.getItem('user'));
        }
        return null;
    }

    locale_is_valid(locale){
        if(locale !== undefined && locale !== null){
            return true;
        }
        return false;
    }

    setLocale(e){
        e.preventDefault();
        let locale = e.target.getAttribute('data-locale');
        if(this.locale_is_valid(locale)){
            localStorage.setItem("locale", locale);
            //let current_url  = window.location.hash;
            this.props.history.push({
                pathname: '/'
            });
        }
    }

    getLocale(){
        let locale = localStorage.getItem("locale");
        if (this.locale_is_valid(locale)){
            return locale;
        }
        return this.state.locale_default;
    }

    render() {
        //console.log('user..',this.user());
        //console.log('locale...',this.getLocale());
        return (
            <TranslatorProvider translations={require('./locale/'+this.getLocale()+'.json')}>
                <div>
                    <Header emmmiter={this.emmiters} user_logged={this.user_logged} logout={this.logout} setLocale={this.setLocale}/>
                    { this.user_logged() ?
                        <Switch>
                            <Route path="/" component={PageBoard} exact />
                            <Route path="/table" render={(props) => <PageTable {...props} emmmiter={this.emmiters} />} exact />
                            <Route path="/statistics" render={(props) => <PageStatistics {...props} emmmiter={this.emmiters} />} exact />                        
                            <Route path="/list_admin" render={(props) => <PageListAdmin {...props} user={this.user()} />} logout={this.logout} exact />
                            <Route path="/oai" render={(props) => <PageOai {...props} user={this.user()} />} exact />
                            <Route path="/webservice" render={(props) => <PageWebService {...props} user={this.user()} />} exact />
                            <Route path="/scraping" render={(props) => <PageScraping {...props} user={this.user()} />} exact />
                            <Route path="/upload" render={(props) => <PageUpload {...props} user={this.user()} />} exact />
                            <Route path="/users" render={(props) => <PageAdminUsers {...props} user={this.user()} />} exact />
                            <Route path="/apireader" render={(props)=> <PageApiReader {...props} user={this.user()} />} exact />
                            <Redirect to="/" />
                        </Switch>
                        :
                        <Switch>
                            <Route path="/" component={PageBoard} exact />
                            <Route path="/table" render={(props) => <PageTable {...props} emmmiter={this.emmiters} />} exact />
                            <Route path="/statistics" render={(props) => <PageStatistics {...props} emmmiter={this.emmiters} />} exact />
                            <Route path="/login/" render={(props) => <Login {...props} login={this.login} />} exact />
                            <Redirect to="/login/" />
                        </Switch>
                    }
                </div>
            </TranslatorProvider>
        );
    }
}

export default withRouter(App)