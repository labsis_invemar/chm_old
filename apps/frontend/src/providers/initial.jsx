const initial = {
    url: process.env.API_URL,
    mode: process.env.ENV,
    csrftoken: 'csrftoken',
    xcsrftoken:'X-CSRFToken' ,

};

export default initial;