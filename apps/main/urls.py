from django.conf import settings
from django.views.generic import TemplateView
from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import *

app_name = 'main'

router = DefaultRouter()
router.register(r'category', TematicasViewSet, base_name="data")
router.register(r'statistics', StatisticsViewSet, base_name="data")

urlpatterns = [
   path('', indexView, name='home'),
   path('<categoria>', list_marine, name='marine'),
   path('category_fields/', category_fields, name='category-fields'),
   path('auth/login/', LoginAPI.as_view()),
   path('auth/', include('knox.urls')),
   path('oih/<str:categoria>', list_marine_OIH, name='OIH Metadata Expose'),
   path('oih/findid/<str:categoria>/<str:id_nombre>', findid, name='oih_find_id'),

   #path('<categoria>', Vessels_Info.as_view(), name='vessels-list'),   
   #url(r'^scraping/', webScraping.as_view(), name='scraping'),
   #url(r'^crawl/', views.crawl, name='crawl'),
]

urlpatterns += router.urls

# This is required for static files while in development mode. (DEBUG=TRUE)
# No, not relevant to scrapy or crawling :)
# if settings.DEBUG:
#     urlpatterns += static.static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
#     urlpatterns += static.static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
