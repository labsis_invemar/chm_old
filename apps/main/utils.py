import django_tables2 as tables


def template_form_link(url='record.url',record_name='record.nombre'):
    return '<a target="blank" href="{{%s}}">{{%s}}</a>' % (url,record_name)


def template_table():
    return 'main/django_tables2/bootstrap-responsive.html'


def template_image(record_image='record.imagen'):
    return '<div class="divimage" style="background: url({{%s}})">' % (record_image)