from django.contrib import admin
from .models import (
    Categoria, People, Institution, Document,
    Training, Recurso, Laboratory, Platformgeo, Vessel
)

admin.site.register(Categoria)
admin.site.register(People)
admin.site.register(Institution)
admin.site.register(Document)
admin.site.register(Training)
admin.site.register(Recurso)
admin.site.register(Laboratory)
admin.site.register(Platformgeo)
admin.site.register(Vessel)