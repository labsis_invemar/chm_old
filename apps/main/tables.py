import django_tables2 as tables
from .models import (People,Institution)
from .utils import (
    template_form_link,
    template_table,template_image
    )


class PeopleTable(tables.Table):
    imagen = tables.TemplateColumn(template_image())
    nombre = tables.TemplateColumn(template_form_link())
    institucion = tables.Column()
    ocupacion = tables.Column()

    class Meta:
        template_name = template_table()


class InstitutionTable(tables.Table):
    imagen = tables.TemplateColumn(template_image())
    nombre = tables.TemplateColumn(template_form_link())
    direccion = tables.Column()

    class Meta:
        template_name = template_table()


class DocumentTable(tables.Table):
    imagen = tables.TemplateColumn(template_image())
    nombre = tables.TemplateColumn(template_form_link())
    fecha = tables.Column()
    resumen = tables.Column()

    class Meta:
        template_name = template_table()


class TrainingTable(tables.Table):
    imagen = tables.TemplateColumn(template_image())
    nombre = tables.TemplateColumn(template_form_link())
    tipo = tables.Column()
    email = tables.Column()
    ubicacion = tables.Column()
    fecha = tables.Column()
    institucion = tables.Column()    
    # resumen = tables.Column()

    class Meta:
        template_name = template_table()


class LaboratoryTable(tables.Table):
    imagen = tables.TemplateColumn(template_image())
    nombre = tables.TemplateColumn(template_form_link())
    resumen = tables.Column()

    class Meta:
        template_name = template_table()


class PlatformgeoTable(tables.Table):
    imagen = tables.TemplateColumn(template_image())
    nombre = tables.TemplateColumn(template_form_link())
    resumen = tables.Column()

    class Meta:
        template_name = template_table()


class VesselTable(tables.Table):
    pais = tables.Column()
    nombre = tables.Column()
    institucion = tables.Column()
    descripcion = tables.Column()
    equipi_nav = tables.Column()
    cantidad_trip = tables.Column()

    class Meta:
        template_name = template_table()