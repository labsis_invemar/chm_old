from django.shortcuts import render,HttpResponse
from uuid import uuid4
from urllib.parse import urlparse
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError
from django.core.paginator import Paginator
from django.views.decorators.http import require_POST, require_http_methods
from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from scrapyd_api import ScrapydAPI
from .models import People,Categoria,Institution,Document,Training,Laboratory, Platformgeo, Vessel
from .serializers import *
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.decorators import api_view, permission_classes
from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework import status, viewsets
from rest_framework.decorators import action
from django.views import View
from django_tables2 import RequestConfig
from django_tables2.views import SingleTableMixin
from .tables import PeopleTable, InstitutionTable,DocumentTable,TrainingTable, LaboratoryTable, PlatformgeoTable, VesselTable
from .filters import PeopleFilter, InstitutionFilter, DocumentFilter, TrainingFilter, LaboratoryFilter, PlatformgeoFilter, VesselFilter
from django.db.models import Q
import operator
from functools import reduce
from pprint import pprint
import json
from .helper import *
from knox.models import AuthToken
import hashlib
#from main.utils import URLUtil
#from main.models import ScrapyItem

# connect scrapyd service
scrapyd = ScrapydAPI('http://localhost:6800')

def encrypt_string(hash_string):
    sha_signature = hashlib.sha256(hash_string.encode()).hexdigest()
    return sha_signature


def indexView(request):
    template_name = "main/index.html"
    people = People.objects.all().count()
    institution = Institution.objects.all().count()
    document = Document.objects.all().count()
    training = Training.objects.all().count()
    laboratory = Laboratory.objects.all().count()
    platformgeo = Platformgeo.objects.all().count()
    vessel = Vessel.objects.all().count()
    context = {
        "people": people,
        "institution": institution,
        "document": document,
        "training": training,
        "laboratory": laboratory,
        "platformgeo": platformgeo,
        "vessel": vessel
    }
    return render(request, template_name,context)



def list_marine(request,categoria=None):
    context = {
        'table': None,
        'filter': None
    }
    table, f = None, None
    if categoria == "expert":
        queryset = People.objects.all()
        f = PeopleFilter(request.GET, queryset=queryset)
        table = PeopleTable(f.qs)
    elif categoria == "institution":
        queryset = Institution.objects.all()
        f = InstitutionFilter(request.GET, queryset=queryset)
        table = InstitutionTable(f.qs)
    elif categoria == "document":
        queryset = Document.objects.all()
        f = DocumentFilter(request.GET, queryset=queryset)
        table = DocumentTable(f.qs)
    elif categoria == "training":
        queryset = Training.objects.all()
        f = TrainingFilter(request.GET, queryset=queryset)
        table = TrainingTable(f.qs)
    elif categoria == "laboratory":
        queryset = Laboratory.objects.all()
        f = LaboratoryFilter(request.GET, queryset=queryset)
        table = LaboratoryTable(f.qs)
    elif categoria == "platformgeo":
        queryset = Platformgeo.objects.all()
        f = PlatformgeoFilter(request.GET, queryset=queryset)
        table = PlatformgeoTable(f.qs)
    elif categoria == "vessel":
        queryset = Vessel.objects.all()
        f = VesselFilter(request.GET, queryset=queryset)
        table = VesselTable(f.qs)
    if table:
        RequestConfig(request, paginate={'per_page': 10}).configure(table)
        context['table'] = table
    if f:
        context['filter'] = f
    return render(request, 'main/django_table.html', context)


@api_view(['GET'])
def list_marine_OIH(request,*args, **kwargs):
    base_url= "http://portete.invemar.org.co/chm/api/oih/"
    base_url2="http://portete.invemar.org.co/chm/api/oih/findid/"
    JsonResponse = {
    "@context": {
    "@vocab": "https://schema.org/"
    },
    "name": "Resource collection for https://CHM-LAC/",
    "@id": base_url + kwargs["categoria"]+'#'+encrypt_string(kwargs["categoria"]),
    "@type":[
            "ItemList",
        ],
    "author": {
        "@id": encrypt_string("Instituto de Investigaciones Marinas y Costeras INVEMAR"),
        "@type": "Organization",
        "name": "Instituto de Investigaciones Marinas y Costeras INVEMAR",
        "sameAs": [
            "https://oceanexpert.org/institution/6795"
        ]
    },
    "itemListOrder": "https://schema.org/ItemListUnordered",
    "numberOfItems": 0,
    "itemListElement": []    
    }

    if kwargs["categoria"] == "documents":


        documents = Document.objects.all().values()
        JsonResponse["@type"].append("CreativeWork")
        
        for doc in documents:
            print(doc["shared"])
            if doc["json_ld"] is not None and len(doc["json_ld"])>=1 :
                if doc["shared"] is not False:
                    JsonResponse["numberOfItems"] += 1 
                    JsonResponse["itemListElement"].append(json.loads(doc["json_ld"]))
            else:
                if doc["shared"] is not False:
                    JsonResponse["numberOfItems"] += 1 
                    JsonResponse["itemListElement"].append({
                        "@type": "ListItem",
                        "position": JsonResponse["numberOfItems"],
                        "item":
                            {
                                "@id": base_url2 + kwargs["categoria"]+'/'+encrypt_string(doc["id_nombre"]),
                                "@context": {
                                    "@vocab": "https://schema.org/"
                                },
                                "@type": "CreativeWork",
                                "name": doc["nombre"],
                                "description": doc["resumen"],
                                "url": doc["url"],
                                "thumbnailUrl": doc["imagen"]
                            }
                        })  
    elif kwargs["categoria"] == "institution":
        queryset = Institution.objects.all().values()
        JsonResponse["@type"].append("Organization")
        for inst in queryset:
            JsonResponse["numberOfItems"] += 1 
            JsonResponse["itemListElement"].append({
                "@type": "ListItem",
                "position": JsonResponse["numberOfItems"],
                "item":
                    {
                        "@id": base_url2 + kwargs["categoria"]+'/'+encrypt_string(inst["id_nombre"]),
                        "@context": {
                            "@vocab": "https://schema.org/"
                        },
                        "@type": "Organization",
                        "name": inst["nombre"],
                        "url": inst["url"],
                        "address": {
                            "@type": "PostalAddress",
                            "addressLocality": inst["pais"],
                            # "postalCode": "F-75002",
                            "streetAddress": inst["direccion"]
                        },
                        # "datePublished": inst["date_published"],
                        # "dateModified": inst["date_modified"],
                        # "image": inst["imagen"],
                        "thumbnailUrl": inst["imagen"]
                    }
                })
    elif kwargs["categoria"] == "expert":
        queryset = People.objects.all().values()
        JsonResponse["@type"].append("Person")
        for pep in queryset:
            JsonResponse["numberOfItems"] += 1 
            JsonResponse["itemListElement"].append({
                "@type": "ListItem",
                "position": JsonResponse["numberOfItems"],
                "item":
                    {
                        "@id": base_url2 + kwargs["categoria"]+'/'+encrypt_string(pep["id_nombre"]),
                        "@context": {
                            "@vocab": "https://schema.org/"
                        },
                        "@type": "Person",
                        "name": pep["nombre"],
                        "url": pep["url"],
                        "jobTitle": pep["ocupacion"],
                        "affiliation":pep["institucion"],
                        "nationality": [
                            {
                            "@type": "Country",
                            "name": pep["pais"]
                            },
                        ],
                        "knowsAbout":pep["aptitudes"],
                        "thumbnailUrl": pep["imagen"]
                        
                    }
                })
    elif kwargs["categoria"] == "training":
        queryset = Training.objects.all().values()
        JsonResponse["@type"].append("LearningResource")
        JsonResponse["@type"].append("CreativeWork")
        for inst in queryset:
            JsonResponse["numberOfItems"] += 1 
            JsonResponse["itemListElement"].append({
                "@type": "ListItem",
                "position": JsonResponse["numberOfItems"],
                "item":
                    {
                        "@id": base_url2 + kwargs["categoria"]+'/'+encrypt_string(inst["id_nombre"]),
                        "@context": {
                            "@vocab": "https://schema.org/"
                        },
                        "@type": "Course",
                        "name": inst["nombre"],
                        "email": inst["email"],
                        "url": inst["url"],
                        "contentLocation": inst["pais"],
                        "educationalCredentialAwarded": inst["tipo"],
                        "teaches": inst["resumen"],
                        "author": {
                            "@type": "Organization",
                            "name": inst["institucion"]
                        },
                    }
                })    
    # elif kwargs["categoria"] == "laboratory":
    #     queryset = Laboratory.objects.all().values()
    #     JsonResponse["@type"].append("Organization")
    #     for inst in queryset:
    #         JsonResponse["numberOfItems"] += 1 
    #         JsonResponse["itemListElement"].append({
    #             "@type": "ListItem",
    #             "position": JsonResponse["numberOfItems"],
    #             "item":
    #                 {
    #                     "@context": {
    #                         "@vocab": "https://schema.org/"
    #                     },
    #                     "@type": "Organization",
    #                     "name": inst["nombre"],
    #                     "url": inst["url"],
    #                     "address": {
    #                         "@type": "PostalAddress",
    #                         "addressLocality": inst["pais"],
    #                         # "postalCode": "F-75002",
    #                         "streetAddress": inst["direccion"]
    #                     },
    #                     # "datePublished": inst["date_published"],
    #                     # "dateModified": inst["date_modified"],
    #                     # "image": inst["imagen"],
    #                     "thumbnailUrl": inst["imagen"]
    #                 }
    #             })
    elif kwargs["categoria"] == "platformgeo":
        queryset = Platformgeo.objects.all().values()
        JsonResponse["@type"].append("Map")
        for geo in queryset:
            JsonResponse["numberOfItems"] += 1 
            JsonResponse["itemListElement"].append({
                "@type": "ListItem",
                "position": JsonResponse["numberOfItems"],
                "item":
                    {
                        "@id": base_url2 + kwargs["categoria"]+'/'+encrypt_string(geo["id_nombre"]),
                        "@context": {
                            "@vocab": "https://schema.org/"
                        },
                        "@type": "Map",
                        "name": geo["nombre"],
                        "url": geo["url"],
                        "contentLocation": geo["pais"],
                        "description": geo["resumen"],                        
                        # "datePublished": geo["date_published"],
                        # "dateModified": geo["date_modified"],
                        "image": geo["imagen"],
                        "keyword":[ geo["category"]],
                        "source":geo["source"],"source":geo["source"],
                        "spatialCoverage":{
                        "@type":"Place",
                        "geo":{
                        "@type":"GeoShape",
                        "boundingBox":geo["bounding_box"]
                        }
                        }
                    }
                })
    elif kwargs["categoria"] == "vessel":
        queryset = Vessel.objects.all().values()
        JsonResponse["@type"].append("Vehicle")
        for ves in queryset:
            JsonResponse["numberOfItems"] += 1 
            JsonResponse["itemListElement"].append({
                "@type": "ListItem",
                "position": JsonResponse["numberOfItems"],
                "item":
                    {
                        "@id": base_url2 + kwargs["categoria"]+'/'+encrypt_string(ves["id_nombre"]),
                        "@context": {
                            "@vocab": "https://schema.org/"
                        },
                        "@type": "Vehicle",
                        "name": ves["nombre"],
                        "url": ves["url"],
                        "description": ves["descripcion"],                        
                        "vehicleConfiguration": ves["equipi_nav"],
                        "category": ves["categoria"],
                        "vehicleSpecialUsage": ves["actividad"],
                        "additionalProperty":[
                            {
                                "@id": "Crew",
                                "@type": "PropertyValue",
                                "propertyID": "https://en.wikipedia.org/wiki/Crew",
                                "description": ves["cantidad_trip"],
                            },
                            {
                                "@id": "Length_overall",
                                "@type": "PropertyValue",
                                "propertyID": "https://en.wikipedia.org/wiki/Length_overall",
                                "description": ves["envergadura"],
                            },
                            {
                                "@id": "Operation_status",
                                "@type": "PropertyValue",
                                "propertyID": "Operation_status",
                                "description": ves["estado"],
                            }
                        ],
                    }
                })
    return Response(JsonResponse)


@api_view(['GET'])
def findid(request,*args, **kwargs):
    print("-----------------------------------------------",kwargs["categoria"])

    id_nombre = kwargs["id_nombre"]
    JsonResponse = {
    "@context": {
    "@vocab": "https://schema.org/"
    },
    "name": "Resource collection for https://CHM-LAC/",
    "@id":encrypt_string(id_nombre),
    "@type":[
            "ItemList",
        ],
    "author": {
        "@id": encrypt_string("Instituto de Investigaciones Marinas y Costeras INVEMAR"),
        "@type": "Organization",
        "name": "Instituto de Investigaciones Marinas y Costeras INVEMAR",
        "sameAs": [
            "https://oceanexpert.org/institution/6795"
        ]
    },
    "itemListOrder": "https://schema.org/ItemListUnordered",
    "numberOfItems": 0,
    "itemListElement": []    
    }

    if kwargs["categoria"] == "platformgeo":
        plfgeo = Platformgeo.objects.all().values()
        aux = {}
        for x in plfgeo:
            if encrypt_string(x["id_nombre"]) == id_nombre:
                aux = x
                break
        JsonResponse["@type"].append("Map")
        JsonResponse["numberOfItems"] += 1 
        JsonResponse["itemListElement"].append({
            "@type": "ListItem",
            "position": JsonResponse["numberOfItems"],
            "item":
                {
                    "@id": encrypt_string(aux["id_nombre"]),
                    "@context": {
                        "@vocab": "https://schema.org/"
                    },
                    "@type": "Map",
                    "name": aux["nombre"],
                    "url": aux["url"],
                    "contentLocation": aux["pais"],
                    "description": aux["resumen"],                        
                    # "datePublished": geo["date_published"],
                    # "dateModified": geo["date_modified"],
                    "image": aux["imagen"],
                    "keyword":[ aux["category"]],
                }
            })
    
    elif kwargs["categoria"] == "documents":
        documents = Document.objects.all().values()
        aux = {}
        for x in documents:
            if encrypt_string(x["id_nombre"]) == id_nombre:
                aux = x
                break
        JsonResponse["@type"].append("CreativeWork")
        # if aux["json_ld"] is not None and len(aux["json_ld"])>=1 :
        #     JsonResponse["numberOfItems"] += 1 
        #     JsonResponse["itemListElement"].append(json.loads(aux["json_ld"]))
        # else:
        JsonResponse["numberOfItems"] += 1 
        JsonResponse["itemListElement"].append({
            "@type": "ListItem",
            "position": JsonResponse["numberOfItems"],
            "item":
                {
                    "@id":encrypt_string(aux["id_nombre"]),
                    "@context": {
                        "@vocab": "https://schema.org/"
                    },
                    "@type": "CreativeWork",
                    "name": aux["nombre"],
                    "description": aux["resumen"],
                    "url": aux["url"],
                    "thumbnailUrl": aux["imagen"]
                }
            }) 

    elif kwargs["categoria"] == "institution":
        queryset = Institution.objects.all().values()
        aux = {}
        for x in queryset:
            if encrypt_string(x["id_nombre"]) == id_nombre:
                aux = x
                break
        JsonResponse["@type"].append("Organization")
        JsonResponse["numberOfItems"] += 1 
        JsonResponse["itemListElement"].append({
            "@type": "ListItem",
            "position": JsonResponse["numberOfItems"],
            "item":
                {
                    "@id":encrypt_string(aux["id_nombre"]),
                    "@context": {
                        "@vocab": "https://schema.org/"
                    },
                    "@type": "Organization",
                    "name": aux["nombre"],
                    "url": aux["url"],
                    "address": {
                        "@type": "PostalAddress",
                        "addressLocality": aux["pais"],
                        # "postalCode": "F-75002",
                        "streetAddress": aux["direccion"]
                    },
                    # "datePublished": aux["date_published"],
                    # "dateModified": aux["date_modified"],
                    # "image": aux["imagen"],
                    "thumbnailUrl": aux["imagen"]
                }
            })

    elif kwargs["categoria"] == "expert":
        queryset = People.objects.all().values()
        aux = {}
        for x in queryset:
            if encrypt_string(x["id_nombre"]) == id_nombre:
                aux = x
                break        
        JsonResponse["@type"].append("Person")
        JsonResponse["numberOfItems"] += 1 
        JsonResponse["itemListElement"].append({
            "@type": "ListItem",
            "position": JsonResponse["numberOfItems"],
            "item":
                {
                    "@id":encrypt_string(aux["id_nombre"]),
                    "@context": {
                        "@vocab": "https://schema.org/"
                    },
                    "@type": "Person",
                    "name": aux["nombre"],
                    "url": aux["url"],
                    "jobTitle": aux["ocupacion"],
                    "affiliation":aux["institucion"],
                    "nationality": [
                        {
                        "@type": "Country",
                        "name": aux["pais"]
                        },
                    ],
                    "knowsAbout":aux["aptitudes"],
                    "thumbnailUrl": aux["imagen"]
                    
                }
            })

    elif kwargs["categoria"] == "vessel":
        queryset = Vessel.objects.all().values()
        aux = {}
        for x in queryset:
            if encrypt_string(x["id_nombre"]) == id_nombre:
                aux = x
                break        
        JsonResponse["@type"].append("Vehicle")
        JsonResponse["numberOfItems"] += 1 
        JsonResponse["itemListElement"].append({
            "@type": "ListItem",
            "position": JsonResponse["numberOfItems"],
            "item":
                {
                    "@id":encrypt_string(aux["id_nombre"]),
                    "@context": {
                        "@vocab": "https://schema.org/"
                    },
                    "@type": "Vehicle",
                    "name": aux["nombre"],
                    "url": aux["url"],
                    "description": aux["descripcion"],                        
                    "vehicleConfiguration": aux["equipi_nav"],
                    "category": aux["categoria"],
                    "vehicleSpecialUsage":aux["actividad"],
                    "additionalProperty":[
                        {
                            "@id": "Crew",
                            "@type": "PropertyValue",
                            "propertyID": "https://en.wikipedia.org/wiki/Crew",
                            "description": aux["cantidad_trip"],
                        },
                        {
                            "@id": "Length_overall",
                            "@type": "PropertyValue",
                            "propertyID": "https://en.wikipedia.org/wiki/Length_overall",
                            "description": aux["envergadura"],
                        },
                        {
                            "@id": "Operation_status",
                            "@type": "PropertyValue",
                            "propertyID": "Operation_status",
                            "description": aux["estado"],
                        }
                    ],
                }
            })

    elif kwargs["categoria"] == "training":
        queryset = Training.objects.all().values()
        aux = {}
        for x in queryset:
            if encrypt_string(x["id_nombre"]) == id_nombre:
                aux = x
                break 
        JsonResponse["@type"].append("LearningResource")
        JsonResponse["@type"].append("CreativeWork")
        JsonResponse["numberOfItems"] += 1 
        JsonResponse["itemListElement"].append({
            "@type": "ListItem",
            "position": JsonResponse["numberOfItems"],
            "item":
                {
                    "@id":encrypt_string(aux["id_nombre"]),
                    "@context": {
                        "@vocab": "https://schema.org/"
                    },
                    "@type": "Course",
                    "name": aux["nombre"],
                    "email": aux["email"],
                    "url": aux["url"],
                    "contentLocation": aux["pais"],
                    "educationalCredentialAwarded": aux["tipo"],
                    "teaches": aux["resumen"],
                    "author": {
                        "@type": "Organization",
                        "name": aux["institucion"]
                    },
                }
            })  

    return Response(JsonResponse)



@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def category_fields(request):
    categoria = request.query_params.get('categoria')
    if categoria.lower() == 'experts':
        return Response({'fields':get_model_fields_select(People)},status=200)
    elif categoria.lower() == 'documents':
        return Response({'fields': get_model_fields_select(Document)},status=200)
    elif categoria.lower() == 'training':
        return Response({'fields': get_model_fields_select(Training)},status=200)
    elif categoria.lower() == 'laboratories':
        return Response({'fields': get_model_fields_select(Laboratory)},status=200)
    elif categoria.lower() == 'institutions':
        return Response({'fields': get_model_fields_select(Institution)},status=200)
    elif categoria.lower() == 'geo':
        return Response({'fields': get_model_fields_select(Platformgeo)},status=200)
    elif categoria.lower() == 'vess':
        return Response({'fields': get_model_fields_select(Vessel)},status=200)
    else:
        return Response({'fields': []},status=200)

def get_model_fields_select(model):    
        fields = []
        for field in model._meta.get_fields():
            if field._verbose_name != None:
                fields.append({field.name:field._verbose_name,'key':field.name})
        #print("fields: ",fields)
        return fields


class TematicasViewSet(viewsets.ViewSet):

    @action(detail=True, methods=['get'])
    def counter(self,request,pk):
        # data = json.loads(request.body.decode('utf-8'))
        # categoria = data.get('categoria')    
        # categoria = request.query_params.get('categoria')
        categoria = pk
        if categoria.lower() == 'experts':        
            serializer = self.get_serialize_data(People,ExpertsSerializer)
            return Response({'data':serializer.data,'amount':len(serializer.data)}, status=200)
        elif categoria.lower() == 'documents':        
            serializer = self.get_serialize_data(Document,DocumentsSerializer)
            return Response({'data':serializer.data,'amount':len(serializer.data)}, status=200)
        elif categoria.lower() == 'training':            
            serializer = self.get_serialize_data(Training,TrainingSerializer)
            return Response({'data':serializer.data,'amount':len(serializer.data)}, status=200)
        elif categoria.lower() == 'laboratories':        
            serializer = self.get_serialize_data(Laboratory,LaboratorySerializer)
            return Response({'data':serializer.data,'amount':len(serializer.data)}, status=200)
        elif categoria.lower() == 'institutions':
            serializer = self.get_serialize_data(Institution,InstitutionsSerializer)
            return Response({'data':serializer.data,'amount':len(serializer.data)}, status=200)
        elif categoria.lower() == 'geo':
            serializer = self.get_serialize_data(Platformgeo,PlatformgeoSerializer)
            return Response({'data':serializer.data,'amount':len(serializer.data)}, status=200)
        elif categoria.lower() == 'vess':
            serializer = self.get_serialize_data(Vessel,VesselSerializer)
            return Response({'data':serializer.data,'amount':len(serializer.data)}, status=200)
    
    @action(detail=True, methods=['get'])
    def data(self,request,pk):
        categoria = pk
        search = request.query_params.get('search') if request.query_params.get('search') != 'undefined' else None
        page = request.query_params.get('page') if request.query_params.get('page') else 1
        is_pagination = request.query_params.get('is_pagination')
        paginate = request.query_params.get('paginate') if request.query_params.get('paginate') is not None else 10
        facets_data = json.loads(request.query_params.get('facets_data')) if request.query_params.get('facets_data') is not None else None
        count_temathic = self.get_count_temathic_search(search,is_pagination)
        pagination_num = 0
        model_ = None
        data_ = None
        serializer = None
        title = None
        try:
            paginate = int(paginate)
        except Exception as ex:
            paginate = 10

        try:
            page = int(page)
        except Exception as ex:
            page = 1


        if categoria =='all':
            value_ = self.get_temathic_(search,count_temathic)
            if value_:
                categoria = value_
            else:
                categoria = 'experts'

        if categoria.lower() == 'experts':  
            model_ = People   
            title = 'Experts'       
            data_ = self.get_query_data(People,search,facets_data)
            pagination_num = self.get_paginate_num(People,paginate,search,data_)
            serializer = ExpertsSerializer(self.get_paginate_data(data_,paginate,page,pagination_num), many=True)
        elif categoria.lower() == 'documents':
            model_ = Document
            title = 'Documents'
            data_ = self.get_query_data(Document,search,facets_data) 
            pagination_num = self.get_paginate_num(Document,paginate,search,data_)     
            serializer = DocumentsSerializer(self.get_paginate_data(data_,paginate,page,pagination_num), many=True)
        elif categoria.lower() == 'training':
            model_ = Training
            title = 'Training'
            data_ = self.get_query_data(Training,search,facets_data) 
            pagination_num = self.get_paginate_num(Training,paginate,search,data_)     
            serializer = TrainingSerializer(self.get_paginate_data(data_,paginate,page,pagination_num), many=True)
        elif categoria.lower() == 'laboratories':
            model_ = Laboratory
            title = 'Laboratories'
            data_ = self.get_query_data(Laboratory,search,facets_data) 
            pagination_num = self.get_paginate_num(Laboratory,paginate,search,data_)     
            serializer = LaboratorySerializer(self.get_paginate_data(data_,paginate,page,pagination_num), many=True)
        elif categoria.lower() == 'institutions':
            model_ = Institution
            title = 'Institutions'
            data_ = self.get_query_data(Institution,search,facets_data) 
            pagination_num = self.get_paginate_num(Institution,paginate,search,data_)     
            serializer = InstitutionsSerializer(self.get_paginate_data(data_,paginate,page,pagination_num), many=True)
        elif categoria.lower() == 'geo':
            model_ = Platformgeo
            title = 'Geospatial information'
            data_ = self.get_query_data(Platformgeo,search,facets_data) 
            pagination_num = self.get_paginate_num(Platformgeo,paginate,search,data_)     
            serializer = PlatformgeoSerializer(self.get_paginate_data(data_,paginate,page,pagination_num), many=True)
        elif categoria.lower() == 'vess':
            model_ = Vessel
            title = 'Vessels'
            data_ = self.get_query_data(Vessel,search,facets_data) 
            pagination_num = self.get_paginate_num(Vessel,paginate,search,data_)     
            serializer = VesselSerializer(self.get_paginate_data(data_,paginate,page,pagination_num), many=True)

        return Response(
                {'data':serializer.data, 
                'fields':self.get_model_fields(model_), 
                'pagination_num':pagination_num, 
                'categoria':categoria,
                'page_current':int(page) if page is not None and int(page) <= pagination_num else 1,
                'page_next':self.get_paginate_nums(page,pagination_num,'next',data_),
                'page_previous':self.get_paginate_nums(page,pagination_num,'previous',data_),  
                'search': search if search else None,
                'search_temathic':count_temathic,
                'is_pagination':is_pagination,
                'title': title,
                'between_data':self.get_results_nums(pagination_num,int(page),int(paginate),data_),
                'facets_data':facets_data
                }, 
                status=200)

        
    """ if categoria=='all':
            value_ = self.get_temathic_(search,count_temathic)
            if value_:
                categoria = value_
            else:
                categoria = 'experts'

        if categoria.lower() == 'experts':
            experts = self.get_query_data(People,search,facets_data)
            pagination_num = self.get_paginate_num(People,paginate,search,experts)
            serializer = ExpertsSerializer(self.get_paginate_data(experts,paginate,page,pagination_num), many=True)  
            return Response(
                {'data':serializer.data, 
                'fields':self.get_model_fields(People), 
                'pagination_num':pagination_num, 
                'categoria':categoria,
                'page_current':int(page) if page is not None and int(page) <= pagination_num else 1,
                'page_next':self.get_paginate_nums(page,pagination_num,'next',experts),
                'page_previous':self.get_paginate_nums(page,pagination_num,'previous',experts),  
                'search': search if search else None,
                'search_temathic':count_temathic,
                'is_pagination':is_pagination,
                'title':'Experts',
                'between_data':self.get_results_nums(pagination_num,int(page),int(paginate),experts),
                'facets_data':facets_data
                }, 
                status=200)
    
        elif categoria.lower() == 'documents':
            document = self.get_query_data(Document,search,facets_data) 
            pagination_num = self.get_paginate_num(Document,paginate,search,document)     
            serializer = DocumentsSerializer(self.get_paginate_data(document,paginate,page,pagination_num), many=True) 
            return Response(
                {'data':serializer.data, 
                'fields':self.get_model_fields(Document), 
                'pagination_num':pagination_num, 
                'categoria':categoria,
                'page_current':int(page) if page is not None and int(page) <= pagination_num else 1,
                'page_next':self.get_paginate_nums(page,pagination_num,'next',document),
                'page_previous':self.get_paginate_nums(page,pagination_num,'previous',document), 
                'search': search if search else None,
                'search_temathic':count_temathic,
                'is_pagination':is_pagination,
                'title':'Documents',
                'between_data':self.get_results_nums(pagination_num,int(page),int(paginate),document),
                'facets_data':facets_data
                }, 
                status=200)        
    
        elif categoria.lower() == 'training':        
            training = self.get_query_data(Training,search,facets_data)            
            pagination_num = self.get_paginate_num(Training,paginate,search,training)    
            serializer = TrainingSerializer(self.get_paginate_data(training,paginate,page,pagination_num), many=True)              
            return Response(
                {'data':serializer.data, 
                'fields':self.get_model_fields(Training), 
                'pagination_num':pagination_num, 
                'categoria':categoria,
                'page_current':int(page) if page is not None and int(page) <= pagination_num else 1,
                'page_next':self.get_paginate_nums(page,pagination_num,'next',training),
                'page_previous':self.get_paginate_nums(page,pagination_num,'previous',training), 
                'search': search if search else None,
                'search_temathic':count_temathic,
                'is_pagination':is_pagination,
                'title':'Training',
                'between_data':self.get_results_nums(pagination_num,int(page),int(paginate),training),
                'facets_data':facets_data
                }, 
                status=200)
    
        elif categoria.lower() == 'laboratories':
            laboratory = self.get_query_data(Laboratory,search,facets_data)            
            pagination_num = self.get_paginate_num(Laboratory,paginate,search,laboratory)
            serializer = LaboratorySerializer(self.get_paginate_data(laboratory,paginate,page,pagination_num), many=True)         
            return Response(
                {'data':serializer.data, 
                'fields':self.get_model_fields(Laboratory), 
                'pagination_num':pagination_num, 
                'categoria':categoria,
                'page_current':int(page) if page is not None and int(page) <= pagination_num else 1,
                'page_next':self.get_paginate_nums(page,pagination_num,'next',laboratory),
                'page_previous':self.get_paginate_nums(page,pagination_num,'previous',laboratory), 
                'search': search if search else None,
                'search_temathic':count_temathic,
                'is_pagination':is_pagination,
                'title':'Laboratories',
                'between_data':self.get_results_nums(pagination_num,int(page),int(paginate),laboratory),
                'facets_data':facets_data
                }, 
                status=200)
    
        elif categoria.lower() == 'institutions':
            institution = self.get_query_data(Institution,search,facets_data)            
            pagination_num = self.get_paginate_num(Institution,paginate,search,institution)
            serializer = InstitutionsSerializer(self.get_paginate_data(institution,paginate,page,pagination_num), many=True)        
            return Response(
                {'data':serializer.data, 
                'fields':self.get_model_fields(Institution), 
                'pagination_num':pagination_num, 
                'categoria':categoria,
                'page_current':int(page) if page is not None and int(page) <= pagination_num else 1,
                'page_next':self.get_paginate_nums(page,pagination_num,'next',institution),
                'page_previous':self.get_paginate_nums(page,pagination_num,'previous',institution), 
                'search': search if search else None,
                'search_temathic':count_temathic,
                'is_pagination':is_pagination,
                'title':'Institutions',
                'between_data':self.get_results_nums(pagination_num,int(page),int(paginate),institution),
                'facets_data':facets_data
                }, 
                status=200)
        
        elif categoria.lower() == 'geo':
            geo = self.get_query_data(Platformgeo,search,facets_data)            
            pagination_num = self.get_paginate_num(Platformgeo,paginate,search,geo) 
            serializer = PlatformgeoSerializer(self.get_paginate_data(geo,paginate,page,pagination_num), many=True)        
            return Response(
                {'data':serializer.data, 
                'fields':self.get_model_fields(Platformgeo), 
                'pagination_num':pagination_num,
                'categoria':categoria,
                'page_current':int(page) if page is not None and int(page) <= pagination_num else 1,
                'page_next':self.get_paginate_nums(page,pagination_num,'next',geo),
                'page_previous':self.get_paginate_nums(page,pagination_num,'previous',geo), 
                'search': search if search else None,
                'search_temathic':count_temathic,
                'is_pagination':is_pagination,
                'title':'Geospatial information',
                'between_data':self.get_results_nums(pagination_num,int(page),int(paginate),geo),
                'facets_data':facets_data
                }, 
                status=200)
    
        elif categoria.lower() == 'vess':
            vess = self.get_query_data(Vessel,search,facets_data)            
            pagination_num = self.get_paginate_num(Vessel,paginate,search,vess)
            serializer = VesselSerializer(self.get_paginate_data(vess,paginate,page,pagination_num), many=True)           
            return Response(
                {'data':serializer.data, 
                'fields':self.get_model_fields(Vessel), 
                'pagination_num':pagination_num, 
                'categoria':categoria,
                'page_current':int(page) if page is not None and int(page) <= pagination_num else 1,
                'page_next':self.get_paginate_nums(page,pagination_num,'next',vess),
                'page_previous':self.get_paginate_nums(page,pagination_num,'previous',vess), 
                'search': search if search else None,
                'search_temathic':count_temathic,
                'is_pagination':is_pagination,
                'title':'Vessels',
                'between_data':self.get_results_nums(pagination_num,int(page),int(paginate),vess),
                'facets_data':facets_data
                }, 
                status=200)"""

    @action(detail=True, methods=['get'])
    def facets(self,request,pk):
        categoria = pk
        search = request.query_params.get('search') if request.query_params.get('search') != 'undefined' else None
        facets_data=[]
        options_=[]
        model_=None
        print('---------------',search)

        if categoria.lower() == 'experts':            
            model_ = People
        elif categoria.lower() == 'documents':
            model_ = Document
        elif categoria.lower() == 'training':
            model_ = Training
        elif categoria.lower() == 'laboratories':
            model_ = Laboratory
        elif categoria.lower() == 'institutions':
            model_ = Institution
        elif categoria.lower() == 'geo':
            model_ = Platformgeo
        elif categoria.lower() == 'vess':
            model_ = Vessel
        
        try:
            fields = self.get_model_fields(model_)
        except Exception as ex:
            return Response({'data':facets_data}, status=200)

        facets_data = self.get_data_facets(fields,model_,search)['data']           
        return Response({'data':facets_data,'categoria':categoria,'search':search}, status=200)

    def get_data_facets(self,fields,model_,search):
        facets_data = []
        query = self.create_query(model_,search)
        ignore_fields=[
            'imagen',
            'resumen',
            'nombre',
            'json_ld',
            'url'
        ]
        if len(fields)>0:
            for field in fields:
                obj_facet={} 
                options_ = []
                if field[field['key']] and field['key'].lower() not in ignore_fields and len(field[field['key']].strip())>0:                        
                    obj_facet['key_field'] = field['key']
                    obj_facet['name']=field[field['key']]
                    if search is not None:                        
                        print('---->Query')
                        print('---->valor select',field['key'])
                        print(query)
                        print(reduce(operator.or_, query))
                        # return model.objects.filter(reduce(operator.or_, query))
                        try:
                            options_ = model_.objects.order_by().distinct().values_list(field['key'], flat=True).filter(reduce(operator.or_, query)).exclude(**{field['key']+'__isnull':True}).exclude(**{field['key']+'__exact':''})
                        except Exception as e:
                            print('error consulta--->',e)                        
                    else:
                        try:
                            options_ = model_.objects.order_by().values_list(field['key'], flat=True).distinct().exclude(**{field['key']+'__isnull':True}).exclude(**{field['key']+'__exact':''})
                        except Exception as e:
                            print('error consulta 2 --->',e)
                    try:
                        obj_facet['options'] = self.fix_options(options_,field['key'])
                    except Exception as e:
                        print('---error obj-----',e)
                        # obj_facet['error'] = True
                        return self.get_data_facets(fields,model_,search)                    
                    facets_data.append(obj_facet)
        return {'data':facets_data}
    
    def fix_options(self,options,key):
        options = set(options)
        if key=="fecha":
            options = set([x.split("-")[0].strip() for x in options])
        if key=="pais":
            options =set( [x.split(",")[0].strip() for x in options])
        if key=="source":
            options = set([x.split("from")[0].strip() for x in options])
        else:
            options=options
        options_f = [{'value':x,'label':x, 'title':x} for x in options]

        options_f.sort(key=lambda x: x['value'])

        return options_f
    
    def get_serialize_data(self,model,serializers):
        data = model.objects.all()
        serializer = serializers(data, many=True)
        return serializer

    def get_model_fields(self,model):    
        
        fields = []
        for field in model._meta.get_fields():
            if field._verbose_name is not None:
                fields.append({field.name:field._verbose_name,'key':field.name})
        return fields
            
    def get_paginate_num(self,model,paginate,search,data):
            
        pagination_num = len(data)/paginate    
        
        number_dec = str(pagination_num-int(pagination_num))[2:]    
        if int(number_dec) == 0:
            return int(pagination_num)
        elif isinstance(pagination_num, int):
            return int(pagination_num)
        else:
            return int(pagination_num) +1

    def get_paginate_data(self,data,paginate,page,pagination_num):
        
        paginator = Paginator(data, paginate)    
        if page is not None and data is not None and int(page) <= int(pagination_num):        
            return paginator.page(int(page))
        else:
            return paginator.page(1)

    def get_temathic_(self,search,count_temathic):
        
        if search is not None and len(search)>0:     
            for temathic, value in count_temathic.items():
                if(value>0):
                    return temathic

    def get_query_data(self,model,search,facets=None):
        
        if search and bool(facets) is False:
            query = self.create_query(model,search)
            return model.objects.filter(reduce(operator.or_, query))
        elif search and facets:
            query_search = self.create_query(model,search)
            q_objects = Q()
            for key,value in facets.items():
                key_f = key+'__icontains'
                print(key)
                query = reduce(operator.or_, (Q(**{key_f: facet['value'].strip()}) for facet in value))
                q_objects.add(query, Q.AND)
            return model.objects.filter(reduce(operator.or_, query_search),q_objects)
        elif search is None and bool(facets) is True:
            # pprint(facets)
            q_objects = Q()
            for key,value in facets.items():
                key_f = key+'__icontains'
                query = reduce(operator.or_, (Q(**{key_f: facet['value']}) for facet in value))
                q_objects.add(query, Q.AND)                      
            return model.objects.filter(q_objects)            
        else:
            return model.objects.all()
        
    def create_query(self,model,search):
        
        query = []
        for field in self.get_model_fields(model):
            if field[field['key']] and field['key'] != 'imagen' and field['key'].lower() != 'url' and len(field[field['key']].strip())>0:
                key = field['key']+'__icontains'
                query.append(Q(**{key: search}))            
        return query

    def get_count_temathic_search(self,search,is_pagination):
        
        if search and is_pagination is None:
            print('entre-------')
            people = self.get_query_data(People,search)
            document = self.get_query_data(Document,search)
            training = self.get_query_data(Training,search)
            laboratory = self.get_query_data(Laboratory,search)
            institution = self.get_query_data(Institution,search)
            platformgeo = self.get_query_data(Platformgeo,search)
            vess = self.get_query_data(Vessel,search)

            return {
            'experts':len(people),
            'documents':len(document),
            'training':len(training),
            'laboratories':len(laboratory),
            'institutions':len(institution),
            'geo':len(platformgeo),
            'vess':len(vess)
            }
        else:
            return None

    def get_paginate_nums(self,page,paginate_num,option,data):
        next_ = None
        last_ = None
        if len(data) == 0:
            return 0
        if (option == "next" and int(page) <= int(paginate_num)):
            if (int(page)+1) <= int(paginate_num):
                return  (int(page)+1)
            else:
                diff = (int(paginate_num)-int(page))
                return (int(paginate_num)-diff)
        elif (option == "next" and int(page) > int(paginate_num)):
            page = 1
            return self.get_paginate_nums(1,paginate_num,option,data)

        elif (option == "previous" and int(page) <= int(paginate_num)):
            if int(page) == 1 or int(page) == 2:
                return 1            
            else:
                return (int(page)-int(1))
        elif(option == "previous" and int(page) > int(paginate_num)):                
            return self.get_paginate_nums(1,paginate_num,option,data)
        else:
            return 0

    def get_results_nums(self,pagination_num,page,paginate,data):
        c = len(data)/paginate
        number_dec = 0
        number_ent = 0
        inicio =0
        final=0
        if "." in str(c): # quick check if it is decimal
            number_dec = int(str(c).split(".")[1])
            number_ent = int(str(c).split(".")[0])
        
            if number_ent>0 and number_dec>0:
                if page<= number_ent:
                    if page==1:
                        inicio = 1
                        final = page * paginate
                    else:
                        inicio = (page-1)*paginate
                        final = page * paginate

                elif page>number_ent:
                        inicio = (page-1)*paginate
                        final = ((page-1)*paginate)+number_dec
                        if final > len(data):
                            final = len(data)

            elif number_ent == 0 and number_dec>0:
                inicio = 1
                final = number_dec
                if final > len(data):
                    final = len(data)

            elif number_ent > 0 and number_dec == 0:
                if page<= number_ent:
                    if page==1:
                        inicio = 1
                        final = page * paginate
                    else:
                        inicio = (page-1)*paginate
                        final = page * paginate


        # print('numero----',c)
        # print('entero----',number_ent)
        # print('decimal----',number_dec)

        # print('inicio----',inicio)
        # print('final----',final)

        return {'inicio_info':inicio,'final_info':final, 'total':len(data)}
        

class StatisticsViewSet(viewsets.ViewSet):      
    @action(detail=True, methods=['get'])
    def data(self,request,pk):
        tipo = int(pk)
        search = request.query_params.get('search') if request.query_params.get('search') != 'undefined' else None
        models_ = [People,Institution,Document,Training,Vessel,Laboratory,Platformgeo]
        header_table = header_statistics(tipo)
        array_paises = []
        data_document_p = {}
        data_pais = []
        array_paises = get_data_key_model(models_,'pais')  
        data_document_p = self.get_data_(models_,array_paises,'pais')      
        pprint(data_document_p)  
        
        if(tipo == 1):            
            return Response({'data_header':header_table,'data':self.get_data_by_pais(data_document_p),'sta_document':[]}, status=200)            
        elif(tipo == 2):            
            return Response({'data_header':header_table,'sta_paises':[],'data':self.get_data_by_documen_pais(data_document_p)}, status=200)
        elif(tipo == 3):            
            return Response({'data_header':header_table,'sta_paises':[],'data':self.get_data_by_document(data_document_p)}, status=200)
            

    def get_data_(self,models_,array,key):
        data_paises={}
        for model in models_:                
                obj_={}
                res_ = next((x for x in array if x['model'] == model.__name__), None)
                for pais in res_[key]:                    
                    try:
                        data = []
                        data = model.objects.filter(**{key:pais}).count()                        
                        if pais is not None:
                            pa_ = pais.replace(' ', '')
                            if pa_.lower() in obj_:                                
                                obj_[pa_.lower()]=int(obj_[pa_.lower()])+int(data)
                            else:    
                                obj_[pa_.lower()]=int(data)
                        else:
                            obj_[pais]=int(data)
                    except Exception as ex:
                         print('->error',ex)     
                if(model.__name__ == 'Platformgeo'):
                    data_paises['Geospatial information']=obj_
                else:
                    data_paises[model.__name__]=obj_
        return data_paises


    def rename_key(self,key_d):
        if key_d == 'brasil':
            key_d = 'brazil'
        if key_d == 'unitedstatesofamerica':
            key_d = 'unitedstates'
        return key_d


    def get_data_by_pais(self,data_document_p):
        array_ =[]
        data_pais = {}
        for key,value in data_document_p.items():                
            for key_d, value_d in value.items():
                key_d = self.rename_key(key_d)                    
                if key_d in data_pais:
                    data_pais[key_d] = int(value_d) + int(data_pais[key_d])
                else:
                    data_pais[key_d] = int(value_d)
        val_t=0
        for key,value in data_pais.items():
            val_t += int(value)
            array_.append({'country':key,'total':value})
        array_.append({'name':'total','total':val_t})
        return array_


    def get_data_by_documen_pais(self,data_document_p):
        total_=0
        arr_data=[]            
        
        for key_d,value_d in data_document_p.items():
            obj_data={}
            arr_=[]
            for key_p,value_p in value_d.items():
                obj_={}
                obj_['country']=key_p
                obj_['total']=value_p
                obj_['category']=key_d
                total_ += int(obj_['total'])
                arr_.append(obj_)
            obj_data['category']=key_d
            arr_data += arr_
            # obj_data['values']=arr_
            # arr_data.append(obj_data)
        arr_data.append({'name':'total','total':total_})
        return arr_data

    
    def get_data_by_document(self,data_document_p):
        array_ =[]    
        val_t=0     
        for key,value in data_document_p.items():  
            obj_document={}
            total_=0              
            for key_d, value_d in value.items():
                total_ += int(value_d)                
            obj_document['category']=key
            obj_document['total']=total_
            val_t += int(total_)
            array_.append(obj_document)
        array_.append({'name':'total','total':val_t})
        
        return array_


class LoginAPI(generics.GenericAPIView):

    serializer_class = LoginUserSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data 
        return Response({
            "user": UserSerializer(user, context=self.get_serializer_context()).data,
            "token":"token " + AuthToken.objects.create(user)[1]
        })
