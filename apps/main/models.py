import json
from django.db import models
from django.utils.timezone import now
import datetime


class Categoria(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100, null=True)
    descripcion = models.TextField()

    def __str__(self):
        return self.nombre


class People(models.Model):
    #id = models.AutoField(primary_key=True)
    id_nombre = models.CharField(max_length=200, primary_key=True)
    imagen = models.CharField(max_length=200,verbose_name='  ')
    nombre = models.CharField(max_length=200, null=False, verbose_name='Name' )
    url = models.CharField(max_length=200,verbose_name="Url")
    institucion = models.CharField(max_length=200, verbose_name='Institution' )    
    ocupacion = models.CharField(max_length=200, verbose_name='Occupation')
    #descripcion = models.TextField()
    aptitudes = models.TextField()  
    pais = models.CharField(max_length=300, null=True, verbose_name ='Country' )  
    source = models.CharField(max_length=300, verbose_name='Source')
    source_id = models.IntegerField(null=False, default=0)
    method_harvesting = models.CharField(max_length=300)

    def __str__(self):
        return self.nombre


class Institution(models.Model):
    #id = models.AutoField(primary_key=True)
    id_nombre = models.CharField(max_length=200, primary_key=True)
    imagen = models.CharField(max_length=200, verbose_name=' ')
    nombre = models.CharField(max_length=200, null=False, verbose_name='Name')
    url = models.CharField(max_length=200,verbose_name="Url")
    direccion = models.CharField(max_length=200, verbose_name='Address')
    pais = models.CharField(max_length=300, null=True, verbose_name ='Country' )
    source = models.CharField(max_length=300, verbose_name='Source')
    source_id = models.IntegerField(null=False, default=0)
    method_harvesting = models.CharField(max_length=300)
    #descripcion = models.TextField()    
    
    def __str__(self):
        return self.nombre


class Document(models.Model):
    id_nombre = models.CharField(max_length=1000, primary_key=True)
    imagen = models.CharField(max_length=200, verbose_name=' ')
    nombre = models.CharField(max_length=1000, null=False, verbose_name='Name')
    url = models.CharField(max_length=200, verbose_name="Url")
    fecha = models.CharField(max_length=200,null=True,blank=True, verbose_name='Date')    
    resumen = models.CharField(max_length=3000, verbose_name='_Resume') 
    pais = models.CharField(max_length=300, null=True, verbose_name ='Country' )
    source = models.CharField(max_length=300, verbose_name='Source')
    source_id = models.IntegerField(null=False, default=0)
    method_harvesting = models.CharField(max_length=300)
    json_ld=models.TextField(verbose_name='json_ld', null=True, default=None)
    shared = models.BooleanField(verbose_name='Shared', default=True)
    

    def __str__(self):
        return self.nombre + self.shared


class Training(models.Model):
    id_nombre = models.CharField(max_length=200, primary_key=True)
    imagen = models.CharField(max_length=200, verbose_name=' ' )
    nombre = models.CharField(max_length=200, null=False, verbose_name='Name')
    url = models.CharField(max_length=200, verbose_name="Url")
    tipo = models.CharField(max_length=300, null=True, verbose_name='Type' )    
    email = models.CharField(max_length=300, null=True,verbose_name='_Email'  )    
    ubicacion = models.CharField(max_length=200,null=True, blank=True, verbose_name='Location')
    fecha = models.CharField(max_length=200,null=True,blank=True, verbose_name='_Date')
    institucion = models.CharField(max_length=300,null=True, verbose_name='Institution'  )
    resumen = models.CharField(max_length=300)
    order =  models.IntegerField(blank=True, null=True)
    pais = models.CharField(max_length=300, null=True )    
    facultad = models.CharField(max_length=300, null=True )    
    telefono = models.CharField(max_length=300, null=True )
    source = models.CharField(max_length=300, verbose_name='Source')
    source_id = models.IntegerField(null=False, default=0)
    method_harvesting = models.CharField(max_length=300)
    def __str__(self):
        return self.nombre

    class Meta:
        ordering = ('-tipo',)


class Vessel(models.Model):
    id_nombre = models.CharField(max_length=200, primary_key=True)    
    nombre = models.CharField(max_length=200, blank=True, null=True, verbose_name='Name')
    url = models.CharField(max_length=200, verbose_name="Url")
    pais = models.CharField(max_length=300, null=True, verbose_name ='Country' )    
    institucion = models.CharField(max_length=200, blank=True, null=True, verbose_name='Institution')        
    descripcion = models.CharField(max_length=300, blank=True, null=True, verbose_name='_Description')
    equipi_nav = models.CharField(max_length=300, blank=True, null=True, verbose_name='_Navigation Equipment')
    cantidad_trip = models.CharField(max_length=300, blank=True, null=True,verbose_name='_Crew')
    source = models.CharField(max_length=300, verbose_name='_Source', default="")
    categoria = models.CharField(max_length=300, blank=True, null=True, verbose_name='Category')
    estado = models.CharField(max_length=300, blank=True, null=True, verbose_name='Status')
    actividad = models.CharField(max_length=300, blank=True, null=True, verbose_name='Activity')
    envergadura = models.CharField(max_length=300, blank=True, null=True, verbose_name='Size')
    method_harvesting = models.CharField(max_length=300, default="")
    source_id = models.IntegerField(null=False, default=0)


# class Naviequipment(models.Model):
#     id_nombre = models.CharField(max_length=200, primary_key=True)
#     nombre = models.CharField(max_length=200, null=False)
#     vessels=models.ManyToManyField(Vessel, blank=True)


# class Equipment(models.Model):
#     id_nombre = models.CharField(max_length=200, primary_key=True)
#     nombre = models.CharField(max_length=200, null=False)
#     naviequipment = models.ForeignKey(Naviequipment, on_delete=models.DO_NOTHING, blank=True, null=True)
#     vessel = models.ForeignKey(Vessel, on_delete=models.DO_NOTHING, blank=True, null=True)


class Laboratory(models.Model):
    id_nombre = models.CharField(max_length=200, primary_key=True)
    imagen = models.CharField(max_length=200, verbose_name=' ')
    nombre = models.CharField(max_length=200, null=False, verbose_name='Name')
    url = models.CharField(max_length=200, verbose_name="Url")
    resumen = models.CharField(max_length=300, verbose_name='_Resume')
    pais = models.CharField(max_length=300, null=True, verbose_name ='Country' )
    entidad = models.CharField(max_length=300, null=True, verbose_name ='Organization')
    source = models.CharField(max_length=300, verbose_name='Source')
    source_id = models.IntegerField(null=False, default=0)
    method_harvesting = models.CharField(max_length=300)
    
    
    
    def __str__(self):
        return self.nombre


class Platformgeo(models.Model):
    id_nombre = models.CharField(max_length=200, primary_key=True)
    imagen = models.CharField(max_length=200, verbose_name=' ')
    nombre = models.CharField(max_length=200, null=False, verbose_name='Name')
    url = models.CharField(max_length=200, verbose_name="Url")
    category = models.CharField(max_length=200, verbose_name="Category")
    resumen = models.CharField(max_length=300, verbose_name='_Abstract')
    date = models.CharField(max_length=300, verbose_name='Publicacion Date')
    source = models.CharField(max_length=300, verbose_name='Source')
    source_id = models.IntegerField(null=False, default=0)
    pais = models.CharField(max_length=300, null=True, verbose_name ='Country')
    method_harvesting = models.CharField(max_length=300, default="")
    bounding_box = models.CharField(max_length=300,null=True,verbose_name="bounding box")
    def __str__(self):
        return self.nombre


class Recurso(models.Model):
    id = models.AutoField(primary_key=True,editable=False)
    titulo = models.CharField(max_length=200, null=False)
    url = models.CharField(max_length=200)
    funcionalidad = models.CharField(max_length=300)
    licenciamiento = models.CharField(max_length=200,verbose_name="Clase de Licenciamiento")
    palabrasClave = models.CharField(max_length=200,null=True,blank=True,verbose_name="Palabras Claves")

    def __str__(self):
        return self.titulo


