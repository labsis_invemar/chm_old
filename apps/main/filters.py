from .models import (
    People, Institution, Document,
    Training, Laboratory, Platformgeo, Vessel
    )
import django_filters


class PeopleFilter(django_filters.FilterSet):
    
    nombre = django_filters.CharFilter(label='Name',lookup_expr='icontains')
    ocupacion = django_filters.CharFilter(label='occupation',lookup_expr='icontains')
    institucion = django_filters.CharFilter(label='institution',lookup_expr='icontains')

    class Meta:
        model = People
        fields = ['nombre','ocupacion','institucion']


class InstitutionFilter(django_filters.FilterSet):
    
    nombre = django_filters.CharFilter(label='Name',lookup_expr='icontains')

    class Meta:
        model = Institution
        fields = ['nombre']

class DocumentFilter(django_filters.FilterSet):

    nombre = django_filters.CharFilter(label='Name',lookup_expr='icontains')

    class Meta:
        model  = Document
        fields = ['nombre']


class TrainingFilter(django_filters.FilterSet):

    nombre = django_filters.CharFilter(label='Name',lookup_expr='icontains')

    class Meta:
        model  = Training
        fields = ['nombre']


class LaboratoryFilter(django_filters.FilterSet):

    nombre = django_filters.CharFilter(label='Name',lookup_expr='icontains')

    class Meta:
        model  = Laboratory
        fields = ['nombre']


class PlatformgeoFilter(django_filters.FilterSet):

    nombre = django_filters.CharFilter(label='Name',lookup_expr='icontains')

    class Meta:
        model  = Platformgeo
        fields = ['nombre']


class VesselFilter(django_filters.FilterSet):

    nombre = django_filters.CharFilter(label='Name',lookup_expr='icontains')

    class Meta:
        model  = Vessel
        fields = ['nombre']