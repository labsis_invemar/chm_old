# Generated by Django 2.0 on 2022-01-15 22:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0019_merge_20190617_1600'),
    ]

    operations = [
        migrations.AddField(
            model_name='document',
            name='json_ld',
            field=models.TextField(default=None, null=True, verbose_name='json_ld'),
        ),
        migrations.AlterField(
            model_name='document',
            name='id_nombre',
            field=models.CharField(max_length=1000, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='document',
            name='nombre',
            field=models.CharField(max_length=1000, verbose_name='Name'),
        ),
        migrations.AlterField(
            model_name='document',
            name='resumen',
            field=models.CharField(max_length=3000, verbose_name='_Resume'),
        ),
        migrations.AlterField(
            model_name='people',
            name='ocupacion',
            field=models.CharField(max_length=200, verbose_name='Occupation'),
        ),
    ]
