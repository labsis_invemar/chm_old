from django.core.paginator import Paginator
from .serializers import *
from apps.admins.serializers import *

def get_data_list(request, menu, serializer_class, fields_include=[]):
    page = request.query_params.get('page')
    is_pagination = request.query_params.get('is_pagination')
    paginate = 10
    pagination_num = 0
    objs = search = serializer = None
    model = serializer_class.Meta.model
    try:
        objs = model.objects.all()
        pagination_num = get_paginate_num(model, paginate, None, objs)
        serializer = serializer_class( 
            get_paginate_data(objs, paginate, page, pagination_num), 
            many=True
            )
    except Exception as err:
        print(err)
    return {
            'data':serializer.data, 
            'fields':get_model_fields_select(model, fields_include), 
            'pagination_num':pagination_num, 
            'menu':menu,
            'page_current':page if page is not None and int(page) <= pagination_num else 1,
            'search': search if search else None,
            'is_pagination':is_pagination
        }


def get_fields_list(menu=''):
    if menu:
        if menu.lower() == 'users':
            return [
                'username', 
                'first_name',
                'last_name', 
                'email', 
                'is_active',
                'is_superuser'
                ]
    return []


def get_serializer_class(menu=''):
    if menu:
        if menu.lower() == 'webservice':
            return WebServiceSerializer
        elif menu.lower() == 'oai':
            return OaiRepoSerializer
        elif menu.lower() == 'scraping':
            return ScrapingSerializer
        elif menu.lower() == 'users':
            return UserSerializer
        elif menu.lower() == 'upload':
            return UpdloadFiledSerializer
        elif menu.lower() == 'apireader': 
            return ApiReaderSerializer
        print('Does not exist serializer class for menu ' + menu)
    return None


def obj_set_data(obj, data, attrs, booleans):
    for attr in attrs:
        if hasattr(obj, attr):
            if attr in booleans:
                val = data.get(attr)
                if len(str(val).replace(" ","")) > 0:
                    setattr(obj, attr, True)
                else:
                    setattr(obj, attr, False)
            else:    
                setattr(obj, attr, data.get(attr))
        else:
            print(f'obj {obj} has not attr {attr}')
    return obj


def get_paginate_num(model,paginate,search,data):
    pagination_num = len(data)/paginate       
    number_dec = str(pagination_num-int(pagination_num))[2:]
    if int(number_dec) == 0:
        return int(pagination_num)
    elif isinstance(pagination_num, int):
        return int(pagination_num)
    else:
        return int(pagination_num) +1


def get_paginate_data(data,paginate,page,pagination_num):
    paginator = Paginator(data, paginate)    
    if page is not None and data is not None and int(page) <= int(pagination_num):        
        return paginator.page(page)
    return paginator.page(1)


def get_model_fields(model):    
    fields = []
    for field in model._meta.get_fields():
        fields.append({field.name:field._verbose_name,'key':field.name})
    return fields


def get_model_fields_select(model, include=[]):    
    fields = []
    for field in model._meta.fields:
        if field._verbose_name:
            if field._verbose_name != None and field._verbose_name.strip() != "" :
                if not include: 
                    fields.append({field.name:field._verbose_name,'key':field.name})
                else:
                    if field.name in include:
                        fields.append({field.name:field._verbose_name,'key':field.name})
    return fields
        

def get_temathic_(search,count_temathic):
    if search is not None and len(search)>0:     
        for temathic, value in count_temathic.items():
            if(value>0):
                return temathic


def get_query_data(model,search):        
    if search:
        query = create_query(model,search)
        return model.objects.filter(reduce(operator.or_, query))
    else:
        return model.objects.all()
     

def create_query(model,search):
    query = []
    for field in get_model_fields(model):
        if field[field['key']] is not None and field['key'] is not 'imagen':
            key = field['key']+'__icontains'
            query.append(Q(**{key: search}))            
    return query


def get_count_temathic_search(search,is_pagination):    
    if search and is_pagination is None:
        print('entre-------')
        people = get_query_data(People,search)
        document = get_query_data(Document,search)
        training = get_query_data(Training,search)
        laboratory = get_query_data(Laboratory,search)
        institution = get_query_data(Institution,search)
        platformgeo = get_query_data(Platformgeo,search)
        vess = get_query_data(Vessel,search)

        return {
        'experts':len(people),
        'documents':len(document),
        'training':len(training),
        'laboratories':len(laboratory),
        'institutions':len(institution),
        'geo':len(platformgeo),
        'vess':len(vess)
        }
    else:
        return None


def header_statistics(option):
    if option:
        if option == 1:
            return [
                # 'country', 
                'country',
                '%',
                'total'
                ]
        if option == 2:
            return [
                'category', 
                'country',
                '%',
                'total'
                ]
        if option == 3:
            return [
                # 'country', 
                'category',
                '%',
                'total'
                ]
    return []

def get_data_key_model(models_=[],key=''):
    if len(models_)>0 and key:
        array=[]
        for model in models_:
            data = []
            obj_={}
            try:
                data = model.objects.order_by().values_list(key, flat=True).distinct()                                    
                obj_['model']=model.__name__
                obj_[key]=data
                array.append(obj_)
            except Exception as ex:
                obj_['model']=model.__name__
                obj_[key]=[]
                array.append(obj_)
        return array
    else:
        return []
