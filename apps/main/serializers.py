from .models import *
from rest_framework import serializers, permissions
from django.contrib.auth.models import User
from django.contrib.auth import authenticate

class VesselSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vessel
        fields = '__all__'


class ExpertsSerializer(serializers.ModelSerializer):

    class Meta:
        model = People
        fields = '__all__'
        

class DocumentsSerializer(serializers.ModelSerializer):

    permission_classes = [permissions.IsAuthenticated, ]

    class Meta:
        model = Document
        fields = '__all__'


class TrainingSerializer(serializers.ModelSerializer):

    class Meta:
        model = Training
        fields = '__all__'


class LaboratorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Laboratory
        fields = '__all__'


class InstitutionsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Institution
        fields = '__all__'


class PlatformgeoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Platformgeo
        fields = '__all__'




class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'


class LoginUserSerializer(serializers.Serializer):
    
    username = serializers.CharField()
    password = serializers.CharField()

    def validate(self, data):
        user = authenticate(**data)
        if user and user.is_active:
            return user
        raise serializers.ValidationError("Validation error")