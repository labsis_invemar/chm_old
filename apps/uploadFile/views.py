import openpyxl
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import redirect, render
from django.utils.decorators import method_decorator
from django.views import View
import unidecode
from pprint import pprint
from .forms import UploadFileForm
from apps.main.models import *
import xlwt

class ExportFiles(View):
    
    def export_data_xls(self, type_worksheet, filters, data):
        # print('filters>>>',filters)
        # print('hoja de trabajo>>>',type_worksheet)
        # print('datos>>>',data[0])
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename='+type_worksheet+'_failed.xls'
        
        
        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet(type_worksheet)

        # Sheet header, first row
        row_num = 0

        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        for i, attr in enumerate(filters):
            # print('index>>>',i)
            # print('item>>>',attr)
            ws.write(row_num, i, attr, font_style)
        

        # Sheet body, remaining rows
        font_style = xlwt.XFStyle()

        rows = data
       

        for row in rows:
            row_num += 1
            for i, attr in enumerate(filters):
                if attr in row:
                    ws.write(row_num, i, row[attr], font_style)
                else:
                     ws.write(row_num, i, '-----', font_style) 
            
                    

        wb.save(response)        
        return response

class UploadFile(ExportFiles):
    template_name = "uploadFile/index.html"
    def get(self, request): 
        upload_file_Form = UploadFileForm(prefix="upload-fl")       
        context = {
            'title':'Cargar datos',
            'form': upload_file_Form
        }
        return render(request, self.template_name, context)
    
    def post(self, request):
        
        
        upload_file_Form = UploadFileForm(request.POST,
                request.FILES,prefix="upload-fl")
        
        if upload_file_Form.is_valid():
            array_data_fail = []
            excel_file = request.FILES["upload-fl-file"]
            try:                
                wb = openpyxl.load_workbook(excel_file)
            except Exception as e:
                return render(request, self.template_name, {
                    'title':'Cargar datos',
                    'form': upload_file_Form,
                    'error_msj': 'Error al tratar de abrir el archivo excel por favor si tiene graficas borrelas e itente nuevamente'
                })
            
            type_worksheet = upload_file_Form.cleaned_data.get('type')
            sheets = wb.sheetnames
            
            for sheet in sheets:
                filters = {}
                array_data = []                
                res = {}
                model = None
                try:                
                    worksheet = wb[sheet]
                except Exception as e:
                    return render(request, self.template_name, {
                        'title':'Cargar datos',
                        'form': upload_file_Form,
                        'error_msj': 'Error al abrir hoja de trabajo '+sheet+ ' por favor trate cambiandole el nombre y que no contenga ninguna grafica'
                    })


                filters = self.get_filters(worksheet)
                # pprint(filters)                            
                model = self.instance_models(filters, type_worksheet)                

                if model is not None:
                    res = self.get_data_excel(filters, worksheet, type_worksheet)
                    array_data = res['data']
                    array_data_fail += res['data_fail']

                    # print(model.__name__)
                    if(len(array_data)>0):
                        print('array_succes>>>',len(array_data))
                        for data in array_data:
                            self.save_data(model,data)                    

                else:

                    return render(request, self.template_name, {
                        'title':'Cargar datos',
                        'form': upload_file_Form,
                        'error_msj': 'La hoja '+ sheet +' no cumple con las columnas para ser almacenada como '+ type_worksheet
                    })
            
        
            if(len(array_data_fail)>0):
                            print('array_fail>>>',len(array_data_fail))
                            return self.export_data_xls(type_worksheet, filters, array_data_fail)
        context = {
                'title':'Cargar datos',
                'form': upload_file_Form
            }
        return render(request, self.template_name, context)

    def get_filters(self,worksheet):
        filters={}     
        for i, row in enumerate(worksheet.iter_rows()):             
            if i == 0:
                for cell in row:
                    if cell.value is not None:                        
                        filters[cell.value.lower().replace(" ", "")]=cell.column            
        # print('filtrosss>>>',filters)
        return filters

    def get_data_excel(self, filters, worksheet, type_worksheet):
        start, stop = 0, 10
        obj_data = {}
        data = []   
        data_fail = []             
        for index, row in enumerate(worksheet.iter_rows()):
            if (start < index):
                obj_data = {}
                for cell in row:
                    if(cell.value is not None):                            
                        for attr, value in filters.items():
                            if(value == cell.column):
                                obj_data[attr] = cell.value                
                
                if self.create_id_for_obj(obj_data, type_worksheet)  is not None:
                    data.append(self.create_id_for_obj(obj_data, type_worksheet))       
                else:
                    data_fail.append(obj_data)        
        return {
            'data':data,
            'data_fail':data_fail
        }

    def instance_models(self, filters, type_worksheet):
        # aqui simpre se manda con sw == 2 para que la funcion retorne simpre los modelos        
        return self.keys_for_model(filters, type_worksheet, 2)
    

    def save_data(self,model,data):
        if(model.__name__ == 'Training'):
            # try:
            #     obj = Training.objects.get(pk=data['id_nombre'])
            #     print('encontradoo>>>',obj)
            # except Exception as e:
            #     pass  
            traning = model()
            traning.id_nombre = data['id_nombre'] if 'id_nombre' in data else None
            traning.ubicacion = data['state'] # if 'address' in data else None
            traning.facultad = data['department'] if 'department' in data else None
            traning.institucion = data['institution'] if 'institution' in data else None
            traning.order = 2
            traning.telefono = data['phone'] if 'phone' in data else None
            traning.nombre = data['institution']+" -> "+"  "+data['department']+" -> "+"  "+data['program'] 
            traning.pais = data['state'] if 'state' in data else None
            traning.tipo = ' Formal '+data['type'] if 'type' in data else None
            traning.url = data['url'] if 'url' in data else None
            traning.email = data['email'] if 'email' in data else None
            traning.imagen = "/static/main/index/images/training-default.png"
            traning.save()
        elif (model.__name__ == 'Vessel'):
            # try:
            #     obj = Vessel.objects.get(pk=data['id_nombre'])
            #     print('encontradoo>>>',obj)
            # except Exception as e:
            #     pass    
            
            vessel = model()
            vessel.id_nombre = data['id_nombre'] if 'id_nombre' in data else None
            vessel.pais = data['state'] if 'state' in data else None
            vessel.nombre = data['name'] if 'name' in data else None
            vessel.descripcion = data['resume'] if 'resume' in data else None
            vessel.equipi_nav = data['naviequipment'] if 'naviequipment' in data else None
            vessel.cantidad_trip = data['crew'] if 'crew' in data else None
            vessel.institucion = data['institution'] if 'institution' in data else None
            vessel.save()


    def keys_for_model(self, filters, type_worksheet, sw):
        if type_worksheet == 'Training':
             if('url' in filters and 'program' in filters and 'type' in filters and 'department' in filters and 'institution' in filters):
                if sw == 1:
                    return {'valid':True, 'type_worksheet':type_worksheet}
                else:
                    return  Training
        elif type_worksheet == 'Vessel':             
            if('state' in filters and 'name' in filters and 'resume' in filters):                
                if sw == 1:
                    return {'valid':True, 'type_worksheet':type_worksheet}
                else:                    
                    return  Vessel


    def create_id_for_obj(self, obj_data, type_worksheet):
       objw = self.keys_for_model(obj_data, type_worksheet, 1)       
       if (objw is not None  and  objw['valid'] == True and objw['type_worksheet'] == 'Training'):       
            obj_data['id_nombre'] = unidecode.unidecode(
                obj_data['program'][0:10].upper().replace(" ", "")+
                obj_data['type'].upper().replace(" ", "")+
                obj_data['department'][-10:].upper().replace(" ", "")+
                obj_data['institution'][-10:].upper().replace(" ", ""))
            return obj_data
       if (objw is not None  and  objw['valid'] == True and objw['type_worksheet'] == 'Vessel'):
           obj_data['id_nombre'] = unidecode.unidecode(
                obj_data['name'].replace(" ", "").upper()+
                obj_data['state'].replace(" ", "").upper())
           return obj_data

