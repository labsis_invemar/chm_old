import os
from django import forms
from pprint import pprint

class UploadFileForm(forms.Form):
    NGO_CHOICES = (
            ('Training', 'Training'),
            ('Vessel', 'Vessel'),
            # (3, 'Especie'),
        )

    def __init__(self, *args, **kwargs):        
        super(UploadFileForm, self).__init__(*args, **kwargs)
        for key in self.fields:
            self.fields[key].required = True 

    def clean_file(self, *args, **kwargs):
        cleaned_data = super(UploadFileForm, self).clean()
        file = cleaned_data.get('file')

        if file:
            filename = file.name            
            if filename.endswith('.xlsx'):
                print ('')
            else:               
                print('error') 
                raise forms.ValidationError("Archivo no permitido")
            
        
        
    file = forms.FileField(
        label="Cargar Archivo",
        help_text="Solo archivos Excel",
        required=True,
        widget=forms.FileInput(attrs={})
        )

    type = forms.ChoiceField(
        label="Tipo de documento",
        choices=NGO_CHOICES,
         widget=forms.Select(attrs={'class':'regDropDown'})       
        )    