from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('', include('apps.frontend.urls')),
    path('api/admins/',include('apps.admins.urls')),
    path('api/',include('apps.main.urls')),
    path('admin/', admin.site.urls), 
]