# -*- coding: utf-8 -*-
from .base import *

DEBUG = False
PATH_ENV = '/var/envs/chm/bin/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
