FROM nikolaik/python-nodejs:python3.7-nodejs16
# MAINTAINER INVEMAR

ENV PYTHONUNBUFFERED 1
RUN apt-get install python2
# RUN git config --global user.name "INVEMAR"

# RUN git config --global user.email "francisco.campo@invemar.org.co"

# RUN git clone https://bitbucket.org/labsis_inemar/chm.git

WORKDIR chm

COPY . .
# RUN apt-get install make g++
# RUN npm install
# RUN npm run build 
# RUN git pull origin master &&  chmod -R 777 . 

RUN pip install -r requirements.txt
# RUN python manage.py makemigrations
# RUN python manage.py migrate
EXPOSE 8000

CMD  gunicorn chm.wsgi:application --bind 0.0.0.0:8000 --workers=3 --pid /tmp/gunicorn.pid